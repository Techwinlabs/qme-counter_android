package com.techwin.counter.di.base.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.techwin.counter.BR;


public class BaseSheet<B extends ViewDataBinding> extends BottomSheetDialogFragment {

    private B binding;
    private ClickListener listener;
    private final int layout;

    public BaseSheet(@LayoutRes int layout) {
        this.layout = layout;
    }

    public BaseSheet(int layout, ClickListener listener) {
        this.listener = listener;
        this.layout = layout;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, layout, container, false);
        binding.setVariable(BR.callback, listener);
        return binding.getRoot();
    }

    public void setListener(ClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (this.listener != null)
            this.listener.onViewCreated();
    }

    public B getBinding() {
        return binding;
    }

    public interface ClickListener {
        void onClick(@NonNull View view);

        void onViewCreated();
    }
}
