package com.techwin.counter.di.base.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.techwin.counter.BR;

import java.util.Collections;
import java.util.List;

public class SimpleRecyclerViewAdapter<M, B extends ViewDataBinding> extends RecyclerView.Adapter<SimpleRecyclerViewAdapter.SimpleViewHolder<B>> {

    @LayoutRes
    private final int layoutResId;
    private final int modelVariableId;
    private final SimpleCallback<M> callback;
    private final ObservableList<M> dataList = new ObservableArrayList<>();
    public DataBindingUtil bindingUtil;
        B binding;

    public ObservableList<M> getDataList() {
        return dataList;
    }

    @Nullable
    public M getData(int index) {
        if (index < dataList.size())
            return dataList.get(index);
        return null;

    }

    public B getBinding() {
        return binding;
    }

    public void removeItem(int i) {
        try {
            if (i >= 0 && i < dataList.size()) {
                dataList.remove(i);
                notifyItemRemoved(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void set(int i, M scanResult) {
        if (scanResult == null)
            return;
        dataList.add(i, scanResult);
        notifyItemChanged(i);
    }


    public interface SimpleCallback<M> {
        void onItemClick(View v, M m);

        default void onPositionClick(View v, int pos) {
        }

        default void onClickPos(View v, M m, int pos) {

        }


    }


    public SimpleRecyclerViewAdapter(@LayoutRes int layoutResId, int modelVariableId, SimpleCallback<M> callback) {
        this.layoutResId = layoutResId;
        this.modelVariableId = modelVariableId;
        this.callback = callback;

    }


    @NonNull
    @Override
    public SimpleViewHolder<B> onCreateViewHolder(ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), layoutResId, parent, false);
        binding.setVariable(BR.callback, callback);
        return new SimpleViewHolder<>(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SimpleViewHolder holder, int position) {
        holder.binding.setVariable(BR.holder, holder);
        holder.binding.setVariable(modelVariableId, dataList.get(position));
        holder.binding.setVariable(BR.pos, position);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void setList(@Nullable List<M> newDataList) {
        dataList.clear();
        if (newDataList != null)
            dataList.addAll(newDataList);
        notifyDataSetChanged();
    }

    public List<M> getList() {
        return dataList;

    }

    public void addToList(@Nullable List<M> newDataList) {
        if (newDataList == null) {
            newDataList = Collections.emptyList();
        }
        int positionStart = dataList.size();
        int itemCount = newDataList.size();
        dataList.addAll(newDataList);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void clearList() {
        dataList.clear();
        notifyDataSetChanged();
    }

    public void addData(@NonNull M data) {
        int positionStart = dataList.size();
        dataList.add(data);

        notifyItemInserted(positionStart);
    }




    /**
     * Simple view holder for this adapter
     *
     * @param <S>
     */
    public static class SimpleViewHolder<S extends ViewDataBinding> extends RecyclerView.ViewHolder {
        S binding;

        public SimpleViewHolder(S binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}
