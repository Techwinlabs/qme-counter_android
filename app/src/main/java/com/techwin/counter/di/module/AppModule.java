package com.techwin.counter.di.module;

import android.content.Context;

import com.techwin.counter.BuildConfig;
import com.techwin.counter.di.qualifier.ApiDateFormat;
import com.techwin.counter.di.qualifier.ApiEndpoint;
import com.techwin.counter.di.qualifier.AppContext;
import com.techwin.counter.util.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.support.DaggerApplication;
import okhttp3.HttpUrl;

@Module
public class AppModule {
    @AppContext
    @Provides
    static Context context(DaggerApplication daggerApplication) {
        return daggerApplication;
    }

    @ApiEndpoint
    @Singleton
    @Provides
    static HttpUrl apiEndpoint() {
        return HttpUrl.parse(BuildConfig.BASE_URL);
    }



    @Singleton
    @Provides
    static HttpUrl placeEndpoint() {
        return HttpUrl.parse(Constants.GOOGLE_URL);
    }

    @ApiDateFormat
    @Singleton
    @Provides
    static String apiDateFormat() {
        return "yyyy-MM-dd HH:mm:ss";
    }


}
