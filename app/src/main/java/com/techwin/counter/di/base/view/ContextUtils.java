package com.techwin.counter.di.base.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;

import java.util.Locale;

class ContextUtils extends ContextWrapper {
    public ContextUtils(Context base) {
        super(base);
    }

    public static ContextWrapper updateLocale(Context context, Locale localeToSwitchTo) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocale(localeToSwitchTo);
            LocaleList localeList = new LocaleList(localeToSwitchTo);
            LocaleList.setDefault(localeList);
            configuration.setLocales(localeList);
            context = context.createConfigurationContext(configuration);
        } else {
            Locale.setDefault(localeToSwitchTo);
            configuration.locale = localeToSwitchTo;
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }
        return new ContextUtils(context);
    }
}
