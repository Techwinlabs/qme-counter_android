package com.techwin.counter.di.module;


import androidx.lifecycle.ViewModel;

import com.techwin.counter.di.mapkey.ViewModelKey;
import com.techwin.counter.ui.admin.card.CardFragmentVM;
import com.techwin.counter.ui.admin.history.HistoryFragmentVM;
import com.techwin.counter.ui.admin.history.detail.HistoryDetailFragmentVM;
import com.techwin.counter.ui.admin.manage.ManageFragmentVM;
import com.techwin.counter.ui.admin.profile.ProfileFragmentVM;
import com.techwin.counter.ui.admin.reservation.FutureFragmentVM;
import com.techwin.counter.ui.admin.today.TodayFragmentVM;
import com.techwin.counter.ui.admin.today.detail.TodayDetailFragmentVM;
import com.techwin.counter.ui.admin.wallet.BalanceFragmentVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class FragmentViewModelModule {
    // NOTE: customize here
    /*@Binds
    @IntoMap
    @ViewModelKey(VideoFragmentVM.class)
    abstract ViewModel VideoFragmentVM(VideoFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(VideoDetailFragmentVM.class)
    abstract ViewModel VideoDetailFragmentVM(VideoDetailFragmentVM vm);


   */
    @Binds
    @IntoMap
    @ViewModelKey(CardFragmentVM.class)
    abstract ViewModel CardFragmentVM(CardFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(ProfileFragmentVM.class)
    abstract ViewModel ProfileFragmentVM(ProfileFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(BalanceFragmentVM.class)
    abstract ViewModel BalanceFragmentVM(BalanceFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(ManageFragmentVM.class)
    abstract ViewModel ManageFragmentVM(ManageFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(FutureFragmentVM.class)
    abstract ViewModel FutureFragmentVM(FutureFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(TodayFragmentVM.class)
    abstract ViewModel TodayFragmentVM(TodayFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(HistoryFragmentVM.class)
    abstract ViewModel HistoryFragmentVM(HistoryFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(TodayDetailFragmentVM.class)
    abstract ViewModel TodayDetailFragmentVM(TodayDetailFragmentVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(HistoryDetailFragmentVM.class)
    abstract ViewModel HistoryDetailFragmentVM(HistoryDetailFragmentVM vm);
}
