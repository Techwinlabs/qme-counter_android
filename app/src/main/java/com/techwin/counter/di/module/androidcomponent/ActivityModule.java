package com.techwin.counter.di.module.androidcomponent;

import com.techwin.counter.ui.admin.card.otp.OtpActivity;
import com.techwin.counter.ui.admin.main.AdminMainActivity;
import com.techwin.counter.ui.login.LoginActivity;
import com.techwin.counter.ui.main.MainActivity;
import com.techwin.counter.ui.main.detail.DetailActivity;
import com.techwin.counter.ui.main.multi.Main2Activity;
import com.techwin.counter.ui.reservation.ReservationActivity;
import com.techwin.counter.ui.reservation.detail.ReservationDetailActivity;
import com.techwin.counter.ui.welcome.WelcomeActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    // NOTE: UserApp  here


    @ContributesAndroidInjector
    abstract ReservationDetailActivity reservationDetailActivity();

    @ContributesAndroidInjector
    abstract DetailActivity detailActivity();

    @ContributesAndroidInjector
    abstract ReservationActivity reservationActivity();

    @ContributesAndroidInjector
    abstract WelcomeActivity welcomeActivity();

    @ContributesAndroidInjector
    abstract AdminMainActivity adminMainActivity();

    @ContributesAndroidInjector
    abstract MainActivity mainActivity();

    @ContributesAndroidInjector
    abstract Main2Activity main2Activity();

    @ContributesAndroidInjector
    abstract LoginActivity loginActivity();

    @ContributesAndroidInjector
    abstract OtpActivity otpActivity();

}
