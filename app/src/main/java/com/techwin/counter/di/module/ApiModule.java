package com.techwin.counter.di.module;

import com.techwin.counter.data.remote.api.MainApi;
import com.techwin.counter.di.qualifier.ApiEndpoint;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ApiModule {

    @Singleton
    @Provides
    static MainApi welcomeApi(@ApiEndpoint Retrofit retrofit) {
        return retrofit.create(MainApi.class);
    }


}
