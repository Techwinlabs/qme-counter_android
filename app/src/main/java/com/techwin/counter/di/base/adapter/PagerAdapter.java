package com.techwin.counter.di.base.adapter;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.Collections;
import java.util.List;

/**
 * Use for static number of fragments
 * Fragments are kept in memory
 */
public class PagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragmentList;
    private float pageWidth;

    public PagerAdapter(FragmentManager fm, @Nullable List<Fragment> fragmentList) {
        super(fm);
        if (fragmentList == null) {
            fragmentList = Collections.emptyList();
        }

        this.fragmentList = fragmentList;
    }

    public PagerAdapter(FragmentManager fm, @Nullable List<Fragment> fragmentList, float pageWidth) {
        super(fm);
        if (fragmentList == null) {
            fragmentList = Collections.emptyList();
        }

        this.fragmentList = fragmentList;
        this.pageWidth = pageWidth;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public float getPageWidth(int position) {
        if (pageWidth > 0)
            return pageWidth;
        return super.getPageWidth(position);
    }
}

