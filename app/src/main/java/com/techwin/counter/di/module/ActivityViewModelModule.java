package com.techwin.counter.di.module;


import androidx.lifecycle.ViewModel;

import com.techwin.counter.di.mapkey.ViewModelKey;
import com.techwin.counter.ui.admin.card.otp.OtpActivityVM;
import com.techwin.counter.ui.admin.main.AdminMainActivityVM;
import com.techwin.counter.ui.login.LoginActivityVM;
import com.techwin.counter.ui.main.MainActivityVM;
import com.techwin.counter.ui.main.detail.DetailActivityVM;
import com.techwin.counter.ui.main.multi.Main2ActivityVM;
import com.techwin.counter.ui.reservation.ReservationActivityVM;
import com.techwin.counter.ui.reservation.detail.ReservationDetailActivityVM;
import com.techwin.counter.ui.welcome.WelcomeActivityVM;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ActivityViewModelModule {
    // NOTE: customize here

    @Binds
    @IntoMap
    @ViewModelKey(ReservationDetailActivityVM.class)
    abstract ViewModel ReservationDetailActivityVM(ReservationDetailActivityVM vm);
      @Binds
    @IntoMap
    @ViewModelKey(DetailActivityVM.class)
    abstract ViewModel DetailActivityVM(DetailActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(ReservationActivityVM.class)
    abstract ViewModel ReservationActivityVM(ReservationActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(OtpActivityVM.class)
    abstract ViewModel OtpActivityVM(OtpActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(WelcomeActivityVM.class)
    abstract ViewModel welcomeActivityVM(WelcomeActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(AdminMainActivityVM.class)
    abstract ViewModel AdminMainActivityVM(AdminMainActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityVM.class)
    abstract ViewModel MainActivityVM(MainActivityVM vm);

    @Binds
    @IntoMap
    @ViewModelKey(Main2ActivityVM.class)
    abstract ViewModel Main2ActivityVM(Main2ActivityVM vm);


    @Binds
    @IntoMap
    @ViewModelKey(LoginActivityVM.class)
    abstract ViewModel LoginActivityVM(LoginActivityVM vm);


}
