package com.techwin.counter.di.module.androidcomponent;


import com.techwin.counter.ui.admin.card.CardFragment;
import com.techwin.counter.ui.admin.history.HistoryFragment;
import com.techwin.counter.ui.admin.history.detail.HistoryDetailFragment;
import com.techwin.counter.ui.admin.manage.ManageFragment;
import com.techwin.counter.ui.admin.profile.ProfileFragment;
import com.techwin.counter.ui.admin.reservation.FutureFragment;
import com.techwin.counter.ui.admin.today.TodayFragment;
import com.techwin.counter.ui.admin.today.detail.TodayDetailFragment;
import com.techwin.counter.ui.admin.wallet.BalanceFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {
    // NOTE: user  here
    @ContributesAndroidInjector
    abstract CardFragment cardFragment();

    @ContributesAndroidInjector
    abstract BalanceFragment balanceFragment();

    @ContributesAndroidInjector
    abstract ManageFragment manageFragment();

    @ContributesAndroidInjector
    abstract ProfileFragment profileFragment();

    @ContributesAndroidInjector
    abstract TodayFragment todayFragment();

    @ContributesAndroidInjector
    abstract FutureFragment futureFragment();

    @ContributesAndroidInjector
    abstract TodayDetailFragment todayDetailFragment();

    @ContributesAndroidInjector
    abstract HistoryFragment historyFragment();

    @ContributesAndroidInjector
    abstract HistoryDetailFragment historyDetailFragment();
/*
    @ContributesAndroidInjector
    abstract VideoDetailFragment videoDetailFragment();

    @ContributesAndroidInjector
    abstract VideoFragment videoFragment();

    @ContributesAndroidInjector
    abstract InfoFragment infoFragment();

    @ContributesAndroidInjector
    abstract SearchFragment searchFragment();*/


}
