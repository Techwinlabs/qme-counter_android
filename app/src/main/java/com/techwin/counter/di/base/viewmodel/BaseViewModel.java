package com.techwin.counter.di.base.viewmodel;


import android.view.View;

import androidx.annotation.CallSuper;
import androidx.lifecycle.ViewModel;

import com.techwin.counter.util.event.SingleActionEvent;
import com.techwin.counter.util.event.SingleMessageEvent;

import io.reactivex.disposables.CompositeDisposable;

/**
 * This will serve as the base class for all Activity ViewModels
 * <p>
 */
public abstract class BaseViewModel extends ViewModel {
    public final String TAG=this.getClass().getSimpleName();
    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();
    public final SingleActionEvent<View> onClick = new SingleActionEvent<>();
    public final SingleMessageEvent normal = new SingleMessageEvent();
    public final SingleMessageEvent success = new SingleMessageEvent();
    public final SingleMessageEvent error = new SingleMessageEvent();
    public final SingleMessageEvent info = new SingleMessageEvent();
    public final SingleMessageEvent warn = new SingleMessageEvent();

    public void onClick(View view) {
        onClick.setValue(view);
    }

    @CallSuper
    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }
}
