package com.techwin.counter.di.module;


import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.api.MainApi;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.data.repo.MainRepoImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Singleton
    @Provides
    static MainRepo mainRepo(SharedPref pref, MainApi mainApi) {
        return new MainRepoImpl(pref, mainApi);
    }
}
