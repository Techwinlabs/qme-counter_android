package com.techwin.counter;

import android.content.Intent;

import com.techwin.counter.di.base.BaseApplication;
import com.techwin.counter.ui.welcome.WelcomeActivity;
import com.techwin.counter.util.misc.AppVisibilityDetector;

public class MyApplication extends BaseApplication {
    private static final String TAG = MyApplication.class.getSimpleName();
    private static MyApplication application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        AppVisibilityDetector.init(this, new AppVisibilityDetector.AppVisibilityCallback() {
            @Override
            public void onAppGotoForeground() {

              /*  if (sharedPref.contains("userdata")) {
                    long ct = System.currentTimeMillis() / 1000;
                    if (lastonlinetime != -1 && ct - lastonlinetime < DEFAULT_ONLINE_TIME_THRESHOLD) {
                        lastonlinetime = ct;
                    } else {
                        startActivity(SecurityActivity.newIntent(application));
                    }
                }*/
            }

            @Override
            public void onAppGotoBackground() {
                // lastonlinetime = System.currentTimeMillis() / 1000;
            }
        });


    }


    public static MyApplication getInstance() {
        return application;
    }

    public void restartApp() {
        Intent intent = WelcomeActivity.newIntent(this);
        intent.putExtra("reset",true);
        startActivity(intent);

    }


}
