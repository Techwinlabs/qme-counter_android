package com.techwin.counter.ui.admin.wallet;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techwin.counter.data.beans.InvoiceBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class BalanceFragmentVM extends BaseViewModel {

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    public final SingleRequestEvent<List<InvoiceBean>> obrData = new SingleRequestEvent<>();
    public final SingleRequestEvent<String> obrBalance = new SingleRequestEvent<>();
    private final MainRepo mainRepo;

    @Inject
    public BalanceFragmentVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, MainRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.mainRepo = baseRepo;
    }

    public void getWallet() {
        mainRepo.getWallet()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrBalance.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        if (simpleApiResponse.isJsonObject()) {
                            JsonObject object = simpleApiResponse.getAsJsonObject();
                            float s = object.get("amount").getAsFloat();
                            obrBalance.setValue(Resource.success(String.valueOf(s), "Data"));
                        }


                    }

                    @Override
                    public void onError(Throwable e) {
                        obrBalance.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }

    public void getInvoices() {
        mainRepo.Invoices()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<InvoiceBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrData.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull List<InvoiceBean> simpleApiResponse) {

                        obrData.setValue(Resource.success(simpleApiResponse, "Data"));


                    }

                    @Override
                    public void onError(Throwable e) {
                        obrData.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }
    /*

    public void recieve_and_send_money() {
        mainRepo.recieve_and_send_money()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ApiResponse<List<TransBean>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrData.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(ApiResponse<List<TransBean>> simpleApiResponse) {
                        if (simpleApiResponse.isStatusOK())
                            obrData.setValue(Resource.success(simpleApiResponse.getData(), simpleApiResponse.getMessage()));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrData.setValue(Resource.error(
                                null,
                                networkErrorHandler.getErrMsg(e)));

                    }
                });

    }*/
}
