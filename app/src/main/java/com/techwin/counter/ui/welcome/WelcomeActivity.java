package com.techwin.counter.ui.welcome;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.UserBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.databinding.ActivityWelcomeBinding;
import com.techwin.counter.di.base.view.AppActivity;
import com.techwin.counter.ui.admin.card.otp.OtpActivity;
import com.techwin.counter.ui.login.LoginActivity;
import com.techwin.counter.ui.main.MainActivity;
import com.techwin.counter.ui.main.multi.Main2Activity;
import com.techwin.counter.util.event.SingleRequestEvent;

import javax.inject.Inject;

public class WelcomeActivity extends AppActivity<ActivityWelcomeBinding, WelcomeActivityVM> {

    @Inject
    SharedPref sharedPref;

    @Override
    protected BindingActivity<WelcomeActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_welcome, WelcomeActivityVM.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, WelcomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void subscribeToEvents(final WelcomeActivityVM vm) {
      //  setDefaultLocale();
        if (getIntent().getBooleanExtra("reset", false))
            sharedPref.deleteAll();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (sharedPref.getUserData() == null) {
                    Intent intent = LoginActivity.newIntent(WelcomeActivity.this);
                    startNewActivity(intent, true);
                } else {
                    if (sharedPref.getUserData().numberOfServices > 1) {
                        Intent intent = Main2Activity.newIntent(WelcomeActivity.this);
                        startNewActivity(intent, true);
                    } else {
                        UserBean userBean = sharedPref.getUserData();
                        if (userBean.services != null && userBean.services.size() > 0) {
                            Intent intent = MainActivity.newIntent(WelcomeActivity.this, userBean.services.get(0).serviceId);
                            startNewActivity(intent, true);
                        } else {
                            vm.error.setValue("No Service");
                        }
                    }
                }
            }
        }, 500);

    }

   /* private void setDefaultLocale() {
        Locale myLocale = new Locale(sharedPref.getDefaultLocale(Constants.DEFAULTLOCALE));
        Resources res = getResources();
        Locale.setDefault(myLocale);
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(myLocale);
        res.updateConfiguration(conf, dm);
        Log.e(TAG, "setDefaultLocale" + conf.locale.getDisplayName());
    }*/

}

