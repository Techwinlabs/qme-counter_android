package com.techwin.counter.ui.admin.calender;

import java.io.Serializable;

@SuppressWarnings("serial")
public class DayEntityModel implements Serializable {

	public String date; //yyyy-mm-dd
	public String day;
	public String year;
	public String month;


	public boolean monthEvent;

	public boolean isChecked;
	public boolean isCurrentDay;
	public boolean isCurrentMonthDay;
	public boolean isPastDay;
	public boolean check_value = false;

}
