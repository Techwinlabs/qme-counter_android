package com.techwin.counter.ui.admin.main;


import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import javax.inject.Inject;

public class AdminMainActivityVM extends BaseViewModel {

    final SingleRequestEvent<String> obrCallTicket = new SingleRequestEvent<>();
    final SingleRequestEvent<Void> obrLogout = new SingleRequestEvent<>();
    final SingleRequestEvent<Void> obrReset = new SingleRequestEvent<>();

    private final MainRepo mainRepo;

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;


    @Inject
    public AdminMainActivityVM(MainRepo mainRepo, SharedPref sharedPref, NetworkErrorHandler networkErrorHandler) {
        this.mainRepo = mainRepo;
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
    }

/*

    void loadCounter(int serviceId) {
        mainRepo.counter(serviceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CounterBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrData.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull CounterBean simpleApiResponse) {
                        obrData.setValue(Resource.success(simpleApiResponse, "counter"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrData.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });

    }

    void loadVisits(int serviceId, int branchId) {
        mainRepo.getCustomersData(serviceId, branchId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<List<CustomerDataBean>, List<CustomerDataBean>>() {
                    @Override
                    public List<CustomerDataBean> apply(@NonNull List<CustomerDataBean> customerDataBeans) throws Exception {
                        if (customerDataBeans.size() > 0)
                            customerDataBeans.get(0).show = true;
                        return customerDataBeans;
                    }
                })
                .subscribe(new SingleObserver<List<CustomerDataBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrVisits.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull List<CustomerDataBean> simpleApiResponse) {
                        if (simpleApiResponse.size() > 0)
                            obrVisits.setValue(Resource.success(simpleApiResponse, "Data"));
                        else obrVisits.setValue(Resource.warn(simpleApiResponse, "No Data"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrVisits.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });

    }

    void newTicket(int serviceId, String name, String phone) {
        if (InputUtils.isEmpty(phone)) {
            warn.setValue("Phone empty");
            return;
        }

        JSONObject map = new JSONObject();
        try {
            map.put("serviceId", serviceId);
            map.put("customerName", name);
            map.put("customerPhoneNumber", phone);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        mainRepo.issueTicket(map.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<NewTicketBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrNewTicket.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull NewTicketBean simpleApiResponse) {
                        obrNewTicket.setValue(Resource.success(simpleApiResponse, simpleApiResponse.message));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrNewTicket.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });

    }

    void callTicket(int serviceId, String pin) {
        if (InputUtils.isEmpty(pin)) {
            warn.setValue("Pin empty");
            return;
        }

        JSONObject map = new JSONObject();
        try {
            map.put("serviceId", serviceId);
            map.put("pin", pin);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        mainRepo.callTicket(map.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                        compositeDisposable.add(d);
                        obrCallTicket.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        obrCallTicket.setValue(Resource.success(pin, "Call ticket successful"));
                    }

                    @Override
                    public void onError(@NotNull Throwable throwable) {

                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrCallTicket.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_NO_CONTENT:
                                    obrCallTicket.setValue(Resource.success(pin, "Call ticket successful"));
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                    obrCallTicket.setValue(Resource.warn(null, networkErrorHandler.getErrorMessage(throwable)));
                                    break;
                            }
                        } else if (throwable instanceof IOException) {
                            if (throwable instanceof RequestInterceptor.NoContentException) {
                                obrCallTicket.setValue(Resource.success(pin, "Call ticket successful"));
                            } else
                                obrCallTicket.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                        } else {
                            if (throwable.getMessage() != null)
                                obrCallTicket.setValue(Resource.error(null, throwable.getMessage()));
                        }


                    }
                });

    }

    void logout(String pin) {
        if (InputUtils.isEmpty(pin)) {
            warn.setValue("Pin empty");
            return;
        }

        JSONObject map = new JSONObject();
        try {
            map.put("pin", pin);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        mainRepo.logout(map.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                        compositeDisposable.add(d);
                        obrLogout.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {

                    }

                    @Override
                    public void onError(@NotNull Throwable throwable) {

                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrLogout.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_NO_CONTENT:
                                    obrLogout.setValue(Resource.success(null, "Logout successful"));
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                    obrLogout.setValue(Resource.warn(null, networkErrorHandler.getErrorMessage(throwable)));
                                    break;
                            }
                        } else if (throwable instanceof IOException) {
                            if (throwable instanceof RequestInterceptor.NoContentException) {
                                obrLogout.setValue(Resource.success(null, "Logout successful"));
                            } else
                                obrLogout.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                        } else {
                            if (throwable.getMessage() != null)
                                obrLogout.setValue(Resource.error(null, throwable.getMessage()));
                        }


                    }
                });

    }
*/
}
