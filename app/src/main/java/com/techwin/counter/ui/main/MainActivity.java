package com.techwin.counter.ui.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.google.gson.JsonElement;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.CallTicketbean;
import com.techwin.counter.data.beans.CounterBean;
import com.techwin.counter.data.beans.NewTicketBean;
import com.techwin.counter.data.beans.UserBean;
import com.techwin.counter.data.beans.base.SimpleApiResponse;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.Status;
import com.techwin.counter.databinding.ActivityMainBinding;
import com.techwin.counter.databinding.DialogAddBinding;
import com.techwin.counter.databinding.DialogAuthBinding;
import com.techwin.counter.databinding.DialogCounterBinding;
import com.techwin.counter.di.base.view.AppActivity;
import com.techwin.counter.di.base.view.BaseCustomDialog;
import com.techwin.counter.ui.admin.main.AdminMainActivity;
import com.techwin.counter.ui.login.LoginActivity;
import com.techwin.counter.ui.main.detail.DetailActivity;
import com.techwin.counter.ui.reservation.ReservationActivity;
import com.techwin.counter.util.dump.InputUtils;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppActivity<ActivityMainBinding, MainActivityVM> {
    @Nullable
    private BaseCustomDialog<DialogAuthBinding> dialogAuth;
    @Nullable
    private BaseCustomDialog<DialogAddBinding> dialogAdd;
    @Nullable
    private BaseCustomDialog<DialogCounterBinding> dialogTimer;
    @Nullable
    private CountDownTimer countDownTimer;
    @Inject
    SharedPref sharedPref;

    private int serviceId = 0;
    private String branchId = "0";


    @Override
    protected BindingActivity<MainActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_main, MainActivityVM.class);
    }

    /*  public static Intent newIntent(Activity activity) {
          Intent intent = new Intent(activity, MainActivity.class);
          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
          return intent;
      }
  */
    public static Intent newIntent(Context activity, int serviceId) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra("serviceId", serviceId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void subscribeToEvents(final MainActivityVM vm) {

        vm.obrRefreshToken.observe(this, (SingleRequestEvent.RequestObserver<JsonElement>) resource -> {
            /*if (resource.status == Status.SUCCESS)*/
                viewModel.loadCounter(serviceId);
        });


        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.btn_newticket) {
                    showNewDialog();
                } else if (view.getId() == R.id.btn_callticket) {
                    showAuthDialog(false);
                } else if (view.getId() == R.id.btn_resetticket) {
                    showAuthDialog(true);
                } else if (view.getId() == R.id.btn_logout) {
                    showAuthDialog();
                } else if (view.getId() == R.id.iv_list) {
                    Intent intent = DetailActivity.newIntent(MainActivity.this, serviceId);
                    startNewActivity(intent);
                } else if (view.getId() == R.id.iv_slot) {
                    Intent intent = ReservationActivity.newIntent(MainActivity.this, serviceId);
                    startNewActivity(intent);
                } else if (view.getId() == R.id.iv_admin) {
                    Intent intent = AdminMainActivity.newIntent(MainActivity.this);
                    startNewActivity(intent);
                }
            }
        });

        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<CounterBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    binding.setData(resource.data);
                    break;
                case ERROR:
                    vm.error.setValue(resource.message);
                    break;
            }
        });

        vm.obrNewTicket.observe(this, (SingleRequestEvent.RequestObserver<NewTicketBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (dialogAdd != null)
                        dialogAdd.dismiss();
                    vm.success.setValue(resource.message);
                    viewModel.loadCounter(serviceId);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    viewModel.loadCounter(serviceId);
                    break;
            }
        });
        vm.obrCallTicket.observe(this, (SingleRequestEvent.RequestObserver<CallTicketbean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    if (dialogAuth != null)
                        dialogAuth.dismiss();
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    viewModel.loadCounter(serviceId);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    if (dialogAuth != null)
                        dialogAuth.show();
                    viewModel.loadCounter(serviceId);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    showCounterDialog(resource.data);
                    viewModel.loadCounter(serviceId);
                    break;
            }
        });
        vm.obrReschedule.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    if (dialogAuth != null)
                        dialogAuth.dismiss();
                    showProgressDialog(R.string.plz_wait);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    /*if (dialogTimer != null)
                        dialogTimer.dismiss();*/
                    viewModel.loadCounter(serviceId);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
            }
        });
        vm.obrLogout.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
                case SUCCESS:
                    sharedPref.deleteAll();
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    Intent intent = LoginActivity.newIntent(MainActivity.this);
                    startNewActivity(intent, true);
                    break;
            }
        });
        vm.obrReset.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    if (dialogAuth != null)
                        dialogAuth.dismiss();
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    viewModel.loadCounter(serviceId);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    if (dialogAuth != null)
                        dialogAuth.show();
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    viewModel.loadCounter(serviceId);
                    break;
            }
        });

        init();

    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshToken();
    }

    private void refreshToken() {
        try {
            UserBean userBean = sharedPref.getUserData();
            JSONObject object = new JSONObject();
            object.put("refreshToken", userBean.refreshToken);
            viewModel.refreshToken(object.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void getListOfReservation() {
        viewModel.loadReservations(serviceId/*Integer.parseInt(branchId)*/);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //    viewModel.loadCounter(serviceId);
    }

    private void showAuthDialog(boolean b) {
        dialogAuth = new BaseCustomDialog<>(this, R.layout.dialog_auth, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.btn_callticket) {
                    String pin = dialogAuth.getBinding().otpView.getOTP();
                    if (pin.length() > 2) {
                        if (b)
                            viewModel.resetTicket(serviceId, pin);
                        else {
                            // viewModel.callNext(pin);
                            viewModel.callTicket(serviceId, pin);
                        }
                    }
                }
            }
        });
        if (b)
            dialogAuth.getBinding().btnCallticket.setText(R.string.reset_queue);
        dialogAuth.show();
        Window window = dialogAuth.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }

    private void showAuthDialog() {
        dialogAuth = new BaseCustomDialog<>(this, R.layout.dialog_auth, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.btn_callticket) {
                    String pin = dialogAuth.getBinding().otpView.getOTP();
                    if (pin.length() > 2) {
                        viewModel.logout(pin);
                    }
                }
            }
        });
        dialogAuth.getBinding().btnCallticket.setText(R.string.logout);
        dialogAuth.show();
        Window window = dialogAuth.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }

    private void showNewDialog() {
        dialogAdd = new BaseCustomDialog<>(this, R.layout.dialog_add, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.submit) {
                    String name = InputUtils.getTrimString(dialogAdd.getBinding().name.getText().toString());
                    String phone = InputUtils.getTrimString(dialogAdd.getBinding().etPhone.getText().toString());
                    String cc = InputUtils.getTrimString(dialogAdd.getBinding().ccp.getFullNumberWithPlus());

                    if (TextUtils.isEmpty(phone)) {
                        viewModel.error.setValue("Phone empty");
                        return;
                    }

                    viewModel.newTicket(serviceId, name, cc + phone, dialogAdd.getBinding().code.getText().toString());
                }
            }
        });
        dialogAdd.show();
        dialogAdd.getBinding().ccp.setDefaultCountryUsingNameCodeAndApply("EG");
        Window window = dialogAdd.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }

    private void showCounterDialog(CallTicketbean data) {
        if (dialogTimer != null)
            dialogTimer.dismiss();
        dialogTimer = new BaseCustomDialog<>(this, R.layout.dialog_counter, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.btn_next && dialogTimer.getBinding().getNext()) {
                    //  viewModel.callNext(data);
                    viewModel.callTicket(serviceId, data.pin);
                } else if (view.getId() == R.id.btn_showup) {
                    if (countDownTimer != null)
                        countDownTimer.cancel();
                    dialogTimer.dismiss();
                } else if (view.getId() == R.id.btn_postpone) {
                    viewModel.postpone(serviceId);
                }
            }
        });
        dialogTimer.getBinding().setNext(false);
        dialogTimer.setCancelable(true);
        dialogTimer.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {

            }
        });

        if (countDownTimer != null)
            countDownTimer.cancel();

        countDownTimer = new CountDownTimer(20000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int t = (int) (millisUntilFinished / 1000);
                int progress = (int) ((20000 - millisUntilFinished) / 1000);
                dialogTimer.getBinding().tvTime.setText(String.valueOf(t));
                dialogTimer.getBinding().circularProgressBar.setProgress(progress);
            }

            @Override
            public void onFinish() {
                dialogTimer.getBinding().setNext(true);
            }
        };
        countDownTimer.start();

        dialogTimer.show();
        dialogTimer.getBinding().setCurrentticket(data.ticketNumber);
        Window window = dialogTimer.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    protected void onDestroy() {
        if (countDownTimer != null)
            countDownTimer.cancel();
        super.onDestroy();
    }


    private void init() {
        serviceId = getIntent().getIntExtra("serviceId", 0);
        UserBean userBean = sharedPref.getUserData();
        if (userBean == null) return;
        if (serviceId != 0) {
            branchId = sharedPref.get("branch", "0");
            UserBean.Services services = null;
            for (int i = 0; i < userBean.services.size(); i++) {
                if (userBean.services.get(i).serviceId == serviceId) {
                    services = userBean.services.get(i);
                    break;
                }
            }
            if (services != null) {
                //binding.setLogo(services.counterData.logoUrl);
                binding.setLogo(services.logoUrl);
            }
        } else
            binding.setLogo(userBean.logo);
    }

    private List<SimpleApiResponse> getSlotList() {
        List<SimpleApiResponse> data = new ArrayList<>();
        data.add(new SimpleApiResponse());
        data.add(new SimpleApiResponse());
        data.add(new SimpleApiResponse());
        data.add(new SimpleApiResponse());
        data.add(new SimpleApiResponse());
        data.add(new SimpleApiResponse());
        return data;
    }

}

