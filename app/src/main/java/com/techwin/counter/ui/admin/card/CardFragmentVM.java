package com.techwin.counter.ui.admin.card;


import com.google.gson.JsonElement;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;

import java.net.HttpURLConnection;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class CardFragmentVM extends BaseViewModel {

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    public final SingleRequestEvent<JsonElement> obrPay = new SingleRequestEvent<>();
    public final SingleRequestEvent<Void> obrOtp = new SingleRequestEvent<>();

    private final MainRepo mainRepo;

    @Inject
    public CardFragmentVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, MainRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.mainRepo = baseRepo;
    }


    public void pay(String body) {
        mainRepo.pay(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrPay.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        obrPay.setValue(Resource.success(simpleApiResponse, "Data"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        obrPay.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }

    public void confirmOtp(String body) {
        mainRepo.confirmOtp(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrOtp.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        obrOtp.setValue(Resource.success(null, "Data"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (networkErrorHandler.getStatusCode(e) == HttpURLConnection.HTTP_NO_CONTENT) {
                            obrOtp.setValue(Resource.success(null, "done"));
                        } else
                            obrOtp.setValue(Resource.error(
                                    null, networkErrorHandler.resolveError(e)));

                    }
                });
    }
}
