package com.techwin.counter.ui.admin.wallet;

import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;

import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.CardBean;
import com.techwin.counter.data.beans.InvoiceBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.databinding.FragmentBalanceBinding;
import com.techwin.counter.databinding.HolderCardsBinding;
import com.techwin.counter.databinding.HolderInvociesBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppFragment;
import com.techwin.counter.util.decoration.ListVerticalItemDecoration;
import com.techwin.counter.util.event.SingleRequestEvent;
import com.techwin.counter.util.misc.DefaultListChangeCallback;

import java.util.List;

import javax.inject.Inject;

public class BalanceFragment extends AppFragment<FragmentBalanceBinding, BalanceFragmentVM> {
    public static final String TAG = "BalanceFragment";
    @Inject
    public SharedPref sharedPref;
    private SimpleRecyclerViewAdapter<InvoiceBean, HolderInvociesBinding> adapterInvoice;
    private SimpleRecyclerViewAdapter<CardBean, HolderCardsBinding> adapterCard;

    @Override
    protected BindingFragment<BalanceFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_balance, BalanceFragmentVM.class);
    }


    @Override
    protected void subscribeToEvents(final BalanceFragmentVM vm) {
        binding.header.ivSearch.setVisibility(View.GONE);
        setInvoceAdapter();
        setCardAdapter();
        vm.obrBalance.observe(this, (SingleRequestEvent.RequestObserver<String>) resource -> {
            switch (resource.status) {
                case LOADING:
                    binding.balance.setText(null);
                    // showLoading();
                    break;
                case SUCCESS:
                    binding.balance.setText(resource.data);
                    break;
                case ERROR:
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<List<InvoiceBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    break;
                case SUCCESS:
                    adapterInvoice.setList(resource.data);
                    break;
                case ERROR:
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.tv_add) {
                    Bundle args = new Bundle();
                    args.putSerializable("addcard", true);
                    Navigation.findNavController(view).navigate(R.id.cardFragment, args);
                }
            }
        });

    }

    private void setCardAdapter() {
        adapterCard = new SimpleRecyclerViewAdapter<>(R.layout.holder_cards, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<CardBean>() {
            @Override
            public void onItemClick(View v, CardBean cardBean) {

            }

            @Override
            public void onPositionClick(View v, int pos) {
                sharedPref.removeCard(adapterCard.getData(pos));
                adapterCard.removeItem(pos);
            }
        });
        binding.rvCard.addItemDecoration(new ListVerticalItemDecoration(baseContext, R.dimen.dp1));
        binding.rvCard.setAdapter(adapterCard);
        adapterCard.getDataList().addOnListChangedCallback(new DefaultListChangeCallback<CardBean>() {
            @Override
            public void onSizeUpdated(int size) {
                if (size > 0) {
                    binding.rvCard.setVisibility(View.VISIBLE);
                    binding.tvCards.setVisibility(View.VISIBLE);
                } else {
                    binding.rvCard.setVisibility(View.GONE);
                    binding.tvCards.setVisibility(View.GONE);
                }
            }
        });
    }


    private void setInvoceAdapter() {
        adapterInvoice = new SimpleRecyclerViewAdapter<>(R.layout.holder_invocies, BR.bean, (v, invoiceBean) -> {
            if (v.getId() == R.id.btn_pay) {
                Bundle args = new Bundle();
                args.putSerializable("data", invoiceBean);
                Navigation.findNavController(v).navigate(R.id.cardFragment, args);
            }
        });
        binding.rvOne.setAdapter(adapterInvoice);
    }

    @Override
    public void onStart() {
        super.onStart();
        adapterCard.setList(sharedPref.getCards());
        viewModel.getWallet();
        viewModel.getInvoices();
        ;
    }
}
