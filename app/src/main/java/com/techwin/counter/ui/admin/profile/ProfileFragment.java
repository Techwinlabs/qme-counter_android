package com.techwin.counter.ui.admin.profile;

import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;

import com.techwin.counter.R;
import com.techwin.counter.data.beans.ProfileBean;
import com.techwin.counter.data.beans.UserBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.databinding.DialogAuthBinding;
import com.techwin.counter.databinding.FragmentProfileBinding;
import com.techwin.counter.databinding.SheetMoreLinksBinding;
import com.techwin.counter.di.base.view.AppFragment;
import com.techwin.counter.di.base.view.BaseCustomDialog;
import com.techwin.counter.di.base.view.BaseSheet;
import com.techwin.counter.ui.login.LoginActivity;
import com.techwin.counter.ui.main.MainActivity;
import com.techwin.counter.ui.main.multi.Main2Activity;
import com.techwin.counter.util.Constants;
import com.techwin.counter.util.event.SingleRequestEvent;

import javax.inject.Inject;

public class ProfileFragment extends AppFragment<FragmentProfileBinding, ProfileFragmentVM> {

    public static final String TAG = "ProfileFragment";
    @Inject
    SharedPref sharedPref;


    @Override
    protected BindingFragment<ProfileFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_profile, ProfileFragmentVM.class);
    }


    @Override
    protected void subscribeToEvents(final ProfileFragmentVM vm) {

        binding.header.ivSearch.setVisibility(View.GONE);
        vm.mainActivityVM.obrLogout.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
                case SUCCESS:
                    sharedPref.deleteAll();
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    Intent intent = LoginActivity.newIntent(baseContext);
                    startNewActivity(intent, true);
                    break;
            }
        });

        vm.obrProfileData.observe(this, (SingleRequestEvent.RequestObserver<ProfileBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    break;
                case SUCCESS:
                    if (resource.data != null) {
                        binding.setBean(resource.data);
                        if (resource.data.address != null && resource.data.address.street != null) {
                            String locale = sharedPref.getDefaultLocale(Constants.DEFAULTLOCALE);
                            if (locale.equals(Constants.ENGLOCALE)) {
                                binding.setAddress(resource.data.address.street.en);
                            } else {
                                binding.setAddress(resource.data.address.street.ar);
                            }
                        } else binding.setAddress(null);
                    }
                    break;
                case ERROR:
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {

            }
        });
        showMoreSheet();
    }

    @Override
    public void onStart() {
        super.onStart();
        viewModel.getProfile();
    }

    private BaseSheet<SheetMoreLinksBinding> sheet;

    private void showMoreSheet() {
        sheet = new BaseSheet<>(R.layout.sheet_more_links, new BaseSheet.ClickListener() {
            @Override
            public void onClick(@NonNull View view) {
                sheet.dismissAllowingStateLoss();
                if (view.getId() == R.id.ll_billing_balance) {
                    Navigation.findNavController(getActivity(), R.id.header).navigate(R.id.balanceFragment);
                } else if (view.getId() == R.id.ll_profile) {

                } else if (view.getId() == R.id.ll_switch) {
                    if (sharedPref.getUserData().numberOfServices > 1) {
                        Intent intent = Main2Activity.newIntent(baseContext);
                        startNewActivity(intent, true);
                    } else {
                        UserBean userBean = sharedPref.getUserData();
                        if (userBean.services != null && userBean.services.size() > 0) {
                            Intent intent = MainActivity.newIntent(baseContext, userBean.services.get(0).serviceId);
                            startNewActivity(intent, true);
                        } else {
                            viewModel.error.setValue("No Service");
                        }
                    }
                } else if (view.getId() == R.id.ll_logout) {
                    showAuthDialog();
                }
            }

            @Override
            public void onViewCreated() {

            }
        });
        sheet.show(getChildFragmentManager(), sheet.getTag());
    }


    private BaseCustomDialog<DialogAuthBinding> dialogAuth;

    private void showAuthDialog() {
        dialogAuth = new BaseCustomDialog<>(baseContext, R.layout.dialog_auth, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.btn_callticket) {
                    String pin = dialogAuth.getBinding().otpView.getOTP();
                    if (pin.length() > 2) {
                        viewModel.mainActivityVM.logout(pin);
                    }
                }
            }
        });
        dialogAuth.getBinding().btnCallticket.setText(R.string.logout);
        dialogAuth.show();
        Window window = dialogAuth.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }
}
