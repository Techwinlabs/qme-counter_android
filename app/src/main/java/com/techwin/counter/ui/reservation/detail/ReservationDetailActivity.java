package com.techwin.counter.ui.reservation.detail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.GetReservationByDateBean;
import com.techwin.counter.data.beans.base.AvailableSlotsBean;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.databinding.ActivityReservationDetailBinding;
import com.techwin.counter.databinding.DialogCancelBookingBinding;
import com.techwin.counter.databinding.DialogRegisterBinding;
import com.techwin.counter.databinding.HolderAvailableSlotBinding;
import com.techwin.counter.databinding.ViewBookedReservationsBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppActivity;
import com.techwin.counter.di.base.view.BaseCustomDialog;
import com.techwin.counter.util.event.SingleRequestEvent;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class ReservationDetailActivity extends AppActivity<ActivityReservationDetailBinding, ReservationDetailActivityVM> {


    public int serviceId = 0;
    private int reservationId = 0;
    private String date;
    private DialogRegisterBinding dialogBinding;
    private BaseCustomDialog<DialogCancelBookingBinding> cancelBookingDailog;
    private SimpleRecyclerViewAdapter<GetReservationByDateBean, ViewBookedReservationsBinding> adapterSlotByTime;
    private SimpleRecyclerViewAdapter<AvailableSlotsBean, HolderAvailableSlotBinding> slotAdapter;
    private AvailableSlotsBean selectedSlotBean;

    @Override
    protected BindingActivity<ReservationDetailActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_reservation_detail, ReservationDetailActivityVM.class);
    }

    public static Intent newIntent(Context activity, int serviceId, String date) {
        Intent intent = new Intent(activity, ReservationDetailActivity.class);
        intent.putExtra("serviceId", serviceId);
        intent.putExtra("date", date);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    protected void subscribeToEvents(final ReservationDetailActivityVM vm) {
        adapterSlotByTime = new SimpleRecyclerViewAdapter<>(R.layout.view_booked_reservations, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<GetReservationByDateBean>() {
            @Override
            public void onItemClick(View v, GetReservationByDateBean customerDataBean) {
                if (v.getId() == R.id.ivCancelBooking) {
                    showCancelDialog();
                    reservationId = customerDataBean.getId();
                }

            }
        });
        binding.rvSlotsByTime.setLayoutManager(new LinearLayoutManager(this));
        binding.rvSlotsByTime.setAdapter(adapterSlotByTime);

        vm.onClick.observe(this, new Observer<View>() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public void onChanged(View view) {
                switch (view.getId()) {
                    case R.id.iv_add:
                        //    showNewDialog();
                        break;

                }


            }
        });

        vm.obrAvailableSlot.observe(this, new SingleRequestEvent.RequestObserver<List<AvailableSlotsBean>>() {
            @Override
            public void onRequestReceived(@NonNull Resource<List<AvailableSlotsBean>> resource) {
                switch (resource.status) {
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case SUCCESS:
                        dismissProgressDialog();
                        setAvailableSlotsRV(resource.data);
                        break;
                    case WARN:
                        dismissProgressDialog();
                        vm.warn.setValue(resource.message);
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        vm.error.setValue(resource.message);
                        break;
                }
            }
        });

        vm.obrBook.observe(this, new SingleRequestEvent.RequestObserver<Void>() {
            @Override
            public void onRequestReceived(@NonNull Resource<Void> resource) {
                switch (resource.status) {
                    case SUCCESS:
                        dismissProgressDialog();
                        vm.success.setValue(resource.message);
                        break;
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case WARN:
                        dismissProgressDialog();
                        vm.warn.setValue(resource.message);
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        vm.error.setValue(resource.message);
                        break;


                }
            }


        });

        vm.obrCancelBooking.observe(this, new Observer<Resource<Void>>() {
            @Override
            public void onChanged(Resource<Void> resource) {
                switch (resource.status) {
                    case SUCCESS:
                        dismissProgressDialog();
                        if (cancelBookingDailog != null)
                            cancelBookingDailog.cancel();
                        viewModel.getReservationById(serviceId, getIntent().getStringExtra("date"));
                        vm.success.setValue(resource.message);
                        break;
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case WARN:
                        dismissProgressDialog();
                        if (cancelBookingDailog != null)
                            cancelBookingDailog.cancel();
                        vm.warn.setValue(resource.message);
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        if (cancelBookingDailog != null)
                            cancelBookingDailog.cancel();
                        vm.error.setValue(resource.message);
                        break;


                }
            }
        });
        vm.reservationByDateEvent.observe(this, (SingleRequestEvent.RequestObserver<List<GetReservationByDateBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    adapterSlotByTime.setList(resource.data);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });

        init();

    }

    private void showCancelDialog() {
        cancelBookingDailog = new BaseCustomDialog<>(this, R.layout.dialog_cancel_booking, new BaseCustomDialog.Listener() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public void onViewClick(View view) {
                switch (view.getId()) {
                    case R.id.tvYes:
                        viewModel.cancelBooking(serviceId, reservationId);
                        break;
                    case R.id.tvNo:
                        cancelBookingDailog.cancel();
                        break;
                }
            }
        });
        cancelBookingDailog.show();

    }

    private void setAvailableSlotsRV(List<AvailableSlotsBean> mBean) {
        if (dialogBinding != null) {
            slotAdapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_available_slot, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<AvailableSlotsBean>() {

                @Override
                public void onItemClick(View v, AvailableSlotsBean bean) {

                }

                @SuppressLint({"NotifyDataSetChanged"})
                @Override
                public void onClickPos(View v, AvailableSlotsBean bean, int pos) {
                    if (v.getId() == R.id.clParent) {
                        for (int i = 0; i < slotAdapter.getItemCount(); i++) {
                            Objects.requireNonNull(slotAdapter.getData(i)).setChecked(Objects.requireNonNull(slotAdapter.getData(i)).getStartTime().equals(bean.getStartTime()));

                        }
                        selectedSlotBean = bean;
                        slotAdapter.notifyItemRangeChanged(0, slotAdapter.getItemCount());
                    }
                }
            });
            dialogBinding.rvSlots.setAdapter(slotAdapter);
            slotAdapter.setList(mBean);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getListOfReservation();
    }

    private BaseCustomDialog<DialogRegisterBinding> dialogRegister;
    private Calendar calendar;

         /*
    private void showNewDialog() {
        calendar = Calendar.getInstance();
        if (dialogRegister != null)
            dialogRegister.dismiss();
        dialogRegister = new BaseCustomDialog<>(this, R.layout.dialog_register, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.submit) {
                    String name = InputUtils.getTrimString(dialogRegister.getBinding().name.getText().toString());
                    String phone = InputUtils.getTrimString(dialogRegister.getBinding().etPhone.getText().toString());
                    String cc = InputUtils.getTrimString(dialogRegister.getBinding().ccp.getFullNumberWithPlus());
                    String amount = InputUtils.getTrimString(dialogRegister.getBinding().emount.getText().toString());
                    // String time = InputUtils.getTrimString(dialogRegister.getBinding().etTime.getText().toString());

                    if (TextUtils.isEmpty(name)) {
                        dialogRegister.getBinding().name.setError("Empty name");
                        return;
                    }
                    if (TextUtils.isEmpty(phone)) {
                        dialogRegister.getBinding().etPhone.setError("Empty phone");
                        return;
                    }
                    if (TextUtils.isEmpty(amount)) {
                        dialogRegister.getBinding().emount.setError("Empty paid amount");
                        return;
                    }
                    if (TextUtils.isEmpty(time)) {
                        dialogRegister.getBinding().etTime.setError("Empty time");
                        return;
                    }


                    bookAPi(new SimpleDateFormat("HH", Locale.US).format(calendar.getTime()),
                            new SimpleDateFormat("mm", Locale.US).format(calendar.getTime()),
                            name, cc + phone, amount);
                    //  viewModel.newTicket(serviceId, name, cc + phone, dialogRegister.getBinding().code.getText().toString());
                } else if (view.getId() == R.id.et_time) {
                    DatePickerUtils.getInstance(ReservationDetailActivity.this).getDatePickerDialog(calendar, new DatePickerUtils.DateSetListener() {
                        @Override
                        public void onDateSet(@NonNull DatePickerDialog datePickerDialog, @NonNull Calendar ca) {
                            super.onDateSet(datePickerDialog, ca);
                            DatePickerUtils.getInstance(ReservationDetailActivity.this).getTimePickerDialog(ca, new DatePickerUtils.TimeSetListener() {
                                @Override
                                public void onTimeSet(@NonNull TimePickerDialog timePickerDialog, @NonNull Calendar ca) {
                                    super.onTimeSet(timePickerDialog, ca);
                                    calendar = ca;
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                                    dialogRegister.getBinding().etTime.setText(dateFormat.format(calendar.getTime()));
                                    Map<String, String> data = new HashMap<>();
                                    data.put("service_id", String.valueOf(serviceId));
                                    data.put("date", dialogBinding.etTime.getText().toString());
                                    viewModel.getAvailableSlot(data);
                                }
                            }, false).show();
                        }
                    }).show();

                }
            }
        });
        dialogRegister.show();
        Map<String, String> data = new HashMap<>();
        data.put("service_id", String.valueOf(serviceId));
        if (date.contains(String.valueOf(calendar.get(Calendar.DATE)))) {
            StringBuilder dateB = new StringBuilder(date);
            dateB.replace(11, 17, "10:50:");
            date = String.valueOf(dateB);
        }
        data.put("date", date);
        viewModel.getAvailableSlot(data);
        dialogBinding = dialogRegister.getBinding();
        dialogRegister.getBinding().ccp.setDefaultCountryUsingNameCodeAndApply("EG");
        Window window = dialogRegister.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }*/

         /*  private void bookAPi(String timeH, String timeM, String
            customerName, String phone, String amount) {
        //"date": "2022-01-13T10:42:32.564Z",
        //"timeH": 0,
        //"timeM": 0,
        //"serviceId": 0,
        //"customerName": "string",
        //"customerPhoneNumber": "string",
        //"paidAmount": 0


        try {
            JSONObject object = new JSONObject();
            object.put("date", date);
            object.put("timeH", selectedSlotBean.getStartTime().getHours());
            object.put("timeM", selectedSlotBean.getStartTime().getMinutes());
            object.put("customerName", customerName);
            object.put("customerPhoneNumber", phone);
            object.put("paidAmount", amount);
            object.put("serviceId", serviceId);
            viewModel.book(object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            viewModel.error.setValue(e.getMessage());
        }
    }
*/

    private void getListOfReservation() {
        viewModel.getReservationById(serviceId, getIntent().getStringExtra("date"));
    }


    private void init() {
        serviceId = getIntent().getIntExtra("serviceId", 0);
        date = getIntent().getStringExtra("date");
        binding.setDate(getIntent().getStringExtra("date"));
    }


}

