package com.techwin.counter.ui.main;


import android.util.Log;

import com.google.gson.JsonElement;
import com.techwin.counter.MyApplication;
import com.techwin.counter.data.beans.CallTicketbean;
import com.techwin.counter.data.beans.CounterBean;
import com.techwin.counter.data.beans.GetReservationBean;
import com.techwin.counter.data.beans.NewTicketBean;
import com.techwin.counter.data.beans.request.RescheduleBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.remote.intersepter.RequestInterceptor;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.dump.InputUtils;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class MainActivityVM extends BaseViewModel {
    final SingleRequestEvent<CounterBean> obrData = new SingleRequestEvent<>();

    final SingleRequestEvent<List<GetReservationBean>> reservationEvent = new SingleRequestEvent<>();

    final SingleRequestEvent<NewTicketBean> obrNewTicket = new SingleRequestEvent<>();
    final SingleRequestEvent<CallTicketbean> obrCallTicket = new SingleRequestEvent<>();
    final SingleRequestEvent<Void> obrReschedule = new SingleRequestEvent<>();
    public final SingleRequestEvent<Void> obrLogout = new SingleRequestEvent<>();
    final SingleRequestEvent<Void> obrReset = new SingleRequestEvent<>();
    public final SingleRequestEvent<JsonElement> obrRefreshToken = new SingleRequestEvent<>();

    private final MainRepo mainRepo;

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;

    @Inject
    public MainActivityVM(MainRepo mainRepo, SharedPref sharedPref, NetworkErrorHandler networkErrorHandler) {
        this.mainRepo = mainRepo;
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
    }


    void loadCounter(int serviceId) {
        mainRepo.counter(serviceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CounterBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrData.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull CounterBean simpleApiResponse) {
                        obrData.setValue(Resource.success(simpleApiResponse, "counter"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrData.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });

    }

    void loadReservations(int serviceId) {
        mainRepo.getReservationList(serviceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<GetReservationBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        reservationEvent.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull List<GetReservationBean> simpleApiResponse) {
                        if (simpleApiResponse.size() > 0)
                            reservationEvent.setValue(Resource.success(simpleApiResponse, "Data"));
                        else reservationEvent.setValue(Resource.warn(simpleApiResponse, "No Data"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        reservationEvent.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });

    }

    void newTicket(int serviceId, String name, String phone, String rescode) {
        if (InputUtils.isEmpty(phone)) {
            warn.setValue("Phone empty");
            return;
        }

        JSONObject map = new JSONObject();
        try {
            map.put("serviceId", serviceId);
            map.put("customerName", name);
            map.put("customerPhoneNumber", phone);
            if (rescode.length() > 0)
                map.put("reservationCode", rescode);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        mainRepo.issueTicket(map.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<NewTicketBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrNewTicket.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull NewTicketBean simpleApiResponse) {
                        obrNewTicket.setValue(Resource.success(simpleApiResponse, simpleApiResponse.message));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrNewTicket.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });

    }

    void callTicket(int serviceId, String pin) {
        if (InputUtils.isEmpty(pin)) {
            warn.setValue("Pin empty");
            return;
        }

        JSONObject map = new JSONObject();
        try {
            map.put("serviceId", serviceId);
            map.put("pin", pin);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        mainRepo.callTicket(map.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CallTicketbean>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                        compositeDisposable.add(d);
                        obrCallTicket.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull CallTicketbean simpleApiResponse) {
                        /*if (simpleApiResponse.succeed) {
                            simpleApiResponse.pin = pin;
                            obrCallTicket.setValue(Resource.success(simpleApiResponse, "Call ticket successful"));
                        } else {
                            simpleApiResponse.pin = pin;
                            obrCallTicket.setValue(Resource.warn(simpleApiResponse, "No tickets"));
                        }*/
                        simpleApiResponse.pin = pin;
                        obrCallTicket.setValue(Resource.success(simpleApiResponse, "Call ticket successful"));

                    }

                    @Override
                    public void onError(@NotNull Throwable throwable) {
                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrCallTicket.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                    obrCallTicket.setValue(Resource.warn(null, networkErrorHandler.getErrorMessage(throwable)));
                                    break;
                            }
                        }
                    }
                });

    }

    void postpone(int serviceId) {
        JSONObject map = new JSONObject();
        try {
            map.put("serviceId", serviceId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        /*mainRepo.reschedule(map.toString())*/
        mainRepo.rescheduleCopy(new RescheduleBean(serviceId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                        compositeDisposable.add(d);
                        obrReschedule.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        obrReschedule.setValue(Resource.success(null, "Ticket reschedule successful"));

                    }

                    @Override
                    public void onError(@NotNull Throwable throwable) {
                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrReschedule.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                    obrReschedule.setValue(Resource.warn(null, networkErrorHandler.getErrorMessage(throwable)));
                                    break;
                                default:
                                    obrReschedule.setValue(Resource.error(null, exception.message()));
                                    break;
                            }
                        }
                    }
                });

    }

    public void refreshToken(String body) {
        mainRepo.refreshToken(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrRefreshToken.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        obrRefreshToken.setValue(Resource.success(simpleApiResponse, "Data"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        obrRefreshToken.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }

    public void logout(String pin) {
        if (InputUtils.isEmpty(pin)) {
            warn.setValue("Pin empty");
            return;
        }

        JSONObject map = new JSONObject();
        try {
            map.put("pin", pin);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        mainRepo.logout(map.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                        compositeDisposable.add(d);
                        obrLogout.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {

                    }

                    @Override
                    public void onError(@NotNull Throwable throwable) {

                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrLogout.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_NO_CONTENT:
                                    obrLogout.setValue(Resource.success(null, "Logout successful"));
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                    obrLogout.setValue(Resource.warn(null, networkErrorHandler.getErrorMessage(throwable)));
                                    break;
                            }
                        } else if (throwable instanceof IOException) {
                            if (throwable instanceof RequestInterceptor.NoContentException) {
                                obrLogout.setValue(Resource.success(null, "Logout successful"));
                            } else
                                obrLogout.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                        } else {
                            if (throwable.getMessage() != null)
                                obrLogout.setValue(Resource.error(null, throwable.getMessage()));
                        }


                    }
                });

    }

    void callNext(int serviceId, String pin) {
        if (InputUtils.isEmpty(pin)) {
            warn.setValue("Pin empty");
            return;
        }

        JSONObject map = new JSONObject();
        try {
            map.put("serviceId", serviceId);
            map.put("pin", pin);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        mainRepo.CallNext(map.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                        compositeDisposable.add(d);
                        obrCallTicket.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {

                    }

                    @Override
                    public void onError(@NotNull Throwable throwable) {

                    /*    if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrCallTicket.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_NO_CONTENT:
                                    obrCallTicket.setValue(Resource.success(pin, "Call ticket successful"));
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                    obrCallTicket.setValue(Resource.warn(null, networkErrorHandler.getErrorMessage(throwable)));
                                    break;
                            }
                        } else if (throwable instanceof IOException) {
                            if (throwable instanceof RequestInterceptor.NoContentException) {
                                obrCallTicket.setValue(Resource.success(pin, "Call ticket successful"));
                            } else
                                obrCallTicket.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                        } else {
                            if (throwable.getMessage() != null)
                                obrCallTicket.setValue(Resource.error(null, throwable.getMessage()));
                        }

*/
                    }
                });

    }

    void resetTicket(int serviceId, String pin) {
        if (InputUtils.isEmpty(pin)) {
            warn.setValue("Pin empty");
            return;
        }

        JSONObject map = new JSONObject();
        try {
            map.put("serviceId", serviceId);
            map.put("pin", pin);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        mainRepo.reset(map.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                        compositeDisposable.add(d);
                        obrReset.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        Log.i("Text", ":::::" + simpleApiResponse);
                    }

                    @Override
                    public void onError(@NotNull Throwable throwable) {

                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrReset.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_NO_CONTENT:
                                    obrReset.setValue(Resource.success(null, "Reset successful"));
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                    obrReset.setValue(Resource.warn(null, networkErrorHandler.getErrorMessage(throwable)));
                                    break;
                                case HttpURLConnection.HTTP_RESET:
                                    obrReset.setValue(Resource.success(null, "Reset successful"));
                                    break;
                                case HttpURLConnection.HTTP_BAD_METHOD:
                                    obrReset.setValue(Resource.warn(null, networkErrorHandler.getErrorMessage(throwable)));
                                    break;
                            }
                        } else if (throwable instanceof IOException) {
                            if (throwable instanceof RequestInterceptor.NoContentException) {
                                obrReset.setValue(Resource.success(null, "Reset successful"));
                            } else
                                obrReset.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                        } else {
                            if (throwable.getMessage() != null)
                                obrReset.setValue(Resource.error(null, throwable.getMessage()));
                        }


                    }
                });

    }

}
