package com.techwin.counter.ui.reservation;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techwin.counter.MyApplication;
import com.techwin.counter.data.beans.GetQueueAvailability;
import com.techwin.counter.data.beans.GetReservationBean;
import com.techwin.counter.data.beans.base.AvailableSlotsBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class ReservationActivityVM extends BaseViewModel {
    public final SingleRequestEvent<Void> obrBook = new SingleRequestEvent<>();
    final SingleRequestEvent<List<GetReservationBean>> reservationEvent = new SingleRequestEvent<>();
    private final MainRepo mainRepo;
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    public final SingleRequestEvent<List<AvailableSlotsBean>> obrAvailableSlot = new SingleRequestEvent<>();
    public final SingleRequestEvent<GetQueueAvailability> obrGetQueueAvailability = new SingleRequestEvent<>();

    @Inject
    public ReservationActivityVM(MainRepo mainRepo, SharedPref sharedPref, NetworkErrorHandler networkErrorHandler) {
        this.mainRepo = mainRepo;
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
    }


    public void getQueueAvailability(int serviceId) {
        mainRepo.getQueueAvailability(serviceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<GetQueueAvailability>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrGetQueueAvailability.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(GetQueueAvailability getQueueAvailability) {
                        obrGetQueueAvailability.setValue(Resource.success(getQueueAvailability, "Success"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (networkErrorHandler.getStatusCode(e) == HttpURLConnection.HTTP_NOT_FOUND) {
                            obrGetQueueAvailability.setValue(Resource.warn(null, networkErrorHandler.resolveError(e)));
                        } else {
                            obrGetQueueAvailability.setValue(Resource.error(null, networkErrorHandler.resolveError(e)));
                        }

                    }
                });
    }

    public void getAvailableSlot(int serviceId, String date/*Map<String, String> data*/) {
        mainRepo.getAvailableSlotsCopy(serviceId, date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<AvailableSlotsBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrAvailableSlot.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(List<AvailableSlotsBean> bean) {
                        if (bean.size() > 0)
                            obrAvailableSlot.setValue(Resource.success(bean, "Success"));
                        else
                            obrAvailableSlot.setValue(Resource.warn(bean, "No Data Available"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrAvailableSlot.setValue(Resource.error(null, networkErrorHandler.resolveError(e)));
                    }
                });

    }


    void loadReservations(int serviceId) {
        mainRepo.getReservationList(serviceId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<GetReservationBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        reservationEvent.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull List<GetReservationBean> simpleApiResponse) {
                        if (simpleApiResponse.size() > 0)
                            reservationEvent.setValue(Resource.success(simpleApiResponse, "Data"));
                        else reservationEvent.setValue(Resource.warn(simpleApiResponse, "No Data"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        reservationEvent.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });

    }


    public void book(String body) {
        mainRepo.book(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                        compositeDisposable.add(d);
                        obrBook.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        JsonObject jsonObject = simpleApiResponse.getAsJsonObject();
                        obrBook.setValue(Resource.success(null, "Booked"));
                    }

                    @Override
                    public void onError(@NotNull Throwable throwable) {

                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrBook.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_NO_CONTENT:
                                    obrBook.setValue(Resource.success(null, "Logout successful"));
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                case 422:
                                    obrBook.setValue(Resource.warn(null, networkErrorHandler.getErrorMessage(throwable)));
                                    break;
                            }
                        } else if (throwable instanceof IOException) {
                            obrBook.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                        } else {
                            if (throwable.getMessage() != null)
                                obrBook.setValue(Resource.error(null, throwable.getMessage()));
                        }


                    }
                });

    }
}
