package com.techwin.counter.ui.login;


import androidx.databinding.ObservableField;

import com.techwin.counter.data.beans.UserBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.dump.InputUtils;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginActivityVM extends BaseViewModel {

    final SingleRequestEvent<UserBean> obrLogin = new SingleRequestEvent<>();

    private final SharedPref sharedPref;
    private final MainRepo mainRepo;
    private final NetworkErrorHandler networkErrorHandler;
    public final ObservableField<String> fieldusername = new ObservableField<>();
    public final ObservableField<String> field_Password = new ObservableField<>();
    public final ObservableField<String> field_branch = new ObservableField<>();

    @Inject
    public LoginActivityVM(SharedPref sharedPref, MainRepo mainRepo, NetworkErrorHandler networkErrorHandler) {

        this.sharedPref = sharedPref;
        this.mainRepo = mainRepo;
        this.networkErrorHandler = networkErrorHandler;
    }

    void login(boolean remindme) {
        if (InputUtils.isEmpty(fieldusername.get())) {
            warn.setValue("Username is empty");
            return;
        }
        if (InputUtils.isEmpty(field_Password.get())) {
            warn.setValue("Password empty");
            return;
        }
    /*    if (InputUtils.isEmpty(field_branch.get())) {
            warn.setValue("Branch ID empty");
            return;
        }*/
        JSONObject map = new JSONObject();
        try {
            map.put("username", InputUtils.getTrimString(fieldusername.get()));
            map.put("password", InputUtils.getTrimString(field_Password.get()));
           // map.put("branchId", InputUtils.getTrimString(field_branch.get()));
            map.put("rememberMe", remindme);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
        mainRepo.login(map.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<UserBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrLogin.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull UserBean simpleApiResponse) {
                      //  sharedPref.put("branch", InputUtils.getTrimString(field_branch.get()));
                        obrLogin.setValue(Resource.success(simpleApiResponse, "logged in"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrLogin.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e, false)));

                    }
                });

    }

}
