package com.techwin.counter.ui.admin.history.detail;

import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.ResvationsBean;
import com.techwin.counter.databinding.FragmentHistoryDetailBinding;
import com.techwin.counter.databinding.HolderCountBinding;
import com.techwin.counter.databinding.HolderHistoryDetailBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppFragment;
import com.techwin.counter.ui.admin.today.TodayFragment;
import com.techwin.counter.util.decoration.ListVerticalItemDecoration;
import com.techwin.counter.util.event.SingleRequestEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class HistoryDetailFragment extends AppFragment<FragmentHistoryDetailBinding, HistoryDetailFragmentVM> {
    public static final String TAG = "HistoryDetailFragment";
    private Calendar date = Calendar.getInstance();
    private ResvationsBean data;

    @Override
    protected BindingFragment<HistoryDetailFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_history_detail, HistoryDetailFragmentVM.class);
    }


    @Override
    protected void subscribeToEvents(final HistoryDetailFragmentVM vm) {
        if (getArguments() != null) {
            date.setTimeInMillis(getArguments().getLong("date"));
            binding.tvLine2.setText(new SimpleDateFormat("MMMM dd,yyyy", Locale.getDefault()).format(date.getTime()));
        }
        setTopAdater();
        setBottomeAdater();
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<ResvationsBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    break;
                case SUCCESS:
                    data=resource.data;
                    setData();
                    break;
                case ERROR:
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.header) {
                    Navigation.findNavController(view).navigateUp();
                }
            }
        });
        setData();
    }

    @Override
    public void onStart() {
        super.onStart();
        viewModel.getData(new SimpleDateFormat("MM/dd/yyyy", Locale.US).format(date.getTime()));
    }

    private void setData() {
        if (data == null)
            data = new ResvationsBean();
        List<TodayFragment.TopData> topData = new ArrayList<>();
        topData.add(new TodayFragment.TopData("Total Customers", getCount(data.allCustomers), "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.total_color)), true));
        topData.add(new TodayFragment.TopData("Walk in Customers", getCount(data.walkIns), "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.walkin_color))));
        topData.add(new TodayFragment.TopData("Total Reservations", getCount(data.reservations), "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.reservation_color))));
        topData.add(new TodayFragment.TopData("Not show Customers", getCount(data.noShowReservations), "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.nodata_color))));
        adpterTop.setList(topData);
        selectBottomData(data.allCustomers);
    }

    private void selectBottomData(List<ResvationsBean.DataBean> beans) {
        adapterBottom.clearList();
        if (beans != null) {
            for (int i = 0; i < beans.size(); i++) {
                TodayFragment.TopData top = new TodayFragment.TopData("", "", "#ffffff");
                top.dataBean = beans.get(i);
                adapterBottom.addData(top);
            }
        }
    }

    private String getCount(List<ResvationsBean.DataBean> t1) {
        if (t1 != null)
            return String.valueOf(t1.size());
        return "0";
    }

    private SimpleRecyclerViewAdapter<TodayFragment.TopData, HolderHistoryDetailBinding> adapterBottom;

    private void setBottomeAdater() {
        adapterBottom = new SimpleRecyclerViewAdapter<>(R.layout.holder_history_detail, BR.bean, (SimpleRecyclerViewAdapter.SimpleCallback<TodayFragment.TopData>) (v, topData) -> {

        });
        binding.rvTwo.addItemDecoration(new ListVerticalItemDecoration(baseContext, R.dimen.dp2));
        binding.rvTwo.setLayoutManager(new LinearLayoutManager(baseContext));
        binding.rvTwo.setAdapter(adapterBottom);
    }

    private SimpleRecyclerViewAdapter<TodayFragment.TopData, HolderCountBinding> adpterTop;

    private void setTopAdater() {
        adpterTop = new SimpleRecyclerViewAdapter<>(R.layout.holder_count, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<TodayFragment.TopData>() {
            @Override
            public void onItemClick(View v, TodayFragment.TopData topData) {

            }

            @Override
            public void onPositionClick(View v, int pos) {
                for (int i = 0; i < adpterTop.getItemCount(); i++) {
                    adpterTop.getList().get(i).checked = i == pos;
                }
                adpterTop.notifyDataSetChanged();
                if (data != null) {
                    if (pos == 0) {
                        selectBottomData(data.allCustomers);
                    } else if (pos == 1) {
                        selectBottomData(data.walkIns);
                    } else if (pos == 2) {
                        selectBottomData(data.reservations);
                    } else if (pos == 3) {
                        selectBottomData(data.noShowReservations);
                    }
                }
            }
        });
        binding.rvTop.setLayoutManager(new GridLayoutManager(baseContext, 4));
        binding.rvTop.setAdapter(adpterTop);

    }


}
