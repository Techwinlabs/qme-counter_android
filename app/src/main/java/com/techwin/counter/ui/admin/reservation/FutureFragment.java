package com.techwin.counter.ui.admin.reservation;

import android.view.View;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.DaysBean;
import com.techwin.counter.data.beans.ReservationListBean;
import com.techwin.counter.databinding.FragmentFutureBinding;
import com.techwin.counter.databinding.HolderDateBinding;
import com.techwin.counter.databinding.HolderFutureBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppFragment;
import com.techwin.counter.util.decoration.ListVerticalItemDecoration;
import com.techwin.counter.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.List;

public class FutureFragment extends AppFragment<FragmentFutureBinding, FutureFragmentVM> {
    public static final String TAG = "FutureFragment";

    private final List<DaysBean> daysBeans = new ArrayList<>();
    private SimpleRecyclerViewAdapter<DaysBean, HolderDateBinding> adapterTop;
    private SimpleRecyclerViewAdapter<DaysBean.ReservationsBean, HolderFutureBinding> adapterBottom;

    @Override
    protected BindingFragment<FutureFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_future, FutureFragmentVM.class);
    }


    @Override
    protected void subscribeToEvents(final FutureFragmentVM vm) {
        binding.header.ivSearch.setVisibility(View.GONE);
        initTopAdapter();
        initBottomAdapter();
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<ReservationListBean>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    binding.srOne.setRefreshing(false);
                    daysBeans.clear();
                    if (resource.data != null && resource.data.days != null) {
                        daysBeans.addAll(resource.data.days);
                    } else {
                        vm.info.setValue("No data");
                    }
                    updateTopAdater();
                    break;
                case ERROR:
                    binding.srOne.setRefreshing(false);
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {

            }
        });
        binding.srOne.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
        loadData();
    }


    private void updateTopAdater() {
        if (daysBeans.size() > 0) {
            daysBeans.get(0).checked = true;
            adapterBottom.setList(daysBeans.get(0).reservations);
            binding.setBean(daysBeans.get(0));
        }
        adapterTop.setList(daysBeans);

    }

    private void loadData() {
        viewModel.get7DayData("7");
    }

    private void initTopAdapter() {
        adapterTop = new SimpleRecyclerViewAdapter<>(R.layout.holder_date, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<DaysBean>() {
            @Override
            public void onItemClick(View v, DaysBean topData) {
                for (int i = 0; i < adapterTop.getItemCount(); i++) {
                    adapterTop.getList().get(i).checked = adapterTop.getList().get(i).date.equals(topData.date);
                }
                adapterTop.notifyItemRangeChanged(0, adapterTop.getItemCount());
                binding.setBean(topData);
                updateBottomAdapter(topData.reservations);
            }
        });
        binding.rvTop.setLayoutManager(new LinearLayoutManager(baseContext, RecyclerView.HORIZONTAL, false));
        binding.rvTop.setAdapter(adapterTop);
        new PagerSnapHelper().attachToRecyclerView(binding.rvTop);
    }

    private void updateBottomAdapter(List<DaysBean.ReservationsBean> reservations) {
        adapterBottom.setList(reservations);
    }

    private void initBottomAdapter() {
        adapterBottom = new SimpleRecyclerViewAdapter<>(R.layout.holder_future, BR.bean, (SimpleRecyclerViewAdapter.SimpleCallback<DaysBean.ReservationsBean>) (v, topData) -> {

        });
        binding.rvTwo.addItemDecoration(new ListVerticalItemDecoration(baseContext, R.dimen.dp2));
        binding.rvTwo.setLayoutManager(new LinearLayoutManager(baseContext));
        binding.rvTwo.setAdapter(adapterBottom);
    }
}
