package com.techwin.counter.ui.reservation.detail;


import com.google.gson.JsonElement;
import com.techwin.counter.MyApplication;
import com.techwin.counter.data.beans.GetReservationByDateBean;
import com.techwin.counter.data.beans.base.AvailableSlotsBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class ReservationDetailActivityVM extends BaseViewModel {
    final SingleRequestEvent<List<GetReservationByDateBean>> reservationByDateEvent = new SingleRequestEvent<>();
    private final MainRepo mainRepo;
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    public final SingleRequestEvent<Void> obrBook = new SingleRequestEvent<>();
    public final SingleRequestEvent<List<AvailableSlotsBean>> obrAvailableSlot = new SingleRequestEvent<>();
    public final SingleRequestEvent<Void> obrCancelBooking = new SingleRequestEvent<>();

    @Inject
    public ReservationDetailActivityVM(MainRepo mainRepo, SharedPref sharedPref, NetworkErrorHandler networkErrorHandler) {
        this.mainRepo = mainRepo;
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
    }


    public void cancelBooking(int serviceId, int reservationId) {
        JSONObject data = new JSONObject();
        try {
            data.put("serviceId", serviceId);
            data.put("reservationId", reservationId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mainRepo.cancelBooking(data.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrCancelBooking.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(JsonElement jsonElement) {
                        obrCancelBooking.setValue(Resource.success(null, "Success"));
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrCancelBooking.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_NO_CONTENT:
                                    obrCancelBooking.setValue(Resource.success(null, "Success"));
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                case 422:
                                    obrCancelBooking.setValue(Resource.warn(null, networkErrorHandler.resolveError(throwable)));
                                    break;
                                case 404:
                                    obrCancelBooking.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                                    break;
                                default:
                                    obrCancelBooking.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                                    break;

                            }
                        } else if (throwable instanceof IOException) {
                            obrCancelBooking.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                        } else {
                            if (throwable.getMessage() != null)
                                obrCancelBooking.setValue(Resource.error(null, throwable.getMessage()));
                        }
                    }
                });
    }

    public void getAvailableSlot(Map<String, String> data) {
        mainRepo.getAvailableSlots(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<AvailableSlotsBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrAvailableSlot.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(List<AvailableSlotsBean> bean) {
                        if (bean.size() > 0)
                            obrAvailableSlot.setValue(Resource.success(bean, "Success"));
                        else
                            obrAvailableSlot.setValue(Resource.warn(bean, "No Data Available"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrAvailableSlot.setValue(Resource.error(null, networkErrorHandler.resolveError(e)));
                    }
                });

    }

    public void book(String body) {
        mainRepo.book(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                        compositeDisposable.add(d);
                        obrBook.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        obrBook.setValue(Resource.success(null, "Success"));
                    }

                    @Override
                    public void onError(@NotNull Throwable throwable) {
                        if (throwable instanceof HttpException) {
                            HttpException exception = (HttpException) throwable;
                            switch (exception.code()) {
                                case HttpURLConnection.HTTP_UNAUTHORIZED:
                                    obrBook.setValue(Resource.error(null, throwable.getMessage()));
                                    MyApplication.getInstance().restartApp();
                                    break;
                                case HttpURLConnection.HTTP_NO_CONTENT:
                                    obrBook.setValue(Resource.success(null, "Booking successful"));
                                    break;
                                case HttpURLConnection.HTTP_BAD_REQUEST:
                                case 422:
                                    obrBook.setValue(Resource.warn(null, networkErrorHandler.resolveError(throwable)));
                                    break;
                            }
                        } else if (throwable instanceof IOException) {
                            obrBook.setValue(Resource.error(null, networkErrorHandler.resolveError(throwable)));
                        } else {
                            if (throwable.getMessage() != null)
                                obrBook.setValue(Resource.error(null, throwable.getMessage()));
                        }


                    }
                });

    }


    public void getReservationById(int serviceId, String date) {
        mainRepo.getReservationByDate(serviceId, date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<GetReservationByDateBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        reservationByDateEvent.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull List<GetReservationByDateBean> simpleApiResponse) {
                        if (simpleApiResponse.size() > 0)
                            reservationByDateEvent.setValue(Resource.success(simpleApiResponse, "Data"));
                        else
                            reservationByDateEvent.setValue(Resource.warn(simpleApiResponse, "No Data"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        reservationByDateEvent.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }

}
