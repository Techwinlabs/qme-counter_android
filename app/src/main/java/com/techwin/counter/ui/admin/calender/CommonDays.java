package com.techwin.counter.ui.admin.calender;

/**
 * Created by androiddev on 8/9/2018.
 */

public class CommonDays {

    int dayOfWeek;
    String requestDate;
    boolean isAvailaible;
    boolean isFullDay;
    boolean isFirstHalf;
    boolean isSecondHalf;
    boolean isPartialDay;
    int clinicPricingId;
    int priceFullDay;
    int pricePartialDay;


    public int getPriceFullDay() {
        return priceFullDay;
    }

    public int getPricePartialDay() {
        return pricePartialDay;
    }


    public void setPriceFullDay(int priceFullDay) {
        this.priceFullDay = priceFullDay;
    }

    public void setPricePartialDay(int pricePartialDay) {
        this.pricePartialDay = pricePartialDay;
    }




    public void setFullDay(boolean fullDay) {
        isFullDay = fullDay;
    }

    public void setFirstHalf(boolean firstHalf) {
        isFirstHalf = firstHalf;
    }

    public void setSecondHalf(boolean secondHalf) {
        isSecondHalf = secondHalf;
    }

    public void setClinicPricingId(int clinicPricingId) {
        this.clinicPricingId = clinicPricingId;
    }


    public boolean isFullDay() {
        return isFullDay;
    }

    public boolean isFirstHalf() {
        return isFirstHalf;
    }

    public boolean isSecondHalf() {
        return isSecondHalf;
    }

    public int getClinicPricingId() {
        return clinicPricingId;
    }



    //setters
    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public void setAvailaible(boolean availaible) {
        isAvailaible = availaible;
    }

    //getters
    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public boolean isAvailaible() {
        return isAvailaible;
    }

    public boolean isPartialDay() {
        return isPartialDay;
    }

    public void setPartialDay(boolean partialDay) {
        isPartialDay = partialDay;
    }

}
