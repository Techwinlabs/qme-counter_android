package com.techwin.counter.ui.admin.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import com.techwin.counter.R;
import com.techwin.counter.databinding.ActivityAdminMainBinding;
import com.techwin.counter.databinding.SheetMoreLinksBinding;
import com.techwin.counter.di.base.view.AppActivity;
import com.techwin.counter.di.base.view.BaseSheet;

public class AdminMainActivity extends AppActivity<ActivityAdminMainBinding, AdminMainActivityVM> {
    private NavController navController;
    private final NavController.OnDestinationChangedListener listioner = new NavController.OnDestinationChangedListener() {
        @Override
        public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
            changeTabBars(destination);

        }
    };

    private void changeTabBars(NavDestination item) {
        try {
            if (item.getId() == R.id.frg_today)
                binding.tab.getTabAt(0).select();
            else if (item.getId() == R.id.frg_future)
                binding.tab.getTabAt(1).select();
            if (item.getId() == R.id.frg_history)
                binding.tab.getTabAt(2).select();
            else if (item.getId() == R.id.frg_manage)
                binding.tab.getTabAt(3).select();
            else if (item.getId() == R.id.frg_more)
                binding.tab.getTabAt(4).select();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected BindingActivity<AdminMainActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_admin_main, AdminMainActivityVM.class);
    }

    public static Intent newIntent(Activity activity) {
        Intent intent = new Intent(activity, AdminMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void subscribeToEvents(final AdminMainActivityVM vm) {
        initBottombar();
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.my_nav_host);
        if (navHostFragment != null) {
            navController = navHostFragment.getNavController();
            NavigationUI.setupWithNavController(binding.bottomBar, navController);
            navController.addOnDestinationChangedListener(listioner);
        }
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {

            }
        });


    /*    vm.obrCallTicket.observe(this, (SingleRequestEvent.RequestObserver<String>) resource -> {
            switch (resource.status) {
                case LOADING:
                    if (dialogAuth != null)
                        dialogAuth.dismiss();
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    if (dialogAuth != null)
                        dialogAuth.show();
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    showCounterDialog(resource.data);
                    viewModel.loadCounter(serviceId);
                    break;
            }
        });*/

    }

    private void initBottombar() {
        binding.tab.addTab(binding.tab.newTab());
        binding.tab.addTab(binding.tab.newTab());
        binding.tab.addTab(binding.tab.newTab());
        binding.tab.addTab(binding.tab.newTab());
        binding.tab.addTab(binding.tab.newTab());
    }

    @Override
    protected void onDestroy() {
        navController.removeOnDestinationChangedListener(listioner);
        super.onDestroy();
    }



}

