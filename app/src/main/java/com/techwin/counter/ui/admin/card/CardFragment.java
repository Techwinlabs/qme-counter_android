package com.techwin.counter.ui.admin.card;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.braintreepayments.cardform.utils.CardType;
import com.braintreepayments.cardform.view.CardForm;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.CardBean;
import com.techwin.counter.data.beans.InvoiceBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.databinding.FragmentCardBinding;
import com.techwin.counter.databinding.HolderCardsBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppFragment;
import com.techwin.counter.ui.admin.card.otp.OtpActivity;
import com.techwin.counter.util.decoration.ListVerticalItemDecoration;
import com.techwin.counter.util.event.SingleRequestEvent;
import com.techwin.counter.util.misc.DefaultListChangeCallback;

import org.json.JSONObject;

import java.util.Locale;

import javax.inject.Inject;

public class CardFragment extends AppFragment<FragmentCardBinding, CardFragmentVM> {
    public static final String TAG = "CardFragment";
    @Nullable
    private InvoiceBean invoice;
    @Nullable
    private CardBean cardBean;
    @Inject
    public SharedPref sharedPref;
    private SimpleRecyclerViewAdapter<CardBean, HolderCardsBinding> adapterCard;
    @Nullable
    private JSONObject pay;

    @Override
    protected BindingFragment<CardFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_card, CardFragmentVM.class);
    }


    @Override
    protected void subscribeToEvents(final CardFragmentVM vm) {
        initViews();
        setCardAdapter();
        vm.obrPay.observe(this, (SingleRequestEvent.RequestObserver<JsonElement>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (resource.data != null) {
                        JsonObject object = resource.data.getAsJsonObject();
                        if (object.has("otpPage")) {
                            String s = object.get("otpPage").getAsString();
                            Intent intent = OtpActivity.newIntent(baseContext, s);
                            startNewActivity(intent);
                        }
                    }
                    break;
                case ERROR:
                    pay=null;
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });

        vm.obrOtp.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    Navigation.findNavController(binding.rvCard).navigateUp();
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.header) {
                    Navigation.findNavController(view).navigateUp();
                } else if (view.getId() == R.id.btn_pay) {
                    try {
                        apiPay();
                        saveCard();
                    } catch (Exception e) {
                        viewModel.error.setValue(e.getMessage());
                    }
                } else if (view.getId() == R.id.btn_add) {
                    try {
                        if (!binding.cardform.isValid()) {
                            binding.cardform.validate();
                            return;
                        }
                        addCard();
                    } catch (Exception e) {
                        viewModel.error.setValue(e.getMessage());
                    }
                }
            }
        });

    }

    private void saveCard() {
        if (binding.switchOne.isChecked()) {
            if (cardBean == null) {
                cardBean = new CardBean();
            }
            cardBean.account = binding.cardform.getCardNumber();
            cardBean.month = binding.cardform.getExpirationMonth();
            cardBean.year = binding.cardform.getExpirationYear();
            cardBean.cvv = binding.cardform.getCvv();
            CardType cardType = binding.cardform.getCardEditText().getCardType();
            if (cardType != null && cardType != CardType.UNKNOWN && cardType != CardType.EMPTY)
                cardBean.icon = cardType.getFrontResource();
            sharedPref.addCard(cardBean);
        } else {
            if (cardBean != null) {
                sharedPref.removeCard(cardBean);
            }
        }
    }

    private void addCard() {
        CardBean cardBean = new CardBean();
        cardBean.account = binding.cardform.getCardNumber();
        cardBean.month = binding.cardform.getExpirationMonth();
        cardBean.year = binding.cardform.getExpirationYear();
        cardBean.cvv = binding.cardform.getCvv();
        CardType cardType = binding.cardform.getCardEditText().getCardType();
        if (cardType != null && cardType != CardType.UNKNOWN && cardType != CardType.EMPTY)
            cardBean.icon = cardType.getFrontResource();
        String message = sharedPref.addCard(cardBean);
        viewModel.info.setValue(message);
        adapterCard.setList(sharedPref.getCards());
    }

    private void initViews() {
        if (getArguments() != null) {
            invoice = (InvoiceBean) getArguments().getSerializable("data");
            binding.setAddcard(getArguments().getBoolean("addcard", false));
        }
        binding.cardform.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .cardholderName(CardForm.FIELD_DISABLED)
                .postalCodeRequired(false)
                .mobileNumberRequired(false)
                .setup(getActivity());
        if (invoice != null) {
            binding.tvSubtotal.setText(String.format(Locale.US, "EGP %d", invoice.totalAmount));
            binding.tvTotal.setText(String.format(Locale.US, "EGP %d", invoice.totalAmount));
        }

    }

    private void setCardAdapter() {
        adapterCard = new SimpleRecyclerViewAdapter<>(R.layout.holder_cards, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<CardBean>() {
            @Override
            public void onItemClick(View v, CardBean c) {
                if (binding.switchOne.getVisibility() == View.VISIBLE) {
                    if (v.getId() == R.id.tv_name) {
                        binding.switchOne.setChecked(true);
                        cardBean = c;
                        binding.cardform.getCardEditText().setText(cardBean.account);
                        binding.cardform.getExpirationDateEditText().setText(new StringBuilder().append(cardBean.month).append("/").append(cardBean.year).toString());
                        binding.cardform.getCvvEditText().setText(cardBean.cvv);
                    }
                }
            }

            @Override
            public void onPositionClick(View v, int pos) {
                CardBean cardBean = adapterCard.getList().get(pos);
                sharedPref.removeCard(cardBean);
                adapterCard.removeItem(pos);
            }
        });
        binding.rvCard.setLayoutManager(new LinearLayoutManager(baseContext));
        binding.rvCard.addItemDecoration(new ListVerticalItemDecoration(baseContext, R.dimen.dp1));
        binding.rvCard.setAdapter(adapterCard);
        adapterCard.getDataList().addOnListChangedCallback(new DefaultListChangeCallback<CardBean>() {

            @Override
            public void onSizeUpdated(int size) {
                if (size > 0) {
                    binding.rvCard.setVisibility(View.VISIBLE);
                    binding.tvCardsave.setVisibility(View.VISIBLE);
                } else {
                    binding.rvCard.setVisibility(View.GONE);
                    binding.tvCardsave.setVisibility(View.GONE);
                }
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        adapterCard.setList(sharedPref.getCards());
        if (pay != null) {
            viewModel.confirmOtp(pay.toString());
        }
    }

    private void apiPay() throws Exception {
        /*{
  "invoiceId": 1,
  "paymentInfo": {
    "card": "235435345",
    "expiry": {
      "month": "10",
      "year": "22"
    },
    "cvv": "123",
    "ipAddress": "321432324"
  }
}*/
        if (!binding.cardform.isValid()) {
            binding.cardform.validate();
            return;
        }
        if (invoice != null) {

            JSONObject object = new JSONObject();
            object.put("invoiceId", invoice.id);

            JSONObject paymentInfo = new JSONObject();
            paymentInfo.put("cvv", binding.cardform.getCvv());
            paymentInfo.put("card", binding.cardform.getCardNumber());
            paymentInfo.put("ipAddress", getWifiIPAddress());

            JSONObject expiry = new JSONObject();
            expiry.put("month", binding.cardform.getExpirationMonth());
            expiry.put("year", binding.cardform.getExpirationYear());
            paymentInfo.put("expiry", expiry);

            object.put("paymentInfo", paymentInfo);
            //   Toast.makeText(baseContext, "done", Toast.LENGTH_SHORT).show();
            this.pay = object;
            viewModel.pay(object.toString());

        } else {
            throw new Exception("Invoice not found");
        }

    }

    public String getWifiIPAddress() {
        String ip = "0.0.0.0";
        try {
            Context context = requireContext().getApplicationContext();
            WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ip;
    }
}
