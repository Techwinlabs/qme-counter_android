package com.techwin.counter.ui.admin.manage;


import android.util.Log;

import com.google.gson.JsonElement;
import com.techwin.counter.data.beans.WorkingDaysBean;
import com.techwin.counter.data.beans.GetDayBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;

import java.net.HttpURLConnection;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class ManageFragmentVM extends BaseViewModel {

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    public final SingleRequestEvent<JsonElement> obrUpdate = new SingleRequestEvent<>();
    public final SingleRequestEvent<GetDayBean> obrData = new SingleRequestEvent<>();
    public final SingleRequestEvent<WorkingDaysBean> obrSummery = new SingleRequestEvent<>();
    private final MainRepo mainRepo;

    @Inject
    public ManageFragmentVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, MainRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.mainRepo = baseRepo;
    }

    void updateTime(String map) {
        mainRepo.UpdateWorkingDay(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrUpdate.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        obrUpdate.setValue(Resource.success(simpleApiResponse, ""));
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (networkErrorHandler.getStatusCode(e) == HttpURLConnection.HTTP_NO_CONTENT) {
                            obrUpdate.setValue(Resource.success(null, "Time slot Updated"));
                        } else
                            obrUpdate.setValue(Resource.error(
                                    null, networkErrorHandler.resolveError(e)));

                    }
                });

    }

    public void getData(String date) {
        Log.e(TAG,date);
        mainRepo.GetDay(date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<GetDayBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrData.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull GetDayBean simpleApiResponse) {
                        obrData.setValue(Resource.success(simpleApiResponse, "Record Found"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        if(networkErrorHandler.getStatusCode(e)==HttpURLConnection.HTTP_NOT_FOUND){
                            obrData.setValue(Resource.warn(
                                    null, networkErrorHandler.resolveError(e)));
                        }else
                        obrData.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }

    public void getWorkingDays() {
        mainRepo.GetWorkingDays()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<WorkingDaysBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrSummery.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull WorkingDaysBean simpleApiResponse) {
                        obrSummery.setValue(Resource.success(simpleApiResponse, "Data"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        obrSummery.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }
}
