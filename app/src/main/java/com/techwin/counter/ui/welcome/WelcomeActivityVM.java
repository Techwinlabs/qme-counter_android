package com.techwin.counter.ui.welcome;


import com.google.gson.JsonElement;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleActionEvent;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WelcomeActivityVM extends BaseViewModel {

    final SingleActionEvent<Integer> clk_options = new SingleActionEvent<>();

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    private final MainRepo mainRepo;
    @Inject
    public WelcomeActivityVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, MainRepo mainRepo) {

        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.mainRepo = mainRepo;
    }


}
