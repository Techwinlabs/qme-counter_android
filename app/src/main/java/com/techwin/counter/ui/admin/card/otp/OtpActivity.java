package com.techwin.counter.ui.admin.card.otp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;

import com.techwin.counter.R;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.databinding.ActivityOtpBinding;
import com.techwin.counter.di.base.view.AppActivity;

import javax.inject.Inject;

public class OtpActivity extends AppActivity<ActivityOtpBinding, OtpActivityVM> {

    @Inject
    SharedPref sharedPref;

    @Override
    protected BindingActivity<OtpActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_otp, OtpActivityVM.class);
    }

    public static Intent newIntent(Context activity, String data) {
        Intent intent = new Intent(activity, OtpActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("data", data);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void subscribeToEvents(final OtpActivityVM vm) {
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.iv_back) {
                   finish();
                }
            }
        });
        String data = getIntent().getStringExtra("data");
        binding.webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                binding.progressBar.setVisibility(View.VISIBLE);
                binding.message.setText(progress + " %");//Make the bar disappear after URL is loaded
                // Return the app name after finish loading
                if (progress == 100) {
                    binding.progressBar.setVisibility(View.GONE);
                }
            }
        });
        binding.webview.setWebViewClient(new HelloWebViewClient());
        binding.webview.getSettings().setJavaScriptEnabled(true);
        if (data != null)
            binding.webview.loadDataWithBaseURL(null, data, "text/html", "utf-8", null);
    }

    private static class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }


}

