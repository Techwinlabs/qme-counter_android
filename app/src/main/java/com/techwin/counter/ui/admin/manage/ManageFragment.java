package com.techwin.counter.ui.admin.manage;

import android.app.TimePickerDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.google.gson.JsonElement;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.GetDayBean;
import com.techwin.counter.databinding.DialogWarningBinding;
import com.techwin.counter.databinding.FragmentManageBinding;
import com.techwin.counter.di.base.view.AppFragment;
import com.techwin.counter.di.base.view.BaseCustomDialog;
import com.techwin.counter.util.event.SingleRequestEvent;
import com.techwin.counter.util.misc.DatePickerUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class ManageFragment extends AppFragment<FragmentManageBinding, ManageFragmentVM> {
    public static final String TAG = "ManageFragment";
    private TimePickerDialog dialog;
    @Nullable
    private Calendar startDate;
    @Nullable
    private Calendar oldstartDate;
    @Nullable
    private Calendar endDate;
    @Nullable
    private GetDayBean data;
    private Calendar selectedDate;

    @Override
    protected BindingFragment<ManageFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_manage, ManageFragmentVM.class);
    }


    @Override
    protected void subscribeToEvents(final ManageFragmentVM vm) {
        binding.header.ivSearch.setVisibility(View.GONE);
      /*  vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<List<TransBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    // showLoading();
                    break;
                case SUCCESS:
                    hideLoading();
                    if (resource.data != null && resource.data.size() > 0) {
                        binding.setBean(resource.data.get(0));
                        setDataDonationFragment(resource.data.get(0));
                    }
                    break;
                case WARN:
                    hideLoading();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    hideLoading();
                    vm.error.setValue(resource.message);
                    break;
            }
        });*/
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.tv_line4) {
                    showWarnDialog();
                } else if (view.getId() == R.id.tv_starttime) {
                    if (binding.getEditmode())
                        showStartTimeDialog();
                } else if (view.getId() == R.id.tv_endtime) {
                   /* if (binding.getEditmode())
                        showEndTimeDialog();*/
                }
            }
        });
        vm.obrUpdate.observe(this, (SingleRequestEvent.RequestObserver<JsonElement>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;

                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    binding.setEditmode(false);
                    apiGetData(selectedDate);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<GetDayBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    data = null;
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                case SUCCESS:
                    dismissProgressDialog();
                    data = resource.data;
                    setEditMode(false);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    setEditMode(false);
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        setCalenderView();
        setEditMode(false);
        selectedDate = (Calendar) binding.calenderView.getFirstSelectedDate().clone();
        /*Calendar today = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        selectedDate.set(Calendar.HOUR, today.get(Calendar.HOUR_OF_DAY));
        selectedDate.set(Calendar.MINUTE, today.get(Calendar.MINUTE));
        try {
            binding.calenderView.setDate(selectedDate);
        } catch (OutOfDateRangeException e) {
            e.printStackTrace();
        }*/
        apiGetData(selectedDate);
    }

    private void apiGetData(Calendar calendar) {
        /*2021-09-25T12:16:00.798Z*/
        String time = "";
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar today = Calendar.getInstance();
        if (dateFormat.format(calendar.getTime()).equals(dateFormat.format(today.getTime()))) {
            Calendar cal = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, cal.get(Calendar.SECOND));
            calendar.add(Calendar.MINUTE, +1);
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
            s.setTimeZone(TimeZone.getTimeZone("UTC"));
            time = s.format(calendar.getTime());
        } else {
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            time = s.format(calendar.getTime());
        }
        viewModel.getData(time);
    }

    private void updateTimeApi() throws JSONException {
        JSONObject ob1 = new JSONObject();
        long mins = 0;
        if (oldstartDate != null && startDate != null) {
            long diff = startDate.getTimeInMillis() - oldstartDate.getTimeInMillis();
            mins = ((diff / 1000) / 60);
        }
        ob1.put("rangeInMinutes", mins);
        ob1.put("date", getStartDate());
        ob1.put("offDay", binding.switchOne.isChecked());
        viewModel.updateTime(ob1.toString());
        Log.e(TAG, ob1.toString());
    }

    private String getStartDate() {
        if (selectedDate != null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            return dateFormat.format(selectedDate.getTime());
        } else return "";
    }

    private void setEditMode(boolean b) {
        binding.setEditmode(b);
        refreshData();
    }

    private void refreshData() {
        startDate = null;
        oldstartDate = null;
        endDate = null;
        if (data != null) {
            binding.setEnable(data.isWorking);
            if (data.isWorking) {
                binding.switchOne.setChecked(false);
                if (data.startTime != null) {
                    startDate = (Calendar) selectedDate.clone();
                    startDate.set(Calendar.HOUR_OF_DAY, data.startTime.hours);
                    startDate.set(Calendar.MINUTE, data.startTime.minutes);
                    startDate.setTimeZone(TimeZone.getDefault());

                    oldstartDate = (Calendar) startDate.clone();
                }
                if (data.endTime != null) {
                    endDate = (Calendar) selectedDate.clone();
                    endDate.set(Calendar.HOUR_OF_DAY, data.endTime.hours);
                    endDate.set(Calendar.MINUTE, data.endTime.minutes);
                    endDate.setTimeZone(TimeZone.getDefault());
                }
            } else {

                binding.switchOne.setChecked(true);
            }
        } else {
            binding.setEnable(false);
            binding.switchOne.setChecked(true);
        }
        updateTextData();

    }

    private void updateTextData() {
        String s1 = startDate != null ? new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(startDate.getTime()) : "--:--";
        String s2 = endDate != null ? new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(endDate.getTime()) : "--:--";
        if (binding.getEditmode()) {
            binding.switchOne.setEnabled(true);
            binding.tvStarttime.setText(s1);
            binding.tvEndtime.setText(s2);
        } else {
            binding.switchOne.setEnabled(false);
            binding.tvStarttime.setText(String.format("Starting at %s", s1));
            binding.tvEndtime.setText(String.format("Ending at %s", s2));
        }
    }


    private void showStartTimeDialog() {
        if (startDate == null) {
            startDate = (Calendar) selectedDate.clone();
        }
        dialog = DatePickerUtils.getInstance(baseContext).getTimePickerDialog(startDate, new DatePickerUtils.TimeSetListener() {
            @Override
            public void onTimeSet(@NonNull TimePickerDialog timePickerDialog, @NonNull Calendar ca) {
                super.onTimeSet(timePickerDialog, ca);
                if (endDate != null) {
                    long sec = ca.getTimeInMillis() - startDate.getTimeInMillis();
                    long act = endDate.getTimeInMillis();
                    act = (act + sec);
                    endDate.setTimeInMillis(act);

                }
                startDate = ca;
                updateTextData();
            }
        }, false);
        dialog.show();
    }


    private void showEndTimeDialog() {
        if (endDate == null) {
            endDate = (Calendar) selectedDate.clone();
        }
        dialog = DatePickerUtils.getInstance(baseContext).getTimePickerDialog(endDate, new DatePickerUtils.TimeSetListener() {
            @Override
            public void onTimeSet(@NonNull TimePickerDialog timePickerDialog, @NonNull Calendar ca) {
                super.onTimeSet(timePickerDialog, ca);
                endDate = ca;
                updateTextData();
            }
        }, false);
        dialog.show();
    }

    private void setCalenderView() {
        //binding.calenderView.setMinimumDate(Calendar.getInstance());
        binding.calenderView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Calendar c = eventDay.getCalendar();
                selectedDate = (Calendar) c.clone();
                apiGetData(selectedDate);
            }
        });
        binding.switchOne.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            }
        });
    }

    private Calendar repairTodayTime(Calendar calendar) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar today = Calendar.getInstance();
        if (dateFormat.format(calendar.getTime()).equals(dateFormat.format(today.getTime()))) {
            Calendar cal = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, cal.get(Calendar.SECOND));
            calendar.add(Calendar.MINUTE, +1);
        }
        return (Calendar) calendar.clone();
    }


    private BaseCustomDialog<DialogWarningBinding> dialogWarn;

    private void showWarnDialog() {
        if (!binding.getEditmode()) {
            binding.setEditmode(true);
            refreshData();
            return;
        }
        if (binding.switchOne.isChecked()) {
            dialogWarn = new BaseCustomDialog<>(baseContext, R.layout.dialog_warning, new BaseCustomDialog.Listener() {
                @Override
                public void onViewClick(View view) {
                    if (view.getId() == R.id.tv_confirm) {
                        apiUpdate();
                    }
                    dialogWarn.dismiss();
                }
            });
            dialogWarn.show();
            Window window = dialogWarn.getWindow();
            if (window != null) {
                window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            }
        } else apiUpdate();
    }

    private void apiUpdate() {
        try {
            updateTimeApi();
        } catch (JSONException e) {
            e.printStackTrace();
            viewModel.error.setValue(e.getMessage());
        }
    }

}
