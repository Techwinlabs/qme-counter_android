package com.techwin.counter.ui.main.detail;


import com.techwin.counter.data.beans.CustomerDataBean;
import com.techwin.counter.data.beans.GetReservationBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class DetailActivityVM extends BaseViewModel {
    final SingleRequestEvent<List<CustomerDataBean>> obrVisits = new SingleRequestEvent<>();
    private final MainRepo mainRepo;
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;

    @Inject
    public DetailActivityVM(MainRepo mainRepo, SharedPref sharedPref, NetworkErrorHandler networkErrorHandler) {
        this.mainRepo = mainRepo;
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
    }


    void loadVisits(int serviceId, int branchId) {
        mainRepo.getCustomersData(serviceId, branchId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<List<CustomerDataBean>, List<CustomerDataBean>>() {
                    @Override
                    public List<CustomerDataBean> apply(@NonNull List<CustomerDataBean> customerDataBeans) throws Exception {
                        if (customerDataBeans.size() > 0)
                            customerDataBeans.get(0).show = true;
                        return customerDataBeans;
                    }
                })
                .subscribe(new SingleObserver<List<CustomerDataBean>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrVisits.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull List<CustomerDataBean> simpleApiResponse) {
                        if (simpleApiResponse.size() > 0)
                            obrVisits.setValue(Resource.success(simpleApiResponse, "Data"));
                        else obrVisits.setValue(Resource.warn(simpleApiResponse, "No Data"));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrVisits.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });

    }

}
