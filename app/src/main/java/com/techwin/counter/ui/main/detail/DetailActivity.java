package com.techwin.counter.ui.main.detail;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.CustomerDataBean;
import com.techwin.counter.data.beans.GetReservationBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.databinding.ActivityDetailBinding;
import com.techwin.counter.databinding.ActivityReservationBinding;
import com.techwin.counter.databinding.DialogAddBinding;
import com.techwin.counter.databinding.DialogAuthBinding;
import com.techwin.counter.databinding.DialogCounterBinding;
import com.techwin.counter.databinding.ViewReservationsBinding;
import com.techwin.counter.databinding.ViewTicketsBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppActivity;
import com.techwin.counter.di.base.view.BaseCustomDialog;
import com.techwin.counter.ui.reservation.ReservationActivityVM;
import com.techwin.counter.util.event.SingleRequestEvent;

import java.util.List;

import javax.inject.Inject;

public class DetailActivity extends AppActivity<ActivityDetailBinding, DetailActivityVM> {
    private int serviceId = 0;
    private String branchId = "0";
    private SimpleRecyclerViewAdapter<CustomerDataBean, ViewTicketsBinding> adapter;

    @Override
    protected BindingActivity<DetailActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_detail, DetailActivityVM.class);
    }

    public static Intent newIntent(Context activity, int serviceId) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra("serviceId", serviceId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    protected void subscribeToEvents(final DetailActivityVM vm) {
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.view_tickets, BR.bean, (v, customerDataBean) -> {

        });
        binding.rvOne.setLayoutManager(new LinearLayoutManager(this));
        binding.rvOne.setAdapter(adapter);
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {

            }
        });
        vm.obrVisits.observe(this, (SingleRequestEvent.RequestObserver<List<CustomerDataBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    adapter.setList(resource.data);
                    binding.rvOne.setVisibility(View.VISIBLE);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });

        init();

    }

    @Override
    protected void onStart() {
        super.onStart();
      getListofVisit();
    }



    private void getListofVisit() {
        viewModel.loadVisits(serviceId, Integer.parseInt(branchId));
    }


    private void init() {
        serviceId = getIntent().getIntExtra("serviceId", 0);
    }


}

