package com.techwin.counter.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;

import androidx.lifecycle.Observer;

import com.techwin.counter.R;
import com.techwin.counter.data.beans.UserBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.databinding.ActivityLoginBinding;
import com.techwin.counter.di.base.view.AppActivity;
import com.techwin.counter.ui.main.MainActivity;
import com.techwin.counter.ui.main.multi.Main2Activity;
import com.techwin.counter.ui.welcome.WelcomeActivity;
import com.techwin.counter.util.Constants;
import com.techwin.counter.util.event.SingleRequestEvent;

import java.util.Locale;

import javax.inject.Inject;

public class LoginActivity extends AppActivity<ActivityLoginBinding, LoginActivityVM> {

    @Inject
    SharedPref sharedPref;

    @Override
    protected BindingActivity<LoginActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_login, LoginActivityVM.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    protected void subscribeToEvents(final LoginActivityVM vm) {
        String locale = sharedPref.getDefaultLocale(Constants.DEFAULTLOCALE);
        if (locale.equals(Constants.DEFAULTLOCALE)) {
            binding.etPass.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    /*if (s.length() > 0) {
                        binding.etPass.setGravity(Gravity.START);
                    } else {
                        binding.etPass.setGravity(Gravity.END);
                    }*/
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.btn_login) {
                    viewModel.login(true);
                } else if (view.getId() == R.id.tv_locale) {
                    String s = sharedPref.getDefaultLocale(Constants.DEFAULTLOCALE);
                    if (s.equals(Constants.DEFAULTLOCALE)) {
                        setDefaultLocale(Constants.ENGLOCALE);
                    } else if (s.equals(Constants.ENGLOCALE)) {
                        setDefaultLocale(Constants.DEFAULTLOCALE);
                    }
                }
            }
        });
        vm.obrLogin.observe(this, (SingleRequestEvent.RequestObserver<UserBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    showMainUi(resource.data);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
    }

    private void showMainUi(UserBean userBean) {
        if (userBean.numberOfServices > 1) {
            Intent intent = Main2Activity.newIntent(LoginActivity.this);
            startNewActivity(intent, true);
        } else {
            if (userBean.services != null && userBean.services.size() > 0) {
                Intent intent = MainActivity.newIntent(LoginActivity.this, userBean.services.get(0).serviceId);
                startNewActivity(intent, true);
            } else {
                viewModel.error.setValue("No Service");
            }
        }

    }

    private void setDefaultLocale(String locale) {
        Locale myLocale = new Locale(locale);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        sharedPref.setDefaultLocale(locale);
        recreate();
    }
}

