package com.techwin.counter.ui.admin.history.detail;


import com.techwin.counter.data.beans.ResvationsBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class HistoryDetailFragmentVM extends BaseViewModel {

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    public final SingleRequestEvent<ResvationsBean> obrData = new SingleRequestEvent<>();
    private final MainRepo mainRepo;

    @Inject
    public HistoryDetailFragmentVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, MainRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.mainRepo = baseRepo;
    }

    public void getData(String date) {
        mainRepo.GetDayStatus(date)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResvationsBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrData.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull ResvationsBean simpleApiResponse) {
                        obrData.setValue(Resource.success(simpleApiResponse, "Data"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        obrData.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }
}
