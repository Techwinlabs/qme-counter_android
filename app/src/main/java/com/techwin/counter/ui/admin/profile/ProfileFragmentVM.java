package com.techwin.counter.ui.admin.profile;


import com.techwin.counter.data.beans.ProfileBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.ui.main.MainActivityVM;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class ProfileFragmentVM extends BaseViewModel {

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    public final SingleRequestEvent<ProfileBean> obrProfileData = new SingleRequestEvent<>();
    public final MainActivityVM mainActivityVM;
    private final MainRepo mainRepo;

    @Inject
    public ProfileFragmentVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, MainActivityVM mainActivityVM, MainRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.mainActivityVM = mainActivityVM;
        this.mainRepo = baseRepo;
    }

    public void getProfile() {
        mainRepo.getProfile()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ProfileBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrProfileData.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull ProfileBean simpleApiResponse) {
                        obrProfileData.setValue(Resource.success(simpleApiResponse, "Data"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        obrProfileData.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }
}
