package com.techwin.counter.ui.admin.card.otp;


import android.util.Log;

import com.google.gson.JsonElement;
import com.techwin.counter.MyApplication;
import com.techwin.counter.data.beans.CallTicketbean;
import com.techwin.counter.data.beans.CounterBean;
import com.techwin.counter.data.beans.CustomerDataBean;
import com.techwin.counter.data.beans.GetReservationBean;
import com.techwin.counter.data.beans.GetReservationByDateBean;
import com.techwin.counter.data.beans.NewTicketBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.remote.intersepter.RequestInterceptor;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.dump.InputUtils;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

public class OtpActivityVM extends BaseViewModel {

    private final MainRepo mainRepo;
    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;

    @Inject
    public OtpActivityVM(MainRepo mainRepo, SharedPref sharedPref, NetworkErrorHandler networkErrorHandler) {
        this.mainRepo = mainRepo;
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
    }

}
