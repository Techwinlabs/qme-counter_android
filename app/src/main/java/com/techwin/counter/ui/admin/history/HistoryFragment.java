package com.techwin.counter.ui.admin.history;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;

import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.MonthsDataBean;
import com.techwin.counter.databinding.FragmentHistoryBinding;
import com.techwin.counter.databinding.HolderStaticsBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppFragment;
import com.techwin.counter.ui.admin.today.TodayFragment;
import com.techwin.counter.util.AppUtils;
import com.techwin.counter.util.event.SingleRequestEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class HistoryFragment extends AppFragment<FragmentHistoryBinding, HistoryFragmentVM> {
    public static final String TAG = "HistoryFragment";
    private final OnCalendarPageChangeListener listioner = () -> updateData();


    @Override
    protected BindingFragment<HistoryFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_history, HistoryFragmentVM.class);
    }


    @Override
    protected void subscribeToEvents(final HistoryFragmentVM vm) {
        binding.header.ivSearch.setVisibility(View.GONE);
        vm.obrData.observe(this, (SingleRequestEvent.RequestObserver<MonthsDataBean>) resource -> {
            switch (resource.status) {
                case SUCCESS:
                    if (resource.data != null) {
                        List<TodayFragment.TopData> topData = new ArrayList<>();
                        topData.add(new TodayFragment.TopData("Total Customers", String.valueOf(resource.data.totalCustomers), "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.total_color))));
                        topData.add(new TodayFragment.TopData("Walk in Customers", String.valueOf(resource.data.walkInCount), "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.walkin_color))));
                        topData.add(new TodayFragment.TopData("Total Reservations", String.valueOf(resource.data.totalReservations), "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.reservation_color))));
                        topData.add(new TodayFragment.TopData("Not show Customers", String.valueOf(resource.data.noShowCount), "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.nodata_color))));
                        viewAdapter.setList(topData);
                    }
                    break;
                case ERROR:
                    viewAdapter.setList(null);
                    vm.error.setValue(resource.message);
                    List<TodayFragment.TopData> topData = new ArrayList<>();
                    topData.add(new TodayFragment.TopData("Total Customers", "0", "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.total_color))));
                    topData.add(new TodayFragment.TopData("Walk in Customers", "0", "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.walkin_color))));
                    topData.add(new TodayFragment.TopData("Total Reservations", "0", "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.reservation_color))));
                    topData.add(new TodayFragment.TopData("Not show Customers", "0", "#" + Integer.toHexString(ContextCompat.getColor(baseContext, R.color.nodata_color))));
                    viewAdapter.setList(topData);
                    break;
            }
        });
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {

            }
        });
        setCalenderView();
        setInitAdapter();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setCalenderView() {
        binding.calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Bundle bundle = new Bundle();
                bundle.putLong("date", eventDay.getCalendar().getTimeInMillis());
                Navigation.findNavController(binding.calendarView).navigate(R.id.action_frg_history_to_historyDetailFragment, bundle);
            }
        });
        binding.calendarView.setOnForwardPageChangeListener(listioner);
        binding.calendarView.setOnPreviousPageChangeListener(listioner);
        updateData();

    }

    private void updateData() {
        //  9/8/2021 12:00:00 AM +00:00
        Calendar calendar = binding.calendarView.getCurrentPageDate();
       // calendar.add(Calendar.MONTH, 1);
        if (AppUtils.compareMonthYear(calendar.getTime(),Calendar.getInstance().getTime())==0) {
            calendar.setTime(Calendar.getInstance().getTime());
            calendar.add(Calendar.DATE, -1);
        }
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Log.e(TAG, dateFormat.format(calendar.getTime()) + " 11:59:00 PM +00:00");
        viewModel.getMonthStatus(dateFormat.format(calendar.getTime()) + " 11:59:00 PM +00:00");
    }

    private SimpleRecyclerViewAdapter<TodayFragment.TopData, HolderStaticsBinding> viewAdapter;

    private void setInitAdapter() {
        viewAdapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_statics, BR.bean, (v, topData) -> {

        });
        binding.rvOne.setLayoutManager(new GridLayoutManager(baseContext, 4));
        binding.rvOne.setAdapter(viewAdapter);

    }

}
