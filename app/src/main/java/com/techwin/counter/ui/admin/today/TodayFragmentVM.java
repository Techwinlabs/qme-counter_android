package com.techwin.counter.ui.admin.today;


import com.techwin.counter.data.beans.ResvationsBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class TodayFragmentVM extends BaseViewModel {

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    public final SingleRequestEvent<ResvationsBean> obrTodayData = new SingleRequestEvent<>();
    private final MainRepo mainRepo;

    @Inject
    public TodayFragmentVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, MainRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.mainRepo = baseRepo;
    }


    public void getTodayStatus() {
        mainRepo.getTodayStatus()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResvationsBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrTodayData.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull ResvationsBean simpleApiResponse) {
                        obrTodayData.setValue(Resource.success(simpleApiResponse, "Data"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        obrTodayData.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }
}
