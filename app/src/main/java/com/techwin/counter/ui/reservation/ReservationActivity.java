package com.techwin.counter.ui.reservation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.GetQueueAvailability;
import com.techwin.counter.data.beans.GetReservationBean;
import com.techwin.counter.data.beans.base.AvailableSlotsBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.databinding.ActivityReservationBinding;
import com.techwin.counter.databinding.DialogRegisterBinding;
import com.techwin.counter.databinding.HolderAvailableSlotBinding;
import com.techwin.counter.databinding.ViewReservationsBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppActivity;
import com.techwin.counter.di.base.view.BaseCustomDialog;
import com.techwin.counter.ui.reservation.detail.ReservationDetailActivity;
import com.techwin.counter.util.dump.InputUtils;
import com.techwin.counter.util.event.SingleRequestEvent;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

public class ReservationActivity extends AppActivity<ActivityReservationBinding, ReservationActivityVM> {

    @Inject
    SharedPref sharedPref;
    private int serviceId = 0;
    private SimpleRecyclerViewAdapter<GetReservationBean, ViewReservationsBinding> adapterSlotByDate;
    private SimpleRecyclerViewAdapter<AvailableSlotsBean, HolderAvailableSlotBinding> slotAdapter;
    private DialogRegisterBinding dialogBinding;
    private AvailableSlotsBean selectedSlotBean;

    @Override
    protected BindingActivity<ReservationActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_reservation, ReservationActivityVM.class);
    }

    /*  public static Intent newIntent(Activity activity) {
          Intent intent = new Intent(activity, MainActivity.class);
          intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
          return intent;
      }
  */
    public static Intent newIntent(Context activity, int serviceId) {
        Intent intent = new Intent(activity, ReservationActivity.class);
        intent.putExtra("serviceId", serviceId);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    protected void subscribeToEvents(final ReservationActivityVM vm) {
        // init recycler view for slot
        adapterSlotByDate = new SimpleRecyclerViewAdapter<>(R.layout.view_reservations, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<GetReservationBean>() {
            @Override
            public void onItemClick(View v, GetReservationBean customerDataBean) {
                Intent intent = ReservationDetailActivity.newIntent(ReservationActivity.this, serviceId, customerDataBean.getDate());
                startNewActivity(intent);
            }
        });
        binding.rvSlotsByDate.setLayoutManager(new LinearLayoutManager(this));
        binding.rvSlotsByDate.setAdapter(adapterSlotByDate);

        vm.obrGetQueueAvailability.observe(this, new Observer<Resource<GetQueueAvailability>>() {
            @Override
            public void onChanged(Resource<GetQueueAvailability> resource) {
                switch (resource.status) {
                    case WARN:
                        dismissProgressDialog();
                        vm.warn.setValue(resource.message);
                        break;
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case SUCCESS:
                        dismissProgressDialog();
                        if (resource.data.getDays() != null) {
                            try {
                                showMaterialCalender(checkDaysAvailability(resource.data));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        //showMaterialCalender();
                        //  vm.success.setValue(resource.message);
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        vm.error.setValue(resource.message);
                        break;
                }
            }


        });
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.iv_add) {
                    viewModel.getQueueAvailability(serviceId);
                    //   showCalenderDialogOne();
                    //showCalenderDialog();
                    //showNewDialog();
                }
            }
        });

        vm.obrAvailableSlot.observe(this, new SingleRequestEvent.RequestObserver<List<AvailableSlotsBean>>() {
            @Override
            public void onRequestReceived(@NonNull Resource<List<AvailableSlotsBean>> resource) {
                switch (resource.status) {
                    case LOADING:
                        showProgressDialog(R.string.plz_wait);
                        break;
                    case SUCCESS:
                        dismissProgressDialog();
                        setAvailableSlotsRV(resource.data);
                        break;
                    case WARN:
                        dismissProgressDialog();
                        vm.warn.setValue(resource.message);
                        break;
                    case ERROR:
                        dismissProgressDialog();
                        vm.error.setValue(resource.message);
                        break;
                }
            }
        });
        vm.reservationEvent.observe(this, (SingleRequestEvent.RequestObserver<List<GetReservationBean>>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    adapterSlotByDate.setList(resource.data);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });


        vm.obrBook.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    if (dialogRegister != null)
                        dialogRegister.dismiss();
                    break;
            }
        });
        init();

    }

    @Override
    protected void onStart() {
        super.onStart();
        getListOfReservation();
    }


    private ArrayList<Integer> checkDaysAvailability(GetQueueAvailability data) {
        ArrayList<Integer> workDays = new ArrayList<>();
        for (int i = 0; i < data.getDays().size(); i++) {
            workDays.add(data.getDays().get(i).getWorkDay() + 1);
        }

        Integer[] day = {1, 2, 3, 4, 5, 6, 7};
        ArrayList<Integer> days = new ArrayList<>();
        Collections.addAll(days, day);
        ArrayList<Integer> offDay = new ArrayList<>();
        for (int i = 0; i < days.size(); i++) {
            if (!workDays.contains(days.get(i))) {
                offDay.add(days.get(i));
            }
        }
        return offDay;

    }

    private void getListOfReservation() {
        viewModel.loadReservations(serviceId/*Integer.parseInt(branchId)*/);
    }


    private void init() {
        serviceId = getIntent().getIntExtra("serviceId", 0);
    }


    //private BaseCustomDialog<DialogCalenderBinding> dialogRegister;


    private void setAvailableSlotsRV(List<AvailableSlotsBean> mBean) {
        if (dialogBinding != null) {
            slotAdapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_available_slot, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<AvailableSlotsBean>() {

                @Override
                public void onItemClick(View v, AvailableSlotsBean bean) {

                }

                @SuppressLint({"NotifyDataSetChanged"})
                @Override
                public void onClickPos(View v, AvailableSlotsBean bean, int pos) {
                    if (v.getId() == R.id.clParent) {
                        for (int i = 0; i < slotAdapter.getItemCount(); i++) {
                            Objects.requireNonNull(slotAdapter.getData(i)).setChecked(Objects.requireNonNull(slotAdapter.getData(i)).getStartTime().equals(bean.getStartTime()));

                        }
                        selectedSlotBean = bean;
                        slotAdapter.notifyItemRangeChanged(0, slotAdapter.getItemCount());
                    }
                }
            });
            dialogBinding.rvSlots.setAdapter(slotAdapter);
            slotAdapter.setList(mBean);

        }
    }


    private Calendar calendarDatePicker;
    String date;


    /*private void showCalenderDialog() {
        calendarDatePicker = Calendar.getInstance();
        DatePickerDialog datePickerDialog = DatePickerUtils.getInstance(ReservationActivity.this).getDatePickerDialog(calendarDatePicker, new DatePickerUtils.DateSetListener() {
                    @Override
                    public void onDateSet(@NonNull DatePickerDialog datePickerDialog, @NonNull Calendar ca) {
                        super.onDateSet(datePickerDialog, ca);
                        calendarDatePicker = ca;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                        date = dateFormat.format(calendarDatePicker.getTime());
                        showNewDialog();
            *//*    DatePickerUtils.getInstance(ReservationActivity.this).getTimePickerDialog(ca, new DatePickerUtils.TimeSetListener() {
                    @Override
                    public void onTimeSet(@NonNull TimePickerDialog timePickerDialog, @NonNull Calendar ca) {
                        super.onTimeSet(timePickerDialog, ca);
                        calendar = ca;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                        date = dateFormat.format(calendar.getTime());
                    }
                }, false).show();*//*
                    }
                }
        );
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }*/


    private void showMaterialCalender(ArrayList<Integer> offDays) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a", Locale.US);
                        //SimpleDateFormat dateFormat = new SimpleDateFormat("dd MM yyyy hh:mm a", Locale.US);
                        //dateFormat.format(Calendar.getInstance().set(year,monthOfYear,dayOfMonth));
                        Date date2 = null;
                        try {
                            if (monthOfYear < 9) {
                                String month = "0" + monthOfYear;
                                if (dayOfMonth < 9) {
                                    String day = "0" + dayOfMonth;
                                    date2 = new SimpleDateFormat("yyyyMMdd", Locale.US).parse("" + year + "0" + (monthOfYear + 1) + "" + day);

                                } else {
                                    date2 = new SimpleDateFormat("yyyyMMdd", Locale.US).parse("" + year + "0" + (monthOfYear + 1) + "" + dayOfMonth);
                                }

                            } else {
                                date2 = new SimpleDateFormat("yyyyMMdd", Locale.US).parse("" + year + (monthOfYear + 1) + "" + dayOfMonth);
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        date = dateFormat.format(date2);
                        //Toast.makeText(ReservationActivity.this, date, Toast.LENGTH_SHORT).show();
                        showNewDialog();
                    }
                },
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
        // restrict to weekdays only
        ArrayList<Calendar> weekdays = new ArrayList<Calendar>();
        Calendar day = Calendar.getInstance();
        /*for (int j = 0; j < offDays.size(); j++) {*/
        for (int i = 0; i < 365; i++) {
            //for (int j=)
            if (offDays != null && offDays.size() > 0) {
                switch (offDays.size()) {
                    case 1:
                        if (day.get(Calendar.DAY_OF_WEEK) != offDays.get(0)) {
                            Calendar d = (Calendar) day.clone();
                            weekdays.add(d);
                        }
                        break;
                    case 2:
                        if (day.get(Calendar.DAY_OF_WEEK) != offDays.get(0) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(1)) {
                            Calendar d = (Calendar) day.clone();
                            weekdays.add(d);
                        }
                        break;
                    case 3:
                        if (day.get(Calendar.DAY_OF_WEEK) != offDays.get(0) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(1) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(2)) {
                            Calendar d = (Calendar) day.clone();
                            weekdays.add(d);
                        }
                        break;
                    case 4:
                        if (day.get(Calendar.DAY_OF_WEEK) != offDays.get(0) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(1) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(3) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(2)) {
                            Calendar d = (Calendar) day.clone();
                            weekdays.add(d);
                        }
                        break;
                    case 5:
                        if (day.get(Calendar.DAY_OF_WEEK) != offDays.get(0) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(1) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(4) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(3) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(2)) {
                            Calendar d = (Calendar) day.clone();
                            weekdays.add(d);
                        }

                        break;
                    case 6:
                        if (day.get(Calendar.DAY_OF_WEEK) != offDays.get(0) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(1) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(5) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(4) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(3) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(2)) {
                            Calendar d = (Calendar) day.clone();
                            weekdays.add(d);
                        }
                        break;
                    case 7:
                        if (day.get(Calendar.DAY_OF_WEEK) != offDays.get(0) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(1) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(6) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(5) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(4) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(3) && day.get(Calendar.DAY_OF_WEEK) != offDays.get(2)) {
                            Calendar d = (Calendar) day.clone();
                            weekdays.add(d);
                        }
                        break;
                }

            }
            day.add(Calendar.DATE, 1);
        }
            /*if (day.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && day.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                Calendar d = (Calendar) day.clone();
                weekdays.add(d);
            }
            day.add(Calendar.DATE, 1);*/
        /*}*/
        Calendar[] weekdayDays = weekdays.toArray(new Calendar[weekdays.size()]);
        dpd.setSelectableDays(weekdayDays);
        dpd.setAccentColor(getResources().getColor(R.color.colorPrimary));
        Calendar minDate = Calendar.getInstance();
        minDate.getTimeInMillis();
        dpd.setMinDate(minDate);
        dpd.setOnCancelListener(dialog -> {
            Log.d("DatePickerDialog", "Dialog was cancelled");
            dpd.dismiss();
        });
        // If you're calling this from a support Fragment
        dpd.show(getSupportFragmentManager(), "Datepickerdialog");
    }


   /* private void showCalenderDialogOne() {
        calendarDatePicker = Calendar.getInstance();

        DatePickerDialog datePickerDialog = DatePickerUtils.getInstance(ReservationActivity.this).getDatePickerDialog(calendarDatePicker, new DatePickerUtils.DateSetListener() {
            @Override
            public void onDateSet(@NonNull DatePickerDialog datePickerDialog, @NonNull Calendar ca) {
                super.onDateSet(datePickerDialog, ca);
                calendarDatePicker = ca;
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a", Locale.US);
                date = dateFormat.format(calendarDatePicker.getTime());
                showNewDialog();
            *//*    DatePickerUtils.getInstance(ReservationActivity.this).getTimePickerDialog(ca, new DatePickerUtils.TimeSetListener() {
                    @Override
                    public void onTimeSet(@NonNull TimePickerDialog timePickerDialog, @NonNull Calendar ca) {
                        super.onTimeSet(timePickerDialog, ca);
                        calendar = ca;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                        date = dateFormat.format(calendar.getTime());
                    }
                }, false).show();*//*
            }
        });
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }*/

    private BaseCustomDialog<DialogRegisterBinding> dialogRegister;
    private Calendar calendar;


    private void showNewDialog() {
        calendar = Calendar.getInstance();
        if (dialogRegister != null)
            dialogRegister.dismiss();
        dialogRegister = new BaseCustomDialog<>(this, R.layout.dialog_register, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.submit) {
                    String name = InputUtils.getTrimString(dialogRegister.getBinding().name.getText().toString());
                    String phone = InputUtils.getTrimString(dialogRegister.getBinding().etPhone.getText().toString());
                    String cc = InputUtils.getTrimString(dialogRegister.getBinding().ccp.getFullNumberWithPlus());
                    String amount = InputUtils.getTrimString(dialogRegister.getBinding().emount.getText().toString());
                    // String time = InputUtils.getTrimString(dialogRegister.getBinding().etTime.getText().toString());

                    if (TextUtils.isEmpty(name)) {
                        dialogRegister.getBinding().name.setError("Empty name");
                        return;
                    }
                    if (TextUtils.isEmpty(phone)) {
                        dialogRegister.getBinding().etPhone.setError("Empty phone");
                        return;
                    }
                    if (TextUtils.isEmpty(amount)) {
                        dialogRegister.getBinding().emount.setError("Empty paid amount");
                        return;
                    }
                   /* if (TextUtils.isEmpty(time)) {
                        dialogRegister.getBinding().etTime.setError("Empty time");
                        return;
                    }
*/

                    bookAPi(new SimpleDateFormat("HH", Locale.US).format(calendar.getTime()),
                            new SimpleDateFormat("mm", Locale.US).format(calendar.getTime()),
                            name, cc + phone, amount);
                    //  viewModel.newTicket(serviceId, name, cc + phone, dialogRegister.getBinding().code.getText().toString());
                } /*else if (view.getId() == R.id.et_time) {
                    DatePickerUtils.getInstance(ReservationDetailActivity.this).getDatePickerDialog(calendar, new DatePickerUtils.DateSetListener() {
                        @Override
                        public void onDateSet(@NonNull DatePickerDialog datePickerDialog, @NonNull Calendar ca) {
                            super.onDateSet(datePickerDialog, ca);
                            DatePickerUtils.getInstance(ReservationDetailActivity.this).getTimePickerDialog(ca, new DatePickerUtils.TimeSetListener() {
                                @Override
                                public void onTimeSet(@NonNull TimePickerDialog timePickerDialog, @NonNull Calendar ca) {
                                    super.onTimeSet(timePickerDialog, ca);
                                    calendar = ca;
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                                    dialogRegister.getBinding().etTime.setText(dateFormat.format(calendar.getTime()));
                                    Map<String, String> data = new HashMap<>();
                                    data.put("service_id", String.valueOf(serviceId));
                                    data.put("date", dialogBinding.etTime.getText().toString());
                                    viewModel.getAvailableSlot(data);
                                }
                            }, false).show();
                        }
                    }).show();

                }*/
            }
        });
        dialogRegister.show();
        Map<String, String> data = new HashMap<>();
        data.put("service_id", String.valueOf(serviceId));
        if (date.contains(String.valueOf(calendar.get(Calendar.DATE)))) {
            StringBuilder dateB = new StringBuilder(date);
            dateB.replace(11, 17, "10:50:");
            date = String.valueOf(dateB);
        }
        data.put("date", date);

        viewModel.getAvailableSlot(serviceId, date);

        dialogBinding = dialogRegister.getBinding();
        dialogRegister.getBinding().ccp.setDefaultCountryUsingNameCodeAndApply("EG");
        Window window = dialogRegister.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }


    private void bookAPi(String timeH, String timeM, String
            customerName, String phone, String amount) {

        //"date": "2022-01-13T10:42:32.564Z",
        //"timeH": 0,
        //"timeM": 0,
        //"serviceId": 0,
        //"customerName": "string",
        //"customerPhoneNumber": "string",
        //"paidAmount": 0

        try {
            if (selectedSlotBean == null) {
                viewModel.warn.setValue("Slots not found.");
                return;
            }
            JSONObject object = new JSONObject();
            object.put("date", date);
            object.put("timeH", selectedSlotBean.getStartTime().getHours());
            object.put("timeM", selectedSlotBean.getStartTime().getMinutes());
            object.put("customerName", customerName);
            object.put("customerPhoneNumber", phone);
            object.put("paidAmount", amount);
            object.put("serviceId", serviceId);
            viewModel.book(object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            viewModel.error.setValue(e.getMessage());
        }
    }


}


// Commented duplicate code

  /*  private void showNewDialog() {
        calendar = Calendar.getInstance();
        dialogRegister = new BaseCustomDialog<>(this, R.layout.dialog_calender, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.submit) {
                    String name = InputUtils.getTrimString(dialogRegister.getBinding().name.getText().toString());
                    String phone = InputUtils.getTrimString(dialogRegister.getBinding().etPhone.getText().toString());
                    String cc = InputUtils.getTrimString(dialogRegister.getBinding().ccp.getFullNumberWithPlus());
                    String amount = InputUtils.getTrimString(dialogRegister.getBinding().emount.getText().toString());
                    // String time = InputUtils.getTrimString(dialogRegister.getBinding().etTime.getText().toString());

                    if (TextUtils.isEmpty(name)) {
                        dialogRegister.getBinding().name.setError("Empty name");
                        return;
                    }
                    if (TextUtils.isEmpty(phone)) {
                        dialogRegister.getBinding().etPhone.setError("Empty phone");
                        return;
                    }
                    if (TextUtils.isEmpty(amount)) {
                        dialogRegister.getBinding().emount.setError("Empty paid amount");
                        return;
                    }
                    if (TextUtils.isEmpty(time)) {
                        dialogRegister.getBinding().etTime.setError("Empty time");
                        return;
                    }

                    bookAPi(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US).format(calendar.getTime()),
                            new SimpleDateFormat("HH", Locale.US).format(calendar.getTime()),
                            new SimpleDateFormat("mm", Locale.US).format(calendar.getTime()),
                            name, cc + phone, amount);
                    //  viewModel.newTicket(serviceId, name, cc + phone, dialogRegister.getBinding().code.getText().toString());
                } else if (view.getId() == R.id.etCal) {
                    DatePickerUtils.getInstance(ReservationActivity.this).getDatePickerDialog(calendar, new DatePickerUtils.DateSetListener() {
                        @Override
                        public void onDateSet(@NonNull DatePickerDialog datePickerDialog, @NonNull Calendar ca) {
                            super.onDateSet(datePickerDialog, ca);
                            DatePickerUtils.getInstance(ReservationActivity.this).getTimePickerDialog(ca, new DatePickerUtils.TimeSetListener() {
                                @Override
                                public void onTimeSet(@NonNull TimePickerDialog timePickerDialog, @NonNull Calendar ca) {
                                    super.onTimeSet(timePickerDialog, ca);
                                    calendar = ca;
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
                                    // dialogRegister.getBinding().etCal.setText(dateFormat.format(calendar.getTime()));
                                }
                            }, false).show();
                        }
                    }).show();
                }
            }
        });
        dialogRegister.show();
        dialogRegister.getBinding().ccp.setDefaultCountryUsingNameCodeAndApply("EG");
        Window window = dialogRegister.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }*/


  /*  private void bookAPi(String date, String timeH, String timeM, String customerName, String phone, String amount)
        {
            *//*"date":"2022-01-13T10:42:32.564Z",
                "timeH":0,
                "timeM":0,
                "serviceId":0,
                "customerName":"string",
                "customerPhoneNumber":"string",
                "paidAmount":0*//*


            try {
                JSONObject object = new JSONObject();
                object.put("date", date);
                object.put("timeH", timeH);
                object.put("timeM", timeM);
                object.put("serviceId", serviceId);
                object.put("customerName", customerName);
                object.put("customerPhoneNumber", phone);
                object.put("paidAmount", amount);
                viewModel.book(object.toString());
            } catch (JSONException e) {
                e.printStackTrace();
                viewModel.error.setValue(e.getMessage());
            }
        }
   */

