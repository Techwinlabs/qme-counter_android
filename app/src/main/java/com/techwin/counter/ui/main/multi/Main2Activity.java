package com.techwin.counter.ui.main.multi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.CallTicketbean;
import com.techwin.counter.data.beans.NewTicketBean;
import com.techwin.counter.data.beans.UserBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.databinding.ActivityMain2Binding;
import com.techwin.counter.databinding.DialogAddBinding;
import com.techwin.counter.databinding.DialogAuthBinding;
import com.techwin.counter.databinding.DialogCounterBinding;
import com.techwin.counter.databinding.ViewServicesBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppActivity;
import com.techwin.counter.di.base.view.BaseCustomDialog;
import com.techwin.counter.ui.login.LoginActivity;
import com.techwin.counter.ui.main.MainActivity;
import com.techwin.counter.util.dump.InputUtils;
import com.techwin.counter.util.event.SingleRequestEvent;

import java.util.List;

import javax.inject.Inject;

public class Main2Activity extends AppActivity<ActivityMain2Binding, Main2ActivityVM> {
    @Nullable
    private BaseCustomDialog<DialogAuthBinding> dialogAuth;
    @Nullable
    private BaseCustomDialog<DialogAddBinding> dialogAdd;
    @Nullable
    private BaseCustomDialog<DialogCounterBinding> dialogTimer;
    @Nullable
    private CountDownTimer countDownTimer;
    @Inject
    SharedPref sharedPref;


    @Override
    protected BindingActivity<Main2ActivityVM> getBindingActivity() {
        return new BindingActivity<>(R.layout.activity_main2, Main2ActivityVM.class);
    }

    public static Intent newIntent(Context activity) {
        Intent intent = new Intent(activity, Main2Activity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void subscribeToEvents(final Main2ActivityVM vm) {
        init();
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.btn_resetticket) {
                    UserBean userBean = sharedPref.getUserData();
                    if (userBean != null && userBean.services != null)
                        showAuthResetQueueDialog(userBean.services);
                } else if (view.getId() == R.id.btn_logout) {
                    showLogoutDialog();
                }
            }
        });

        vm.obrNewTicket.observe(this, (SingleRequestEvent.RequestObserver<NewTicketBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    if (dialogAdd != null)
                        dialogAdd.dismiss();
                    vm.success.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.obrCallTicket.observe(this, (SingleRequestEvent.RequestObserver<CallTicketbean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    if (dialogAuth != null)
                        dialogAuth.dismiss();
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    if (dialogAuth != null)
                        dialogAuth.show();
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    showCounterDialog(resource.data);
                    break;
            }
        });
        vm.obrLogout.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
                case SUCCESS:
                    sharedPref.deleteAll();
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    Intent intent = LoginActivity.newIntent(Main2Activity.this);
                    startNewActivity(intent, true);
                    break;
            }
        });
        vm.obrReset.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    if (dialogAuth != null)
                        dialogAuth.dismiss();
                    showProgressDialog(R.string.plz_wait);
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    if (dialogAuth != null)
                        dialogAuth.show();
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    break;
            }
        });

        vm.obrReschedule.observe(this, (SingleRequestEvent.RequestObserver<Void>) resource -> {
            switch (resource.status) {
                case LOADING:
                    if (dialogAuth != null)
                        dialogAuth.dismiss();
                    showProgressDialog(R.string.plz_wait);
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.success.setValue(resource.message);
                    if (dialogTimer != null)
                        dialogTimer.dismiss();
                    break;
                case WARN:
                    dismissProgressDialog();
                    vm.warn.setValue(resource.message);
                    break;
            }
        });


    }


    private void showAuthResetQueueDialog(List<UserBean.Services> services) {
        dialogAuth = new BaseCustomDialog<>(this, R.layout.dialog_auth, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.btn_callticket) {
                    String pin = dialogAuth.getBinding().otpView.getOTP();
                    if (pin.length() > 2) {
                        for (UserBean.Services s : services)
                            viewModel.resetTicket(s.serviceId, pin);

                    }
                }
            }
        });
        dialogAuth.getBinding().btnCallticket.setText(R.string.reset_queue);
        dialogAuth.show();
        Window window = dialogAuth.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }

    private void showAuthCallTicketDialog(UserBean.Services services) {
        dialogAuth = new BaseCustomDialog<>(this, R.layout.dialog_auth, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.btn_callticket) {
                    String pin = dialogAuth.getBinding().otpView.getOTP();
                    if (pin.length() > 2) {
                        viewModel.callTicket(services.serviceId, pin);
                    }
                }
            }
        });

        dialogAuth.show();
        Window window = dialogAuth.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }

    private void showLogoutDialog() {
        dialogAuth = new BaseCustomDialog<>(this, R.layout.dialog_auth, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.btn_callticket) {
                    String pin = dialogAuth.getBinding().otpView.getOTP();
                    if (pin.length() > 2) {
                        viewModel.logout(pin);
                    }
                }
            }
        });
        dialogAuth.getBinding().btnCallticket.setText(R.string.logout);
        dialogAuth.show();
        Window window = dialogAuth.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }


    private void showNewDialog(UserBean.Services services) {
        dialogAdd = new BaseCustomDialog<>(this, R.layout.dialog_add, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.submit) {
                    String name = InputUtils.getTrimString(dialogAdd.getBinding().name.getText().toString());
                    String phone = InputUtils.getTrimString(dialogAdd.getBinding().etPhone.getText().toString());
                    String cc = InputUtils.getTrimString(dialogAdd.getBinding().ccp.getFullNumberWithPlus());

                    if (TextUtils.isEmpty(phone)) {
                        viewModel.error.setValue("Phone empty");
                        return;
                    }

                    viewModel.newTicket(services.serviceId, name, cc + phone, dialogAdd.getBinding().code.getText().toString());
                }
            }
        });
        dialogAdd.show();
        dialogAdd.getBinding().ccp.setDefaultCountryUsingNameCodeAndApply("EG");
        Window window = dialogAdd.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }


    private void showCounterDialog(CallTicketbean data) {
        if (dialogTimer != null)
            dialogTimer.dismiss();
        dialogTimer = new BaseCustomDialog<>(this, R.layout.dialog_counter, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                if (view.getId() == R.id.btn_next && dialogTimer.getBinding().getNext()) {
                    //  viewModel.callNext(data);
                    viewModel.callTicket(data.serviceid, data.pin);
                } else if (view.getId() == R.id.btn_showup) {
                    if (countDownTimer != null)
                        countDownTimer.cancel();
                    dialogTimer.dismiss();
                } else if (view.getId() == R.id.btn_postpone) {
                    viewModel.postpone(data.serviceid);
                }
            }
        });
        dialogTimer.getBinding().setNext(false);
        dialogTimer.setCancelable(true);
        dialogTimer.show();
        dialogTimer.getBinding().setCurrentticket(data.ticketNumber);
        Window window = dialogTimer.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
        countDownTimer = new CountDownTimer(20000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int t = (int) (millisUntilFinished / 1000);
                int progress = (int) ((20000 - millisUntilFinished) / 1000);
                dialogTimer.getBinding().tvTime.setText(String.valueOf(t));
                dialogTimer.getBinding().circularProgressBar.setProgress(progress);
            }

            @Override
            public void onFinish() {
                dialogTimer.getBinding().setNext(true);
            }
        };
        countDownTimer.start();
    }

    @Override
    protected void onDestroy() {
        if (countDownTimer != null)
            countDownTimer.cancel();
        super.onDestroy();
    }


    private void init() {
        SimpleRecyclerViewAdapter<UserBean.Services, ViewServicesBinding> adapter = new SimpleRecyclerViewAdapter<>(R.layout.view_services, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<UserBean.Services>() {
            @Override
            public void onItemClick(View view, UserBean.Services services) {
                if (view.getId() == R.id.btn_newticket) {
                    showNewDialog(services);
                } else if (view.getId() == R.id.btn_callticket) {
                    showAuthCallTicketDialog(services);
                } else if (view.getId() == R.id.logo) {
                    Intent intent = MainActivity.newIntent(Main2Activity.this, services.serviceId);
                    startNewActivity(intent);
                }
            }
        });
        binding.holder.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        binding.holder.setAdapter(adapter);
        UserBean userBean = sharedPref.getUserData();
        if (userBean != null) {
            adapter.setList(userBean.services);

        }

    }


}

