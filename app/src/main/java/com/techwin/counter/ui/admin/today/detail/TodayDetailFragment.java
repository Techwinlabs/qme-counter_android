package com.techwin.counter.ui.admin.today.detail;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.JsonElement;
import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.CustomerBean;
import com.techwin.counter.data.beans.ResvationsBean;
import com.techwin.counter.databinding.DialogFullImageViewBinding;
import com.techwin.counter.databinding.FragmentTodayDetailBinding;
import com.techwin.counter.databinding.HolderImageBinding;
import com.techwin.counter.databinding.HolderTodayDetailBinding;
import com.techwin.counter.databinding.SheetCommentBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppFragment;
import com.techwin.counter.di.base.view.BaseCustomDialog;
import com.techwin.counter.di.base.view.BaseSheet;
import com.techwin.counter.util.decoration.ListVerticalItemDecoration;
import com.techwin.counter.util.event.SingleLiveEvent;
import com.techwin.counter.util.event.SingleRequestEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TodayDetailFragment extends AppFragment<FragmentTodayDetailBinding, TodayDetailFragmentVM> {
    public static final String TAG = "TodayDetailFragment";
    private SimpleRecyclerViewAdapter<CustomerBean.CommentsBean, HolderTodayDetailBinding> viewAdapter;
    public static SingleLiveEvent<String> clickListenerImage = new SingleLiveEvent<>();
    private CustomerBean bean;

    @Override
    protected BindingFragment<TodayDetailFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_today_detail, TodayDetailFragmentVM.class);
    }


    @Override
    protected void subscribeToEvents(final TodayDetailFragmentVM vm) {
        if (getArguments() != null) {
            bean = getArguments().getParcelable("data");
        }
        binding.setBean(bean);
        setBottomeAdater();
        clickListenerImage.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Log.i("TAG", "::::::" + s);
                showImageDialog(s);
            }
        });

        vm.obrTodayData.observe(this, (SingleRequestEvent.RequestObserver<ResvationsBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    break;
                case SUCCESS:
                    if (resource.data != null) {
                        setDataInAdapter(resource.data);
                    }
                    break;
                case ERROR:
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.header) {
                    Navigation.findNavController(view).navigateUp();
                } else if (view.getId() == R.id.iv_comment) {
                    showCommentSheet();
                }
            }
        });
        vm.obrComment.observe(this, (SingleRequestEvent.RequestObserver<JsonElement>) resource -> {
            switch (resource.status) {
                case LOADING:
                    showProgressDialog(R.string.plz_wait);
                    break;
                case SUCCESS:
                    dismissProgressDialog();
                    vm.getTodayStatus();
                    if (sheet != null) {
                        sheet.dismissAllowingStateLoss();
                    }
                    break;
                case ERROR:
                    dismissProgressDialog();
                    vm.error.setValue(resource.message);
                    break;
            }
        });
        vm.getTodayStatus();
    }

    @Nullable
    private BaseCustomDialog<DialogFullImageViewBinding> dialogTimer;

    private void showImageDialog(String image_url) {
        dialogTimer = new BaseCustomDialog<>(getActivity(), R.layout.dialog_full_image_view, new BaseCustomDialog.Listener() {
            @Override
            public void onViewClick(View view) {
                switch (view.getId()) {
                    case R.id.ivClose:
                        dialogTimer.dismiss();
                        break;
                }
            }
        });
        dialogTimer.setCancelable(true);

        ViewTreeObserver vto = dialogTimer.getBinding().ivIcon.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                dialogTimer.getBinding().ivIcon.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalWidth = dialogTimer.getBinding().ivIcon.getMeasuredWidth();
                dialogTimer.getBinding().ivIcon.getLayoutParams().height = finalWidth;
                return true;
            }
        });
        dialogTimer.getBinding().setImage(image_url);

        /*LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100, 100);
        iv.setLayoutParams(layoutParams);*/

        dialogTimer.show();
        Window window = dialogTimer.getWindow();
        if (window != null) {
            window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        }
    }

    private void setDataInAdapter(ResvationsBean data) {
        for (ResvationsBean.DataBean s : data.waitings) {
            if (s.customer.id == bean.id) {
                viewAdapter.setList(s.customer.comments);
                return;
            }
        }
        for (ResvationsBean.DataBean s : data.walkIns) {
            if (s.customer.id == bean.id) {
                viewAdapter.setList(s.customer.comments);
                return;
            }
        }
        for (ResvationsBean.DataBean s : data.noShowReservations) {
            if (s.customer.id == bean.id) {
                viewAdapter.setList(s.customer.comments);
                return;
            }
        }
        for (ResvationsBean.DataBean s : data.reservations) {
            if (s.customer.id == bean.id) {
                viewAdapter.setList(s.customer.comments);
                return;
            }
        }
        for (ResvationsBean.DataBean s : data.finished) {
            if (s.customer.id == bean.id) {
                viewAdapter.setList(s.customer.comments);
                return;
            }
        }

    }


    private void setBottomeAdater() {
        viewAdapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_today_detail, BR.bean, (SimpleRecyclerViewAdapter.SimpleCallback<CustomerBean.CommentsBean>) (v, topData) -> {
            if (v.getId() == R.id.iv_close) {
                viewModel.deleteComment(topData.id);
            }
        });
        binding.rvTwo.addItemDecoration(new ListVerticalItemDecoration(baseContext, R.dimen.dp2));
        binding.rvTwo.setLayoutManager(new LinearLayoutManager(baseContext));
        binding.rvTwo.setAdapter(viewAdapter);

    }


    private BaseSheet<SheetCommentBinding> sheet;
    private SimpleRecyclerViewAdapter<String, HolderImageBinding> adapterImage;
    
   /* private SimpleRecyclerViewAdapter<ImageData, HolderImageCheckBinding> adapterImage1;

    private void setAdapter(){
        adapterImage1=new SimpleRecyclerViewAdapter<>(R.layout.holder_image_check, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<ImageData>() {
            @Override
            public void onItemClick(View v, ImageData imageData) {

            }
        });

    }*/

    private void showCommentSheet() {
        sheet = new BaseSheet<>(R.layout.sheet_comment, new BaseSheet.ClickListener() {
            @Override
            public void onClick(@NonNull View view) {
                if (view.getId() == R.id.btn_cross) {
                    sheet.dismissAllowingStateLoss();
                } else if (view.getId() == R.id.btn_save) {
                    String text = sheet.getBinding().etComment.getText().toString();
                    if (!TextUtils.isEmpty(text)) {
                        List<File> files = new ArrayList<>();
                        for (String s : adapterImage.getList()) {
                            files.add(new File(s));
                        }
                        viewModel.newComment(bean.id, text, files);
                    } else {
                        sheet.getBinding().etComment.setError("Empty comment");
                    }
                } else if (view.getId() == R.id.btn_attach) {
                    openPicker(view);
                }
            }

            @Override
            public void onViewCreated() {
                adapterImage = new SimpleRecyclerViewAdapter<>(R.layout.holder_image, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<String>() {
                    @Override
                    public void onItemClick(View v, String s) {

                    }

                    @Override
                    public void onPositionClick(View v, int pos) {
                        adapterImage.removeItem(pos);
                    }
                });
                sheet.getBinding().rvOne.setLayoutManager(new LinearLayoutManager(baseContext, RecyclerView.HORIZONTAL, false));
                sheet.getBinding().rvOne.setAdapter(adapterImage);

            }
        });
        sheet.setCancelable(false);
        sheet.show(getChildFragmentManager(), sheet.getTag());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                if (uri != null && adapterImage != null) {
                    adapterImage.addData(uri.getPath());
                }
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            viewModel.error.setValue(ImagePicker.getError(data));
        }
    }

    private PopupMenu popupMenu;

    private void openPicker(View view) {
        popupMenu = new PopupMenu(requireContext(), view);
        popupMenu.getMenu().add("Camera");
        popupMenu.getMenu().add("Gallery");
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getTitle().equals("Camera")) {
                    ImagePicker.with(TodayDetailFragment.this)
                            .cropSquare()
                            .cameraOnly()
                            .start();
                } else if (menuItem.getTitle().equals("Gallery")) {
                    ImagePicker.with(TodayDetailFragment.this)
                            .cropSquare()
                            .galleryOnly()
                            .start();
                }
                return true;
            }
        });
        popupMenu.show();

    }
}
