package com.techwin.counter.ui.admin.today.detail;


import com.google.gson.JsonElement;
import com.techwin.counter.data.beans.ResvationsBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.helper.NetworkErrorHandler;
import com.techwin.counter.data.remote.helper.Resource;
import com.techwin.counter.data.repo.MainRepo;
import com.techwin.counter.di.base.viewmodel.BaseViewModel;
import com.techwin.counter.util.event.SingleRequestEvent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;


public class TodayDetailFragmentVM extends BaseViewModel {

    private final SharedPref sharedPref;
    private final NetworkErrorHandler networkErrorHandler;
    public final SingleRequestEvent<ResvationsBean> obrTodayData = new SingleRequestEvent<>();
    final SingleRequestEvent<JsonElement> obrComment = new SingleRequestEvent<>();
    private final MainRepo mainRepo;

    @Inject
    public TodayDetailFragmentVM(SharedPref sharedPref, NetworkErrorHandler networkErrorHandler, MainRepo baseRepo) {
        this.sharedPref = sharedPref;
        this.networkErrorHandler = networkErrorHandler;
        this.mainRepo = baseRepo;
    }

    public void getTodayStatus() {
        mainRepo.getTodayStatus()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ResvationsBean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrTodayData.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull ResvationsBean simpleApiResponse) {
                        obrTodayData.setValue(Resource.success(simpleApiResponse, "Data"));

                    }

                    @Override
                    public void onError(Throwable e) {
                        obrTodayData.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });
    }

    void newComment(int customerId, String text, List<File> files) {

        mainRepo.postComment(String.valueOf(customerId),text, files)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrComment.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        obrComment.setValue(Resource.success(simpleApiResponse, ""));
                    }

                    @Override
                    public void onError(Throwable e) {
                        obrComment.setValue(Resource.error(
                                null, networkErrorHandler.resolveError(e)));

                    }
                });

    }

    void deleteComment(int id) {
      /* JSONObject map = new JSONObject();
        try {
            map.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }*/
        mainRepo.deleteComment(String.valueOf(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<JsonElement>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                        obrComment.setValue(Resource.loading(null));
                    }

                    @Override
                    public void onSuccess(@NotNull JsonElement simpleApiResponse) {
                        obrComment.setValue(Resource.success(simpleApiResponse, ""));
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (networkErrorHandler.getStatusCode(e) == HttpURLConnection.HTTP_NO_CONTENT) {
                            obrComment.setValue(Resource.success(null, "Done"));
                        } else
                            obrComment.setValue(Resource.error(
                                    null, networkErrorHandler.resolveError(e)));

                    }
                });

    }
}
