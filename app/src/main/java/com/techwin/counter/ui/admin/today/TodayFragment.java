package com.techwin.counter.ui.admin.today;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.techwin.counter.BR;
import com.techwin.counter.R;
import com.techwin.counter.data.beans.ResvationsBean;
import com.techwin.counter.data.remote.helper.Status;
import com.techwin.counter.databinding.FragmentTodayBinding;
import com.techwin.counter.databinding.HolderTodayBinding;
import com.techwin.counter.databinding.HolderTopBinding;
import com.techwin.counter.di.base.adapter.SimpleRecyclerViewAdapter;
import com.techwin.counter.di.base.view.AppFragment;
import com.techwin.counter.util.decoration.ListVerticalItemDecoration;
import com.techwin.counter.util.event.SingleRequestEvent;

import java.util.ArrayList;
import java.util.List;

public class TodayFragment extends AppFragment<FragmentTodayBinding, TodayFragmentVM> {
    public static final String TAG = "TodayFragment";
    public static final String COLOR_WAITING = "#E1E1E1",
            COLOR_FINSHED = "#FFCDF2C4",
            COLOR_RESERVATION = "#E1E1E1",
            COLOR_NOTSHOW = "#FFF8D3CB",
            COLOR_WALKIN = "#E1E1E1";


    private SimpleRecyclerViewAdapter<TopData, HolderTopBinding> viewAdapter;
    @Nullable
    private ResvationsBean bean;

    @Override
    protected BindingFragment<TodayFragmentVM> getBindingFragment() {
        return new BindingFragment<>(R.layout.fragment_today, TodayFragmentVM.class);
    }


    @Override
    protected void subscribeToEvents(final TodayFragmentVM vm) {
        setInitAdater();
        setBottomeAdater();
        vm.obrTodayData.observe(this, (SingleRequestEvent.RequestObserver<ResvationsBean>) resource -> {
            switch (resource.status) {
                case LOADING:
                    break;
                case SUCCESS:
                    setData(resource.data);
                    break;
                case ERROR:
                    if (resource.message != null && !resource.message.equalsIgnoreCase("Forbidden")) {
                        vm.error.setValue(resource.message);
                    }
                    break;
            }
        });
        vm.onClick.observe(this, new Observer<View>() {
            @Override
            public void onChanged(View view) {
                if (view.getId() == R.id.iv_search) {
                    showSearchView();
                } else if (view.getId() == R.id.tv_cancel) {
                    hideSearchView();
                }
            }
        });
        if (vm.obrTodayData.getValue() != null && vm.obrTodayData.getValue().status == Status.SUCCESS) {
            setData(vm.obrTodayData.getValue().data);
        }

    }

    private void setData(ResvationsBean data) {
        if (data != null) {
            bean = data;
            List<TopData> topData = new ArrayList<>();
            topData.add(new TopData("Waiting", String.valueOf(data.waitings.size()), COLOR_WAITING));
            topData.add(new TopData("Finished", String.valueOf(data.finished.size()), COLOR_FINSHED));
            topData.add(new TopData("Reservations", String.valueOf(data.reservations.size()), COLOR_RESERVATION));
            topData.add(new TopData("Not show", String.valueOf(data.noShowReservations.size()), COLOR_NOTSHOW));
            topData.add(new TopData("Walk In", String.valueOf(data.walkIns.size()), COLOR_WALKIN));
            viewAdapter.setList(topData);

            adapter.clearList();
            setBottomData(data.reservations, COLOR_RESERVATION);
            setBottomData(data.waitings, COLOR_WAITING);
            setBottomData(data.noShowReservations, COLOR_NOTSHOW);
            setBottomData(data.walkIns, COLOR_WALKIN);
            setBottomData(data.finished, COLOR_FINSHED);

        }
    }

    private void setBottomData(List<ResvationsBean.DataBean> data, String color) {
        for (ResvationsBean.DataBean bean : data) {
            TopData topData = new TopData("", "", color);
            topData.dataBean = bean;
            adapter.addData(topData);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        viewModel.getTodayStatus();
    }

    private void hideSearchView() {
        binding.header.setSearch(false);
        binding.header.etSearch.removeTextChangedListener(textWatcher);
    }

    private void showSearchView() {
        binding.header.etSearch.setHint("Search Customer Name/Mobile no");
        binding.header.etSearch.addTextChangedListener(textWatcher);
        binding.header.setSearch(true);
    }


    private void setInitAdater() {
        viewAdapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_top, BR.bean, (v, topData) -> {

            //setBottomData(topData.dataBean, COLOR_FINSHED);
        });
        binding.rvTop.setLayoutManager(new GridLayoutManager(baseContext, 3));
        binding.rvTop.setAdapter(viewAdapter);
        List<TopData> topData = new ArrayList<>();
        topData.add(new TopData("Waiting", null, "#E1E1E1"));
        topData.add(new TopData("Finished", null, "#FFCDF2C4"));
        topData.add(new TopData("Reservations", null, "#E1E1E1"));
        topData.add(new TopData("Not show", null, "#FFF8D3CB"));
        topData.add(new TopData("Walk In", null, "#E1E1E1"));
        viewAdapter.setList(topData);
    }

    private SimpleRecyclerViewAdapter<TopData, HolderTodayBinding> adapter;

    private void setBottomeAdater() {
        adapter = new SimpleRecyclerViewAdapter<>(R.layout.holder_today, BR.bean, new SimpleRecyclerViewAdapter.SimpleCallback<TopData>() {
            @Override
            public void onItemClick(View v, TopData topData) {
                if (v.getId() == R.id.iv_comment) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("data", topData.dataBean.customer);
                    Navigation.findNavController(v).navigate(R.id.action_frg_today_to_frg_today_detail, bundle);
                }
            }
        });
        binding.rvTwo.addItemDecoration(new ListVerticalItemDecoration(baseContext, R.dimen.dp2));
        binding.rvTwo.setLayoutManager(new LinearLayoutManager(baseContext));
        binding.rvTwo.setAdapter(adapter);
    }

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    public static class TopData {
        public String key;
        public String value;
        public String color;
        public boolean checked;
        public ResvationsBean.DataBean dataBean;

        public TopData(String key, String value, String color) {
            this.key = key;
            this.value = value;
            this.color = color;
        }

        public TopData(String key, String value, String color, boolean checked) {
            this.key = key;
            this.value = value;
            this.color = color;
            this.checked = checked;
        }
    }
}
