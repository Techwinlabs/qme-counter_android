package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResvationsBean {

    @SerializedName("waitings")
    public List<DataBean> waitings;
    @SerializedName("finished")
    public List<DataBean> finished;
    @SerializedName("reservations")
    public List<DataBean> reservations;
    @SerializedName("noShowReservations")
    public List<DataBean> noShowReservations;
    @SerializedName("walkIns")
    public List<DataBean> walkIns;
    @SerializedName("allCustomers")
    public List<DataBean> allCustomers;

    public static class DataBean {

        @SerializedName("id")
        public String id;
        @SerializedName("code")
        public String code;
        @SerializedName("ticketNumber")
        public String ticketNumber;
        @SerializedName("customer")
        public CustomerBean customer;
        @SerializedName("time")
        public TimeBean timeBean;

        public static class TimeBean {
            @SerializedName("hours")
            public int hours;
            @SerializedName("minutes")
            public int minutes;
        }
    }


}
