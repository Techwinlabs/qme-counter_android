package com.techwin.counter.data.beans;

public class ImageData {
    public String image;
    public boolean imageCheck;

    public ImageData(String image) {
        this.image = image;
        this.imageCheck = false;
    }

    public ImageData(String image, boolean imageCheck) {
        this.image = image;
        this.imageCheck = imageCheck;
    }

    public String getImage() {
        return image;

    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isImageCheck() {
        return imageCheck;
    }

    public void setImageCheck(boolean imageCheck) {
        this.imageCheck = imageCheck;
    }
}
