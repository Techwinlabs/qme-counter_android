package com.techwin.counter.data.remote.intersepter;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Arvind Poonia on 12/12/2018.
 */
public class RequestInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request originalRequest = chain.request();
        Request.Builder requestBuilder = originalRequest.newBuilder();
        Response response = chain.proceed(requestBuilder.build());

        // Throw specific Exception on HTTP 204 response code
        if (response.code() == 204) {
            throw new NoContentException("There is no content");
        }

        return response; // Carry on with the response
    }

    public static class NoContentException extends IOException {
        public NoContentException(String content) {
        }
    }
}
