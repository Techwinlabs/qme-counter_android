package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReservationListBean {
    @SerializedName("days")
    public List<DaysBean> days;

}
