package com.techwin.counter.data.repo;

import com.google.gson.JsonElement;
import com.techwin.counter.data.beans.CallTicketbean;
import com.techwin.counter.data.beans.CounterBean;
import com.techwin.counter.data.beans.CustomerDataBean;
import com.techwin.counter.data.beans.GetDayBean;
import com.techwin.counter.data.beans.GetQueueAvailability;
import com.techwin.counter.data.beans.GetReservationBean;
import com.techwin.counter.data.beans.GetReservationByDateBean;
import com.techwin.counter.data.beans.InvoiceBean;
import com.techwin.counter.data.beans.MonthsDataBean;
import com.techwin.counter.data.beans.NewTicketBean;
import com.techwin.counter.data.beans.ProfileBean;
import com.techwin.counter.data.beans.ReservationListBean;
import com.techwin.counter.data.beans.ResvationsBean;
import com.techwin.counter.data.beans.UserBean;
import com.techwin.counter.data.beans.WorkingDaysBean;
import com.techwin.counter.data.beans.base.AvailableSlotsBean;
import com.techwin.counter.data.beans.request.RescheduleBean;

import java.io.File;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;

public interface MainRepo {
    Single<JsonElement> book(String body);

    Single<JsonElement> pay(String body);

    Single<JsonElement> confirmOtp(String body);

    Single<JsonElement> refreshToken(String s);

    Single<List<InvoiceBean>> Invoices();

    Single<JsonElement> getWallet();

    Single<WorkingDaysBean> GetWorkingDays();

    Single<ProfileBean> getProfile();

    Single<ResvationsBean> GetDayStatus(String date);

    Single<GetDayBean> GetDay(String date);

    Single<ResvationsBean> getTodayStatus();

    Single<UserBean> login(String body);

    Single<CounterBean> counter(int serviceId);

    Single<List<CustomerDataBean>> getCustomersData(int serviceId, int branchId);

    Single<NewTicketBean> issueTicket(String body);

    Single<JsonElement> postComment(String custId, String text, List<File> files);

    Single<JsonElement> deleteComment(String body);

    Single<JsonElement> reschedule(String serviceId);

    Single<JsonElement> rescheduleCopy(RescheduleBean body);

    Single<CallTicketbean> callTicket(String body);

    Single<JsonElement> logout(String body);

    Single<JsonElement> CallNext(String body);

    Single<JsonElement> reset(String body);

    Single<List<GetReservationBean>> getReservationList(int serviceId);

    Single<ReservationListBean> GetWeekStatus(String count);

    Single<List<GetReservationByDateBean>> getReservationByDate(int serviceId, String date);

    Single<MonthsDataBean> getMonthStatus(String date);

    Single<JsonElement> UpdateWorkingDay(String body);

    Single<List<AvailableSlotsBean>> getAvailableSlots(Map<String, String> data);

    Single<List<AvailableSlotsBean>> getAvailableSlotsCopy(int serviceId, String date);

    Single<GetQueueAvailability> getQueueAvailability(int serviceId);

    Single<JsonElement> cancelBooking(String data);
}
