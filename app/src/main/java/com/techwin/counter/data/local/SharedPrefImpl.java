package com.techwin.counter.data.local;

import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.techwin.counter.data.beans.CardBean;
import com.techwin.counter.data.beans.UserBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SharedPrefImpl implements SharedPref {
    private SharedPreferences sharedPreferences;

    public SharedPrefImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public String addCard(CardBean value) {
        String message = "Added";
        if (value == null)
            return "Invalid Card";
        List<CardBean> cardBeanList = getCards();
        int index = 0;
        for (int i = 0; i < cardBeanList.size(); i++) {
            if (cardBeanList.get(i).account.equals(value.account)) {
                cardBeanList.remove(i);
                index = i;
                message = "Updated";
                break;
            }
        }
        cardBeanList.add(index, value);
        sharedPreferences.edit().putString("card", new Gson().toJson(cardBeanList)).apply();
        return message;
    }

    @Override
    public void removeCard(CardBean value) {
        if (value == null)
            return;
        List<CardBean> cardBeanList = getCards();
        for (int i = 0; i < cardBeanList.size(); i++) {
            if (cardBeanList.get(i).id == (value.id)) {
                cardBeanList.remove(i);
                break;
            }
        }
        sharedPreferences.edit().putString("card", new Gson().toJson(cardBeanList)).apply();
    }


    @Override
    public List<CardBean> getCards() {
        try {
            return new Gson().fromJson(sharedPreferences.getString("card", "[]"), new TypeToken<ArrayList<CardBean>>() {
            }.getType());
        } catch (Exception e) {
            removeAllCards();
            return Collections.emptyList();
        }
    }

    @Override
    public void removeAllCards() {
        sharedPreferences.edit().remove("card").apply();
    }

    @Override
    public void putUserData(@NonNull UserBean value) {
        sharedPreferences.edit().putString("userdata", new Gson().toJson(value)).apply();
    }

    @Override
    public UserBean getUserData() {
        try {
            return new Gson().fromJson(sharedPreferences.getString("userdata", null), UserBean.class);
        } catch (JsonSyntaxException e) {
            return null;
        }
    }

    @Override
    public void setDefaultLocale(String locale) {
        sharedPreferences.edit().putString("locale", locale).apply();
    }

    @Override
    public String getDefaultLocale(String locale) {
        return sharedPreferences.getString("locale", locale);
    }

    @Override
    public void put(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    @Override
    public int get(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    @Override
    public void put(String key, float value) {
        sharedPreferences.edit().putFloat(key, value).apply();
    }

    @Override
    public float get(String key, float defaultValue) {
        return sharedPreferences.getFloat(key, defaultValue);
    }

    @Override
    public void put(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    @Override
    public boolean get(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    @Override
    public void put(String key, long value) {
        sharedPreferences.edit().putLong(key, value).apply();
    }

    @Override
    public long get(String key, long defaultValue) {
        return sharedPreferences.getLong(key, defaultValue);
    }

    @Override
    public void put(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    @Override
    public String get(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }

    @Override
    public void delete(String key) {
        sharedPreferences.edit().remove(key).apply();
    }

    @Override
    public void deleteAll() {
        sharedPreferences.edit().clear().apply();
    }
}
