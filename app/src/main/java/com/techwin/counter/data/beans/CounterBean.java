package com.techwin.counter.data.beans;

public class CounterBean {

    public long id;
    public String currentServedTicket;
    public String lastIssuedTicket;
    public String totalCustomersInQueue;

}
