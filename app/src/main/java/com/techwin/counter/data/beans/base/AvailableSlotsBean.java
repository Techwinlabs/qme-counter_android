package com.techwin.counter.data.beans.base;

import com.google.gson.annotations.SerializedName;

public class AvailableSlotsBean  {

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    /**
     * hours : 0
     * minutes : 0
     */

    public boolean checked;

    @SerializedName("startTime")
    private StartTimeBean startTime;
    /**
     * hours : 0
     * minutes : 0
     */

    @SerializedName("endTime")
    private EndTimeBean endTime;

    public StartTimeBean getStartTime() {
        return startTime;
    }

    public void setStartTime(StartTimeBean startTime) {
        this.startTime = startTime;
    }

    public EndTimeBean getEndTime() {
        return endTime;
    }

    public void setEndTime(EndTimeBean endTime) {
        this.endTime = endTime;
    }

    public static class StartTimeBean {
        @SerializedName("hours")
        private String hours;
        @SerializedName("minutes")
        private String minutes;

        public String getHours() {
            return hours;
        }

        public void setHours(String hours) {
            this.hours = hours;
        }

        public String getMinutes() {
            return minutes;
        }

        public void setMinutes(String minutes) {
            this.minutes = minutes;
        }
    }

    public static class EndTimeBean {
        @SerializedName("hours")
        private String hours;
        @SerializedName("minutes")
        private String minutes;

        public String getHours() {
            return hours;
        }

        public void setHours(String hours) {
            this.hours = hours;
        }

        public String getMinutes() {
            return minutes;
        }

        public void setMinutes(String minutes) {
            this.minutes = minutes;
        }
    }
}
