package com.techwin.counter.data.beans;

public class NewTicketBean {
    public String status;
    public String message;
    public long ticketNumber;
    public String customersInQueue;
    public String qrCode;
}
