package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

public class GetDayBean {

    @SerializedName("date")
    public String date;
    @SerializedName("startTime")
    public StartTimeBean startTime;
    @SerializedName("endTime")
    public EndTimeBean endTime;
    @SerializedName("isWorking")
    public boolean isWorking;

    public static class StartTimeBean {
        @SerializedName("hours")
        public int hours;
        @SerializedName("minutes")
        public int minutes;
    }

    public static class EndTimeBean {
        @SerializedName("hours")
        public int hours;
        @SerializedName("minutes")
        public int minutes;
    }
}
