package com.techwin.counter.data.remote.api;

import androidx.annotation.Nullable;

import com.google.gson.JsonElement;
import com.techwin.counter.data.beans.CallTicketbean;
import com.techwin.counter.data.beans.CounterBean;
import com.techwin.counter.data.beans.CustomerDataBean;
import com.techwin.counter.data.beans.GetDayBean;
import com.techwin.counter.data.beans.GetQueueAvailability;
import com.techwin.counter.data.beans.GetReservationBean;
import com.techwin.counter.data.beans.GetReservationByDateBean;
import com.techwin.counter.data.beans.InvoiceBean;
import com.techwin.counter.data.beans.MonthsDataBean;
import com.techwin.counter.data.beans.NewTicketBean;
import com.techwin.counter.data.beans.ProfileBean;
import com.techwin.counter.data.beans.ReservationListBean;
import com.techwin.counter.data.beans.ResvationsBean;
import com.techwin.counter.data.beans.UserBean;
import com.techwin.counter.data.beans.WorkingDaysBean;
import com.techwin.counter.data.beans.base.AvailableSlotsBean;
import com.techwin.counter.data.beans.request.RescheduleBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface MainApi {
    @Headers("Application-Type:Counter")
    @POST("api/Accounts/CountersLogin")
    Single<UserBean> login(@Body RequestBody body);

    @Headers("Application-Type:Controls")
    @POST("api/Accounts/RefreshToken")
    Single<JsonElement> refreshToken(@Header("Authorization") String header, @Body RequestBody body);

    @DELETE("api/Accounts/Logout")
    Single<JsonElement> logout(@Header("Authorization") String header/*, @Body RequestBody body*/);

    @Headers("Application-Type:Counter")
    @GET("api/Counters")
    Single<CounterBean> counter(@Header("Authorization") String header, @Query("serviceId") int serviceId);

    @GET("api/Counters/GetCustomersData")
    Single<List<CustomerDataBean>> getCustomersData(@Header("Authorization") String header, @Query("serviceId") int serviceId);

    @Headers("Application-Type:Counter")
    @POST("api/Counters/IssueTicket")
    Single<NewTicketBean> issueTicket(@Header("Authorization") String header, @Body RequestBody body);

    @PUT("/api/Counters/CallTicket")
    Single<CallTicketbean> callTicket(@Header("Authorization") String header, @Body RequestBody body);

    //@DELETE("api/Counters/Reset")
    @HTTP(method = "DELETE", path = "api/Counters/Reset", hasBody = true)
    Single<JsonElement> reset(@Header("Authorization") String header, @Body RequestBody body);


    @HTTP(method = "DELETE", path = "api/Counters/CancelBooking", hasBody = true)
    Single<JsonElement> cancelBooking(@Header("Authorization") String header, @Body RequestBody body);



    /*
     @DELETE("api/Counters/cancelBooking")
    Single<JsonElement> cancelBooking(@Header("Authorization") String header);
    */

    @PUT("api/Counters/CallNext")
    Single<JsonElement> CallNext(@Header("Authorization") String header, @Body RequestBody body);

    @PUT("api/Counters/Reschedule")
    Single<JsonElement> reschedule(@Header("Authorization") String header, @Body RequestBody body);

    @PUT("api/Counters/Reschedule")
    Single<JsonElement> rescheduleCopy(@Header("Authorization") String header, @Body RescheduleBean body);

    @PUT("api/Controls/UpdateWorkingDay")
    Single<JsonElement> UpdateWorkingDay(@Header("Authorization") String header, @Body RequestBody body);

    @GET("api/Counters/GetReservationsList")
    Single<List<GetReservationBean>> getReservationList(@Header("Authorization") String header, @Query("serviceId") int serviceId);

    @GET("/api/Counters/GetReservationsByDate")
    Single<List<GetReservationByDateBean>> getReservationByDate(@Header("Authorization") String header, @Query("ServiceId") int serviceId, @Query("Date") String date);

    @POST("api/Counters/Book")
    Single<JsonElement> book(@Header("Authorization") String header, @Body RequestBody body);

    @Headers("Application-Type:Controls")
    @GET("/api/Transactions/GetWallet")
    Single<JsonElement> getWallet(@Header("Authorization") String header);


    @GET("/api/Controls/GetTodayStatus")
    Single<ResvationsBean> getTodayStatus(@Header("Authorization") String header);

    @GET("/api/Invoices")
    Single<List<InvoiceBean>> Invoices(@Header("Authorization") String header);

    @POST("/api/Invoices/pay")
    Single<JsonElement> pay(@Header("Authorization") String header, @Body RequestBody body);

    @PUT("/api/Invoices/confirm-otp")
    Single<JsonElement> confirmOtp(@Header("Authorization") String header, @Body RequestBody body);

    @GET("/api/Controls/GetMonthStatus")
    Single<MonthsDataBean> getMonthStatus(@Header("Authorization") String header, @Query("Date") String date);

    @GET("/api/Controls/GetDayStatus")
    Single<ResvationsBean> GetDayStatus(@Header("Authorization") String header, @Query("Date") String date);

    @GET("/api/Controls/GetDay")
    Single<GetDayBean> GetDay(@Header("Authorization") String header, @Query("Date") String date);

    @GET("api/Counters/GetAvailableSlots")
    Single<List<AvailableSlotsBean>> getAvailableSlots(@Header("Authorization") String header, @QueryMap Map<String, String> data);

    @GET("api/Counters/GetAvailableSlots")
    Single<List<AvailableSlotsBean>> getAvailableSlotsCopy(@Header("Authorization") String header, @Query("serviceId") int serviceId, @Query("date") String date);

    @GET("api/Counters/GetQueueAvailability")
    Single<GetQueueAvailability> getQueueAvailability(@Header("Authorization") String header, @Query("serviceId") int serviceId);


    @GET("/api/Controls/GetWorkingDays")
    Single<WorkingDaysBean> GetWorkingDays(@Header("Authorization") String header);

    @GET("/api/Controls/GetWeekStatus")
    Single<ReservationListBean> GetWeekStatus(@Header("Authorization") String header, @Query("Count") String count);

    @GET("/api/Controls/GetProfile")
    Single<ProfileBean> getProfile(@Header("Authorization") String header);

    @Headers("Application-Type:Controls")
    @Multipart
    @POST("api/Controls/PostComment")
    Single<JsonElement> postComment(@Header("Authorization") String header, @PartMap Map<String, RequestBody> body, @Nullable @Part MultipartBody.Part[] multipartBodies);

    @Multipart
    @POST("api/Controls/PostComment")
    Single<JsonElement> postComment(@Header("Authorization") String header, @PartMap Map<String, RequestBody> body);

    @DELETE("api/Controls/DeleteComment")
    Single<JsonElement> deleteComment(@Header("Authorization") String header, @Query("id") String body);

}
