package com.techwin.counter.data.beans.request;

public class RescheduleBean {
    private int serviceId;

    public RescheduleBean(int serviceId) {
        this.serviceId = serviceId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }
}
