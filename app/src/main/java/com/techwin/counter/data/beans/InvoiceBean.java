package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class InvoiceBean  implements Serializable {
    @SerializedName("id")
    public long id;
    @SerializedName("totalAmount")
    public long totalAmount;
    @SerializedName("issuedAt")
    public String issuedAt;
    @SerializedName("paidAt")
    public String paidAt;
}
