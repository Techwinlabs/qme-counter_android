package com.techwin.counter.data.repo;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.techwin.counter.data.beans.CallTicketbean;
import com.techwin.counter.data.beans.CounterBean;
import com.techwin.counter.data.beans.CustomerDataBean;
import com.techwin.counter.data.beans.GetDayBean;
import com.techwin.counter.data.beans.GetQueueAvailability;
import com.techwin.counter.data.beans.GetReservationBean;
import com.techwin.counter.data.beans.GetReservationByDateBean;
import com.techwin.counter.data.beans.InvoiceBean;
import com.techwin.counter.data.beans.MonthsDataBean;
import com.techwin.counter.data.beans.NewTicketBean;
import com.techwin.counter.data.beans.ProfileBean;
import com.techwin.counter.data.beans.ReservationListBean;
import com.techwin.counter.data.beans.ResvationsBean;
import com.techwin.counter.data.beans.UserBean;
import com.techwin.counter.data.beans.WorkingDaysBean;
import com.techwin.counter.data.beans.base.AvailableSlotsBean;
import com.techwin.counter.data.beans.request.RescheduleBean;
import com.techwin.counter.data.local.SharedPref;
import com.techwin.counter.data.remote.api.MainApi;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MainRepoImpl implements MainRepo {
    private final SharedPref sharedPref;
    private final MainApi mainApi;

    public MainRepoImpl(SharedPref sharedPref, MainApi mainApi) {
        this.sharedPref = sharedPref;
        this.mainApi = mainApi;
    }

    @Override
    public Single<JsonElement> book(String body) {
        return mainApi.book(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), body));
    }

    @Override
    public Single<JsonElement> pay(String body) {
        return mainApi.pay(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), body));
    }

    @Override
    public Single<JsonElement> confirmOtp(String body) {
        return mainApi.confirmOtp(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), body));

    }

    @Override
    public Single<JsonElement> refreshToken(String s) {
        return mainApi.refreshToken(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), s)).doOnSuccess(new Consumer<JsonElement>() {
            @Override
            public void accept(JsonElement resource) throws Exception {
                if (resource != null) {
                    UserBean userBean = sharedPref.getUserData();
                    JsonObject object = resource.getAsJsonObject();
                    if (object.has("token")) {
                        userBean.token = object.get("token").getAsString();
                    }
                    if (object.has("refreshToken")) {
                        userBean.refreshToken = object.get("refreshToken").getAsString();
                    }
                    sharedPref.putUserData(userBean);
                }
            }
        });
    }

    @Override
    public Single<List<InvoiceBean>> Invoices() {
        return mainApi.Invoices(getHeaderToken());
    }

    @Override
    public Single<JsonElement> getWallet() {
        return mainApi.getWallet(getHeaderToken());
    }

    @Override
    public Single<WorkingDaysBean> GetWorkingDays() {
        return mainApi.GetWorkingDays(getHeaderToken());
    }

    @Override
    public Single<ProfileBean> getProfile() {
        return mainApi.getProfile(getHeaderToken());
    }

    @Override
    public Single<ResvationsBean> GetDayStatus(String date) {
        return mainApi.GetDayStatus(getHeaderToken(), date);
    }

    @Override
    public Single<GetDayBean> GetDay(String date) {
        return mainApi.GetDay(getHeaderToken(), date);
    }

    @Override
    public Single<ResvationsBean> getTodayStatus() {
        return mainApi.getTodayStatus(getHeaderToken());
    }

    @Override
    public Single<UserBean> login(String body) {
        return mainApi.login(RequestBody.create(MediaType.get("application/json"), body)).doOnSuccess(new Consumer<UserBean>() {
            @Override
            public void accept(UserBean userBean) throws Exception {
                sharedPref.putUserData(userBean);

            }
        });
    }

    @Override
    public Single<CounterBean> counter(int serviceId) {

        return mainApi.counter(getHeaderToken(), serviceId);
    }

    @Override
    public Single<List<CustomerDataBean>> getCustomersData(int serviceId, int branchId) {
        return mainApi.getCustomersData(getHeaderToken(), serviceId);
    }

    @Override
    public Single<NewTicketBean> issueTicket(String body) {
        return mainApi.issueTicket(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), body));
    }

    @Override
    public Single<JsonElement> postComment(String custId, String text, List<File> files) {
        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        requestBodyMap.put("customerId", RequestBody.create(MediaType.get("text/plain"), custId));
        requestBodyMap.put("content", RequestBody.create(MediaType.get("text/plain"), text));
        if (files != null && files.size() > 0) {
            MultipartBody.Part[] parts = new MultipartBody.Part[files.size()];
            for (int index = 0; index < files.size(); index++) {
                RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpeg"), files.get(index));
                parts[index] = MultipartBody.Part.createFormData("Attachments", files.get(index).getName(), requestBody);
            }
            return mainApi.postComment(getHeaderToken(), requestBodyMap, parts);
        } else {

            return mainApi.postComment(getHeaderToken(), requestBodyMap);
        }
    }

    @Override
    public Single<JsonElement> deleteComment(String body) {
        return mainApi.deleteComment(getHeaderToken(), body);
    }

    @Override
    public Single<JsonElement> reschedule(String serviceId) {
        return mainApi.reschedule(getHeaderToken(), RequestBody.create(MediaType.parse("application/json"), serviceId));
    }

    @Override
    public Single<JsonElement> rescheduleCopy(RescheduleBean body) {
        return mainApi.rescheduleCopy(getHeaderToken(), body);
    }

    @Override
    public Single<CallTicketbean> callTicket(String body) {
        return mainApi.callTicket(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), body));

    }

    @Override
    public Single<JsonElement> logout(String body) {
        return mainApi.logout(getHeaderToken()/* RequestBody.create(MediaType.get("application/json"), body)*/);

    }

    @Override
    public Single<JsonElement> CallNext(String body) {
        return mainApi.CallNext(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), body));

    }

    @Override
    public Single<JsonElement> reset(String body) {
        return mainApi.reset(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), body));

    }

    @Override
    public Single<List<GetReservationBean>> getReservationList(int serviceId) {
        return mainApi.getReservationList(getHeaderToken(), serviceId);
    }

    @Override
    public Single<ReservationListBean> GetWeekStatus(String count) {
        return mainApi.GetWeekStatus(getHeaderToken(), count);
    }

    @Override
    public Single<List<GetReservationByDateBean>> getReservationByDate(int serviceId, String date) {
        return mainApi.getReservationByDate(getHeaderToken(), serviceId, date);
    }

    @Override
    public Single<MonthsDataBean> getMonthStatus(String date) {
        return mainApi.getMonthStatus(getHeaderToken(), date);
    }

    @Override
    public Single<JsonElement> UpdateWorkingDay(String body) {
        return mainApi.UpdateWorkingDay(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), body));
    }

    @Override
    public Single<List<AvailableSlotsBean>> getAvailableSlots(Map<String, String> data) {
        return mainApi.getAvailableSlots(getHeaderToken(), data);
    }

    @Override
    public Single<List<AvailableSlotsBean>> getAvailableSlotsCopy(int serviceId, String date) {
        return mainApi.getAvailableSlotsCopy(getHeaderToken(), serviceId, date);
    }

    @Override
    public Single<GetQueueAvailability> getQueueAvailability(int serviceId) {
        return mainApi.getQueueAvailability(getHeaderToken(), serviceId);
    }

    @Override
    public Single<JsonElement> cancelBooking(String object) {
        return mainApi.cancelBooking(getHeaderToken(), RequestBody.create(MediaType.get("application/json"), object));
    }

    private String getHeaderToken() {
        String header = "";
        UserBean userBean = sharedPref.getUserData();
        if (userBean != null)
            header = "Bearer " + userBean.token;
        return header;
    }
}
