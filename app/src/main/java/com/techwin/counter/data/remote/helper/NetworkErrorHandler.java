package com.techwin.counter.data.remote.helper;

import android.content.Context;

import com.techwin.counter.MyApplication;
import com.techwin.counter.R;
import com.techwin.counter.data.remote.intersepter.RequestInterceptor;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;

import dagger.android.support.DaggerApplication;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class NetworkErrorHandler {

    private Context context;

    public NetworkErrorHandler(DaggerApplication context) {
        this.context = context;
    }

    public String resolveError(Throwable throwable) {
        return resolveError(throwable, true);
    }

    public String resolveError(Throwable throwable, boolean restartApp) {
        String errMsg = context.getString(R.string.error_found);
        throwable.printStackTrace();
        if (throwable instanceof HttpException) {
            errMsg = getErrorMessage(throwable);
            HttpException exception = (HttpException) throwable;
            if (restartApp) {
                if (exception.code() == HttpURLConnection.HTTP_UNAUTHORIZED)
                    MyApplication.getInstance().restartApp();
            }
        } else if (throwable instanceof IOException) {
            errMsg = context.getString(R.string.network_error);
        } else {
            if (throwable.getMessage() != null)
                errMsg = throwable.getMessage();
        }

        return errMsg;
    }

    public int getStatusCode(Throwable throwable) {
        if (throwable instanceof RequestInterceptor.NoContentException) {
            return HttpURLConnection.HTTP_NO_CONTENT;
        }
        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            return exception.code();
        }
        return 0;
    }


    public String getErrorMessage(Throwable throwable) {
        try {
            HttpException httpException = (HttpException) throwable;
            ResponseBody errorBody = httpException.response().errorBody();
            String errMsg = context.getString(R.string.error_found);
            if (errorBody != null) {
                JSONObject jsonObject = new JSONObject(errorBody.string());
                if (jsonObject.has("errors")) {
                    errMsg = jsonObject.getString("errors");
                    JSONObject oj = new JSONObject(errMsg);
                    if (oj.has("date")) {
                        JSONArray array = (JSONArray) oj.get("date");
                        errMsg = (String) array.get(0);
                    }
                } else if (jsonObject.has("detail"))
                    errMsg = jsonObject.getString("detail");
                else if (jsonObject.has("title"))
                    errMsg = jsonObject.getString("title");
            }

            return errMsg;

        } catch (Exception e) {
            return ((HttpException) throwable).message();
        }
    }
}
