package com.techwin.counter.data.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerBean implements Parcelable {
    @SerializedName("id")
    public int id;
    @SerializedName("name")
    public String name;
    @SerializedName("phoneNumber")
    public String phoneNumber;
    @SerializedName("comments")
    public List<CommentsBean> comments;

    public static class CommentsBean implements Parcelable {
        @SerializedName("id")
        public int id;
        @SerializedName("content")
        public String content;
        @SerializedName("createdAt")
        public String createdAt;
        @SerializedName("attachments")
        public String[] attachments;


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.content);
            dest.writeString(this.createdAt);
            dest.writeArray(this.attachments);
        }

        public void readFromParcel(Parcel source) {
            this.id = source.readInt();
            this.content = source.readString();
            this.createdAt = source.readString();
            //this.attachments = source.readStringArray();
        }

        public CommentsBean() {
        }

        protected CommentsBean(Parcel in) {
            this.id = in.readInt();
            this.content = in.readString();
            this.createdAt = in.readString();
            this.attachments = (String[]) in.readArray(getClass().getClassLoader());
        }

        public static final Parcelable.Creator<CommentsBean> CREATOR = new Parcelable.Creator<CommentsBean>() {
            @Override
            public CommentsBean createFromParcel(Parcel source) {
                return new CommentsBean(source);
            }

            @Override
            public CommentsBean[] newArray(int size) {
                return new CommentsBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.phoneNumber);
        dest.writeTypedList(this.comments);

    }

    public void readFromParcel(Parcel source) {
        this.id = source.readInt();
        this.name = source.readString();
        this.phoneNumber = source.readString();
        this.comments = source.createTypedArrayList(CommentsBean.CREATOR);
    }

    public CustomerBean() {
    }

    protected CustomerBean(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.phoneNumber = in.readString();
        this.comments = in.createTypedArrayList(CommentsBean.CREATOR);
    }

    public static final Parcelable.Creator<CustomerBean> CREATOR = new Parcelable.Creator<CustomerBean>() {
        @Override
        public CustomerBean createFromParcel(Parcel source) {
            return new CustomerBean(source);
        }

        @Override
        public CustomerBean[] newArray(int size) {
            return new CustomerBean[size];
        }
    };
}
