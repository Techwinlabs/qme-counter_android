package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

public class CallTicketbean {

    /**
     * ticketNumber :
     * succeed : false
     * phoneNumber : null
     */

    @SerializedName("ticketNumber")
    public String ticketNumber;
    @SerializedName("succeed")
    public boolean succeed;
    @SerializedName("phoneNumber")
    public String phoneNumber;
    public String pin;
    public int serviceid;
}
