package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

public class MonthsDataBean {

    @SerializedName("totalCustomers")
    public int totalCustomers;
    @SerializedName("walkInCount")
    public int walkInCount;
    @SerializedName("totalReservations")
    public int totalReservations;
    @SerializedName("noShowCount")
    public int noShowCount;
}
