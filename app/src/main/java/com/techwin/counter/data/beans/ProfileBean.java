package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

public class ProfileBean {

    @SerializedName("contactInfo")
    public ContactInfoBean contactInfo;
    @SerializedName("address")
    public AddressBean address;
    @SerializedName("startAt")
    public String startAt;
    @SerializedName("averageServingTime")
    public int averageServingTime;
    @SerializedName("numberOfCustomers")
    public int numberOfCustomers;
    @SerializedName("accountManager")
    public AccountManagerBean accountManager;

    public static class ContactInfoBean {
        @SerializedName("name")
        public String name;
        @SerializedName("email")
        public String email;
        @SerializedName("phoneNumber")
        public String phoneNumber;
    }

    public static class AddressBean {
        @SerializedName("districtId")
        public int districtId;
        @SerializedName("street")
        public StreetBean street;
        @SerializedName("longitude")
        public String longitude;
        @SerializedName("latitude")
        public String latitude;

        public static class StreetBean {
            @SerializedName("ar")
            public String ar;
            @SerializedName("en")
            public String en;
        }
    }

    public static class AccountManagerBean {

        @SerializedName("id")
        public int id;
        @SerializedName("contactInfo")
        public ContactInfoBean contactInfo;
        @SerializedName("numberOfServiceProvider")
        public int numberOfServiceProvider;

        public static class ContactInfoBean {
            @SerializedName("name")
            public String name;
            @SerializedName("email")
            public String email;
            @SerializedName("phoneNumber")
            public String phoneNumber;
        }
    }
}
