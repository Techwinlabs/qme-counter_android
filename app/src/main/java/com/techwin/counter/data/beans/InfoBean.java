package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

public class InfoBean {

    /**
     * name : test
     * service_provider_id : 1
     * branch_id : 1
     * service_id : 1
     * logo : https://lh5.googleusercontent.com/ia4CO-3b8PLlIe11zSPIdozHy-QZyc70lUYWQAmdkFsJyTFDbUl5dSW-8jSEfR2VHIc2OkM8ny3RLN6GPKU4=w1920-h950
     * nbf : 1613382237
     * exp : 1644918237
     * iat : 1613382237
     * iss : http://api-wrapper.qme.solutions
     */
    @SerializedName("name")
    public String name;
    @SerializedName("service_provider_id")
    public String serviceProviderId;
    @SerializedName("branch_id")
    public String branchId;
    @SerializedName("service_id")
    public String serviceId;
    @SerializedName("logo")
    public String logo;
    @SerializedName("nbf")
    public long nbf;
    @SerializedName("exp")
    public long exp;
    @SerializedName("iat")
    public long iat;
    @SerializedName("iss")
    public String iss;
}
