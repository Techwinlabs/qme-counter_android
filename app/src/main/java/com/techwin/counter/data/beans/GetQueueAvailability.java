package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetQueueAvailability {

    /**
     * averageServingTime : 0
     * days : [{"id":0,"workDay":0,"startTimeH":0,"startTimeM":0,"endTimeH":0,"endTimeM":0,"nightShift":true}]
     * offDays : [{"date":"2022-02-21T10:06:59.949Z"}]
     */

    @SerializedName("averageServingTime")
    private int averageServingTime;
    /**
     * id : 0
     * workDay : 0
     * startTimeH : 0
     * startTimeM : 0
     * endTimeH : 0
     * endTimeM : 0
     * nightShift : true
     */

    @SerializedName("days")
    private List<DaysBean> days;
    /**
     * date : 2022-02-21T10:06:59.949Z
     */

    @SerializedName("offDays")
    private List<OffDaysBean> offDays;

    public int getAverageServingTime() {
        return averageServingTime;
    }

    public void setAverageServingTime(int averageServingTime) {
        this.averageServingTime = averageServingTime;
    }

    public List<DaysBean> getDays() {
        return days;
    }

    public void setDays(List<DaysBean> days) {
        this.days = days;
    }

    public List<OffDaysBean> getOffDays() {
        return offDays;
    }

    public void setOffDays(List<OffDaysBean> offDays) {
        this.offDays = offDays;
    }

    public static class DaysBean {
        @SerializedName("id")
        private int id;
        @SerializedName("workDay")
        private int workDay;
        @SerializedName("startTimeH")
        private int startTimeH;
        @SerializedName("startTimeM")
        private int startTimeM;
        @SerializedName("endTimeH")
        private int endTimeH;
        @SerializedName("endTimeM")
        private int endTimeM;
        @SerializedName("nightShift")
        private boolean nightShift;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getWorkDay() {
            return workDay;
        }

        public void setWorkDay(int workDay) {
            this.workDay = workDay;
        }

        public int getStartTimeH() {
            return startTimeH;
        }

        public void setStartTimeH(int startTimeH) {
            this.startTimeH = startTimeH;
        }

        public int getStartTimeM() {
            return startTimeM;
        }

        public void setStartTimeM(int startTimeM) {
            this.startTimeM = startTimeM;
        }

        public int getEndTimeH() {
            return endTimeH;
        }

        public void setEndTimeH(int endTimeH) {
            this.endTimeH = endTimeH;
        }

        public int getEndTimeM() {
            return endTimeM;
        }

        public void setEndTimeM(int endTimeM) {
            this.endTimeM = endTimeM;
        }

        public boolean isNightShift() {
            return nightShift;
        }

        public void setNightShift(boolean nightShift) {
            this.nightShift = nightShift;
        }
    }

    public static class OffDaysBean {
        @SerializedName("date")
        private String date;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }
}
