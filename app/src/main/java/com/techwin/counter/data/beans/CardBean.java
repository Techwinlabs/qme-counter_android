package com.techwin.counter.data.beans;

import java.io.Serializable;

public class CardBean implements Serializable {
    public String account;
    public String cvv;
    public String month;
    public String year;
    public boolean fav;
    public long id;

    public int icon=-1;

    public CardBean(String account, String cvv, String month, String year, boolean fav) {
        this.account = account;
        this.cvv = cvv;
        this.month = month;
        this.year = year;
        this.fav = fav;
        this.id = System.currentTimeMillis() / 1000;
    }

    public CardBean() {
        this.id = System.currentTimeMillis() / 1000;
    }
}
