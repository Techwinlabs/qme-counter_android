package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerDataBean {
    public boolean show = false;


    /**
     * ticketNumber : 1
     * name : maged
     * phoneNumber : 01227984000
     * values : []
     */

    @SerializedName("ticketNumber")
    public String ticketNumber;
    @SerializedName("name")
    public String name;
    @SerializedName("phoneNumber")
    public String phoneNumber;
    @SerializedName("values")
    public List<?> values;
}
