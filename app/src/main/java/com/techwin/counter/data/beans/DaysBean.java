package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;
import com.techwin.counter.data.beans.base.BaseBean;

import java.util.List;

public class DaysBean extends BaseBean {
    @SerializedName("date")
    public String date;
    @SerializedName("count")
    public int count;
    @SerializedName("reservations")
    public List<ReservationsBean> reservations;

    public static class ReservationsBean {
        @SerializedName("id")
        public int id;
        @SerializedName("code")
        public String code;
        @SerializedName("time")
        public TimeBean time;
        @SerializedName("customer")
        public CustomerBean customer;

        public static class TimeBean {
            @SerializedName("hours")
            public int hours;
            @SerializedName("minutes")
            public int minutes;
        }
    }

}
