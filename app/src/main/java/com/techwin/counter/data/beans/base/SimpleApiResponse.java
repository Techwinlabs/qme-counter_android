package com.techwin.counter.data.beans.base;

import com.google.gson.annotations.SerializedName;

public class SimpleApiResponse {
    @SerializedName("response")
    protected boolean success;
    @SerializedName("message")
    protected String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "SimpleApiResponse{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
