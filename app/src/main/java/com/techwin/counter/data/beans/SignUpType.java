package com.techwin.counter.data.beans;

public class SignUpType {

    private String title;
    private boolean checked;

    public SignUpType(String title) {
        this.title = title;
    }

    public SignUpType() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
