package com.techwin.counter.data.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Arvind Poonia on 12/7/2018.
 */
public class UserBean {

    @SerializedName("token")
    public String token;
    @SerializedName("refreshToken")
    public String refreshToken;
    public String logo;

    /**
     * numberOfServices : 3
     * services : [{"id":10,"name":"Dr. Kirolos","counterData":{"logoUrl":"http://api-wrapper.qme.solutions/images/kirolos.jpeg","firstMessage":"رقم حضرتك @{TicketNumber} ويوجد في قائمة الإنتظار عدد @{CustomersCount} عميل.\nسعداء لاستخدامكم Qme","lastMessage":"برجاء العلم أن رقم حضرتك @{TicketNumber} هو القادم.\nشكرا لاستخدامك Qme"}},{"id":11,"name":"Dr. Kirolos 1","counterData":{"logoUrl":"http://api-wrapper.qme.solutions/images/kirolos_1.jpeg","firstMessage":"رقم حضرتك @{TicketNumber} ويوجد في قائمة الإنتظار عدد @{CustomersCount} عميل.\nسعداء لاستخدامكم Qme","lastMessage":"برجاء العلم أن رقم حضرتك @{TicketNumber} هو القادم.\nشكرا لاستخدامك Qme"}},{"id":12,"name":"Dr. Kirolos 2","counterData":{"logoUrl":"http://api-wrapper.qme.solutions/images/kirolos_2.jpeg","firstMessage":"رقم حضرتك @{TicketNumber} ويوجد في قائمة الإنتظار عدد @{CustomersCount} عميل.\nسعداء لاستخدامكم Qme","lastMessage":"برجاء العلم أن رقم حضرتك @{TicketNumber} هو القادم.\nشكرا لاستخدامك Qme"}}]
     */

    @SerializedName("numberOfServices")
    public int numberOfServices;
    @SerializedName("services")
    public List<Services> services;

    public static class Services {
        /**
         * id : 10
         * name : Dr. Kirolos
         * counterData : {"logoUrl":"http://api-wrapper.qme.solutions/images/kirolos.jpeg","firstMessage":"رقم حضرتك @{TicketNumber} ويوجد في قائمة الإنتظار عدد @{CustomersCount} عميل.\nسعداء لاستخدامكم Qme","lastMessage":"برجاء العلم أن رقم حضرتك @{TicketNumber} هو القادم.\nشكرا لاستخدامك Qme"}
         */

        @SerializedName("id")
        public int id;
        @SerializedName("name")
        public String name;
        @SerializedName("counterData")
        public CounterData counterData;

        @SerializedName("serviceId")
        public int serviceId;
        @SerializedName("logoUrl")
        public String logoUrl;

        public static class CounterData {
            /**
             * logoUrl : http://api-wrapper.qme.solutions/images/kirolos.jpeg
             * firstMessage : رقم حضرتك @{TicketNumber} ويوجد في قائمة الإنتظار عدد @{CustomersCount} عميل.
             سعداء لاستخدامكم Qme
             * lastMessage : برجاء العلم أن رقم حضرتك @{TicketNumber} هو القادم.
             شكرا لاستخدامك Qme
             */

            @SerializedName("logoUrl")
            public String logoUrl;
            @SerializedName("firstMessage")
            public String firstMessage;
            @SerializedName("lastMessage")
            public String lastMessage;
        }
    }
}