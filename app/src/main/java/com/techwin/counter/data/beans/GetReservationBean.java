package com.techwin.counter.data.beans;

public class GetReservationBean {

    /**
     * date : 2021-07-23T12:59:22.949Z
     * reservedSlots : 0
     * availableSlots : 0
     */

    private String date;
    private int reservedSlots;
    private int availableSlots;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getReservedSlots() {
        return reservedSlots;
    }

    public void setReservedSlots(int reservedSlots) {
        this.reservedSlots = reservedSlots;
    }

    public int getAvailableSlots() {
        return availableSlots;
    }

    public void setAvailableSlots(int availableSlots) {
        this.availableSlots = availableSlots;
    }
}
