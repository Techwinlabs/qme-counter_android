package com.techwin.counter.util.misc;

import androidx.databinding.ObservableList;

public abstract class DefaultListChangeCallback<T> extends ObservableList.OnListChangedCallback<ObservableList<T>> {
    @Override
    public void onChanged(ObservableList<T> ts) {

    }

    @Override
    public void onItemRangeChanged(ObservableList<T> ts, int i, int i1) {
        onSizeUpdated(ts.size());
    }

    @Override
    public void onItemRangeInserted(ObservableList<T> ts, int i, int i1) {
        onSizeUpdated(ts.size());
    }

    @Override
    public void onItemRangeMoved(ObservableList<T> ts, int i, int i1, int i2) {
        onSizeUpdated(ts.size());
    }

    @Override
    public void onItemRangeRemoved(ObservableList<T> ts, int i, int i1) {
       onSizeUpdated(ts.size());
    }

    public abstract void onSizeUpdated(int size);

}
