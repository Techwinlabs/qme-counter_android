package com.techwin.counter.util;

/**
 * Created by shank_000 on 10/3/2018.
 */

public class Constants {

    public static final String GOOGLE_URL = "https://maps.googleapis.com/maps/api/";
    public static final String DEFAULTLOCALE = "ar";
    public static final String ENGLOCALE = "en";
}
