package com.techwin.counter.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.provider.Settings;
import android.text.TextPaint;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputLayout;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Arvind Poonia on 11/26/2018.
 */
public class AppUtils {

    public static void setTypefaceToInputLayout(TextInputLayout inputLayout, Typeface typeFace) {
        try {
            inputLayout.getEditText().setTypeface(typeFace);

            // Retrieve the CollapsingTextHelper Field
            final Field collapsingTextHelperField = inputLayout.getClass().getDeclaredField("mCollapsingTextHelper");
            collapsingTextHelperField.setAccessible(true);

            // Retrieve an instance of CollapsingTextHelper and its TextPaint
            final Object collapsingTextHelper = collapsingTextHelperField.get(inputLayout);
            final Field tpf = collapsingTextHelper.getClass().getDeclaredField("mTextPaint");
            tpf.setAccessible(true);

            // Apply your Typeface to the CollapsingTextHelper TextPaint
            ((TextPaint) tpf.get(collapsingTextHelper)).setTypeface(typeFace);
        } catch (Exception ignored) {
            // Nothing to do
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static String getTimeFromUtc(long l) {
        if (l == 0)
            return "";
        try {
            return new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(new Date(l));
        } catch (Exception e) {
            return "";
        }
    }


    public static String getChatInboxTimeFromUtc(long l) {
        if (l == 0)
            return "";
        try {
            Calendar current = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

            Calendar date = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            date.setTimeInMillis(l);

            if (current.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR))
                return new SimpleDateFormat("hh:mm a", Locale.getDefault()).format(date.getTime());
            else
                return new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.getDefault()).format(date.getTime());

        } catch (Exception e) {
            return "";
        }
    }

    public static String getTimeWithFormatFromUtc(long l, String format) {
        if (l == 0)
            return "";
        try {
            return new SimpleDateFormat(format, Locale.getDefault()).format(new Date(l));
        } catch (Exception e) {
            return "";
        }
    }

    public static String getCurrencyFormat(double amount) {
        try {
            // NumberFormat us = NumberFormat.getCurrencyInstance(Locale.US);
            return String.valueOf(amount);
        } catch (Exception e) {
            return "Error";
        }

    }


    public static String getHeaderAgodate(long s) {
        Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Calendar time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        time.setTimeInMillis(s);
        StringBuilder builder = new StringBuilder();
        if (now.get(Calendar.DATE) == time.get(Calendar.DATE)) {
            builder.append("Today");
        } else if (now.get(Calendar.DATE) - time.get(Calendar.DATE) == 1) {
            builder.append("Yesterday");
        } else
            builder.append(new SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(new Date(s)));
        return builder.toString();
    }


    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            if (inputMethodManager != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                inputMethodManager.showSoftInput(activity.getCurrentFocus(), InputMethodManager.SHOW_IMPLICIT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static synchronized int convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return Math.round(px);
    }

    public static synchronized int convertPxTodp(int px) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return Math.round(dp);
    }

    @SuppressLint("HardwareIds")
    public static String getAndroidId(@NonNull Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            return "";
        }

    }


    public static Bitmap createBitmapFromView(@NonNull Activity context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


    public static String requestStatus(Integer s) {
        if (s == null)
            return "";
        switch (s) {
            case 1:
                return "Accepted";
            case 2:
                return "Rejected";
            default:
                return "Pending";
        }
    }

    public static String getProfileDate(@Nullable String date) {
        if (date == null)
            return "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ", Locale.US);
        Date date1 = null;//You will get date object relative to server/client timezone wherever it is parsed
        try {
            date1 = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "Error";
        }
        if (date1 != null) {
            DateFormat formatter = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()); //If you need time just put specific format for time like 'HH:mm:ss'
            return formatter.format(date1);
        }
        return "";
    }

    public static String getCommentDate(@Nullable String date) {
        //  0001-01-01T00:00:00+00:00
        //2021-09-21T06:09:12.5249729+00:00
        if (date == null)
            return "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ", Locale.US);
        Date date1 = null;//You will get date object relative to server/client timezone wherever it is parsed
        try {
            date1 = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "Error";
        }
        if (date1 != null) {
            DateFormat formatter = new SimpleDateFormat("EEE - MMM dd", Locale.getDefault()); //If you need time just put specific format for time like 'HH:mm:ss'
            return formatter.format(date1);
        }
        return "";
    }

    public static String getCommentTime(@Nullable String date) {
        //  0001-01-01T00:00:00+00:00
        if (date == null)
            return "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ", Locale.US);
        // DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
        Date date1 = null;//You will get date object relative to server/client timezone wherever it is parsed
        try {
            date1 = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return "Error";
        }
        if (date1 != null) {
            DateFormat formatter = new SimpleDateFormat("HH:mm a", Locale.getDefault()); //If you need time just put specific format for time like 'HH:mm:ss'
            return formatter.format(date1);
        }
        return "";
    }


    /**
     * @param compare date to compare
     * @param with    date with compare
     * @return int 0 equal,1 greater,-1 less
     */
    public static int compareDates(Date compare, Date with) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.US);
        long t1 = Long.parseLong(simpleDateFormat.format(compare));
        long t2 = Long.parseLong(simpleDateFormat.format(with));
        return Long.compare(t1, t2);
    }

    public static int compareMonthYear(Date compare, Date with) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM", Locale.US);
        long t1 = Long.parseLong(simpleDateFormat.format(compare));
        long t2 = Long.parseLong(simpleDateFormat.format(with));
        return Long.compare(t1, t2);
    }

    public static String getTimeString(int hour, int min) {
        StringBuilder builder = new StringBuilder();
        if (hour > 12) {
            builder.append("0");
            builder.append(hour % 12);
        } else builder.append(hour);
        builder.append(":");
        builder.append(String.format(Locale.US, "%02d", min));
        return builder.toString();
    }

    public static String amPm(int hrs) {
        if (hrs > 12) {
            return "PM";
        } else
            return "AM";
    }

    public static String getTimeByPattern(@Nullable String text, String pattren) {
        //  0001-01-01T00:00:00+00:00
        if (text == null)
            return "";

        Date date = null;//You will get date object relative to server/client timezone wherever it is parsed
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).parse(text);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        if (date == null)
            try {
                date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZ", Locale.US).parse(text);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        if (date != null) {
            DateFormat formatter = new SimpleDateFormat(pattren, Locale.US); //If you need time just put specific format for time like 'HH:mm:ss'
            return formatter.format(date.getTime());
        }
        return "";
    }

}
