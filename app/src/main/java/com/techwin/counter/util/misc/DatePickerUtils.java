package com.techwin.counter.util.misc;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;

import com.techwin.counter.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Utils class for date and time picker
 * Use style
 * <style name="datepicker" parent="Theme.AppCompat.Light.Dialog">
 * <item name="colorPrimary">@color/colorPrimary</item>
 * <item name="colorPrimaryDark">@color/colorPrimaryDark</item>
 * <item name="colorAccent">@color/colorPrimary</item>
 * </style>
 */
public class DatePickerUtils {
    private Context context;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;

    private DatePickerUtils(@NonNull Context context) {
        this.context = context;
    }

    public static DatePickerUtils getInstance(@NonNull Context context) {
        return new DatePickerUtils(context);
    }

    private void setContext(Context context) {
        this.context = context;
    }

    public DatePickerDialog getDatePickerDialog(@NonNull Calendar c, @NonNull DateSetListener listioner) {
        datePickerDialog = new DatePickerDialog(context, R.style.datepicker, (view, year, month, dayOfMonth) -> {
            Calendar calendar = (Calendar) c.clone();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            listioner.onDateSet(datePickerDialog, calendar);
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        return datePickerDialog;
    }

    public TimePickerDialog getTimePickerDialog(@NonNull Calendar c, @NonNull TimeSetListener listener, boolean is24hourView) {
        timePickerDialog = new TimePickerDialog(context, R.style.datepicker, (view, hourOfDay, minute) -> {
            Calendar calendar = (Calendar) c.clone();
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            listener.onTimeSet(timePickerDialog, calendar);
        }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), is24hourView);
        return timePickerDialog;
    }

    public abstract static class DateSetListener {
        @CallSuper
        public void onDateSet(@NonNull DatePickerDialog datePickerDialog, @NonNull Calendar ca) {
            datePickerDialog.dismiss();
        }
    }

    public abstract static class TimeSetListener {
        @CallSuper
        public void onTimeSet(@NonNull TimePickerDialog timePickerDialog, @NonNull Calendar ca) {
            timePickerDialog.dismiss();
        }

    }

    public static String getDateSendingFormat(Calendar calendar) {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(calendar.getTime());
    }
}
