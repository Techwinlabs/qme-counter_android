package com.techwin.counter.util.misc;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

public class TemplateLayout extends RelativeLayout {
    private Point point;

    public TemplateLayout(Context context) {
        super(context);
        init(context);
    }

    public TemplateLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TemplateLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public TemplateLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(getWidthMesure(widthMeasureSpec, heightMeasureSpec), heightMeasureSpec);
    }

    private int getWidthMesure(int w, int h) {
        if (point.x > 0 && point.y > 0) {

        } else {
            return point.x / 2;
        }
        return w;
    }

    private void init(Context context) {
        Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        point = new Point();
        display.getSize(point);
    }
}
